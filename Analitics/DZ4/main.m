n=26;
delf2=1;
delf3=3;
A = [1 2
    -2 -1
    -1 0
    1 0
    0 -1
    0 1];
 
b = [4*n -n 0 2*n 0 1.5*n];
f = [1 -3];
x = linprog(-f,A,b);
A(7,1)=f(1,1)*-1;
A(7,2)=f(1,2)*-1;
b(1,7)=(f(1,1)*x(1,1)+f(1,2)*x(2,1)-delf3)*-1;
 
fprintf("X*3 = \n");
disp(x);
fprintf("f*3 = ");
disp(f(1,1)*x(1,1)+f(1,2)*x(2,1));
 
f=[-3 1];
x = linprog(-f,A,b);
A(8,1)=f(1,1)*-1;
A(8,2)=f(1,2)*-1;
b(1,8)=(f(1,1)*x(1,1)+f(1,2)*x(2,1)-delf2)*-1;
 
fprintf("X*2 = \n");
disp(x);
fprintf("f*2 = ");
disp(f(1,1)*x(1,1)+f(1,2)*x(2,1));
 
f=[1 1];
 
x = linprog(-f,A,b);
fprintf("X*1 = Xopt = \n");
disp(x);
fprintf("f*1 = ");
disp(f(1,1)*x(1,1)+f(1,2)*x(2,1));
 
F1=f(1,1)*x(1,1)+f(1,2)*x(2,1);
fprintf("f1 = ");
disp(F1);
F2=-1*A(8,1)*x(1,1)-A(8,2)*x(2,1);
fprintf("f2 = ");
disp(F2);
F3=-1*A(7,1)*x(1,1)-A(7,2)*x(2,1);
fprintf("f3 = ");
disp(F3);
