clc;
lam=0.6;
Q1 = [6 7 8 10
    4 5 6 13
    10 6 5 11
    8 9 7 9
    1 2 1 2
    2 1 2 4
    3 3 2 2
    4 1 3 3
    ];
Q2 = [3 6 4 8
    9 7 5 11
    4 6 8 12
    7 4 3 5
    5 3 8 4
    2 5 6 12
    8 6 3 11
    2 5 9 10
    ];
[n, m]=size(Q1);
 
fprintf("Матрица Q\n");
for i=1:n
    for k=1:m
        fprintf("(" + Q1(i,k) + "; " + Q2(i,k) + ")\t");
    end
    fprintf("\n");
end
 
rez=zeros(1,n);

%% Maximin principle
arr=zeros(3,n);
for i=1:n
    arr(1,i)=i;
    arr(2,i)=min(Q1(i,:));
    arr(3,i)=min(Q2(i,:));
end
 
i=1;
b=arr(1,:);
while i<=n
    j1=arr(2,i);
    j2=arr(3,i);
    for k=1:n
        if (k~=i)
            if ((arr(2,k)<=j1)&&(arr(3,k)<=j2))
                arr1 = b(b~=k);
                b=arr1;
            end
        end
    end
    i=i+1;
end
 
fprintf("\nМножество оптимальных проектов, удовлетворяющих принципу максимина:\nXp = {");
for i=1:numel(b)
    rez(b(1,i))=rez(b(1,i))+1;
    fprintf("a"+b(1,i));
    if i<numel(b)
        fprintf("; ");
    end
end
fprintf("}\n")

%% R(U), B(w)
R1=zeros(n,m);
R2=zeros(n,m);

B1=max(Q1);
B2=max(Q2);
for i=1:n
    for j=1:m
        R1(i,j)=B1(j)-Q1(i,j);
        R2(i,j)=B2(j)-Q2(i,j);
    end
end

arr=zeros(3,n);
for i=1:n
    arr(1,i)=i;
    arr(2,i)=max(R1(i,:));
    arr(3,i)=max(R2(i,:));
end
%% Vector minimax regret principle
i=1;
b=arr(1,:);
while i<=n
    j1=arr(2,i);
    j2=arr(3,i);
    for k=1:n
        if (k~=i)
            if ((arr(2,k)>=j1)&&(arr(3,k)>=j2))
                arr1 = b(b~=k);
                b=arr1;
            end
        end
    end
    i=i+1;
end
 
fprintf("Множество оптимальных проектов, удовлетворяющих принципу векторного минимаксного сожаления:\nXp = {");
for i=1:numel(b)
    rez(b(1,i))=rez(b(1,i))+1;
    fprintf("a"+b(1,i));
    if i<numel(b)
        fprintf("; ");
    end
end
fprintf("}\n")
%% Savage
rez1=zeros(1,n);
rez2=zeros(1,n);

minb1=zeros(1,1);
[~, minb1(1,1)]=min(arr(2,:));
rez1(minb1(1,1))=rez1(minb1(1,1))+1;
k=2;
for i=(minb1(1,1)+1):n
    if arr(2,i)==min(arr(2,:))
        minb1(1,k)=i;
        rez1(minb1(1,k))=rez1(minb1(1,k))+1;
        k=k+1;
    end
end

minb2=zeros(1,1);
[~,minb2(1,1)]=min(arr(3,:));
rez2(minb2(1,1))=rez2(minb2(1,1))+1;
k=2;
for i=(minb2(1,1)+1):n
    if arr(3,i)==min(arr(3,:))
        minb2(1,k)=i;
        rez2(minb2(1,k))=rez2(minb2(1,k))+1;
        k=k+1;
    end
end
%% Wald

qmin1=min(Q1,[],2);
maxqmin1=zeros(1,1);
[~,maxqmin1(1,1)]=max(qmin1);
rez1(maxqmin1(1,1))=rez1(maxqmin1(1,1))+1;
k=2;
for i=(maxqmin1(1,1)+1):n
    if qmin1(i)==max(qmin1)
        maxqmin1(1,k)=i;
        rez1(maxqmin1(1,k))=rez1(maxqmin1(1,k))+1;
        k=k+1;
    end
end
 
qmin2=min(Q2,[],2);
maxqmin2=zeros(1,1);
[~,maxqmin2(1,1)]=max(qmin2);
rez2(maxqmin2(1,1))=rez2(maxqmin2(1,1))+1;
k=2;
for i=(maxqmin2(1,1)+1):n
    if qmin2(i)==max(qmin2)
        maxqmin2(1,k)=i;
        rez2(maxqmin2(1,k))=rez2(maxqmin2(1,k))+1;
        k=k+1;
    end
end

%% Hurwitz
qmax1=max(Q1,[],2);
qmax2=max(Q2,[],2);

c1=zeros(n,1);
c2=zeros(n,1);
for i=1:n
    c1(i,1)=lam*qmin1(i,1)+(1-lam)*qmax1(i,1);
    c2(i,1)=lam*qmin2(i,1)+(1-lam)*qmax2(i,1);
end
maxc1=zeros(1,1);
[~,maxc1(1,1)]=max(c1);
rez1(maxc1(1,1))=rez1(maxc1(1,1))+1;
k=2;
for i=(maxc1(1,1)+1):n
    if c1(i)==max(c1)
        maxc1(1,k)=i;
        rez1(maxc1(1,k))=rez1(maxc1(1,k))+1;
        k=k+1;
    end
end
maxc2=zeros(1,1);
[~,maxc2(1,1)]=max(c2);
rez2(maxc2(1,1))=rez2(maxc2(1,1))+1;
k=2;
for i=(maxc2(1,1)+1):n
    if c2(i)==max(c2)
        maxc2(1,k)=i;
        rez2(maxc2(1,k))=rez2(maxc2(1,k))+1;
        k=k+1;
    end
end
%% Laplace
d1=zeros(n,1);
d2=zeros(n,1);
pj=1/m;
for i=1:n
    for j=1:m
        d1(i,1)=d1(i,1)+pj*Q1(i,j);
        d2(i,1)=d2(i,1)+pj*Q2(i,j);
    end
end
maxd1=zeros(1,1);
[~,maxd1(1,1)]=max(d1);
rez1(maxd1(1,1))=rez1(maxd1(1,1))+1;
k=2;
for i=(maxd1(1,1)+1):n
    if d1(i)==max(d1)
        maxd1(1,k)=i;
        rez1(maxd1(1,k))=rez1(maxd1(1,k))+1;
        k=k+1;
    end
end
maxd2=zeros(1,1);
[~,maxd2(1,1)]=max(d2);
k=2;
for i=(maxd2(1,1)+1):n
    if d2(i)==max(d2)
        maxd2(1,k)=i;
        rez2(maxd2(1,k))=rez2(maxd2(1,k))+1;
        k=k+1;
    end
end
%% Total
idx1=zeros(1,1);
[~,idx1(1,1)]=max(rez1);
k=2;
for i=(idx1(1,1)+1):n
    if rez1(i)==max(rez1)
        idx1(1,k)=i;
        k=k+1;
    end
end
 
idx2=zeros(1,1);
[~,idx2(1,1)]=max(rez2);
k=2;
for i=(idx2(1,1)+1):n
    if rez2(i)==max(rez2)
        idx2(1,k)=i;
        k=k+1;
    end
end
 
fprintf("Рекомендуемое решение по совокупности критериев Вальда, Сэвиджа, Гурвица, Лапласа для показателя f1 - ");
for i=1:numel(idx1)
    rez(idx1(1,i))=rez(idx1(1,i))+1;
    fprintf("a"+idx1(1,i));
    fprintf("; ");
end
fprintf("\nРекомендуемое решение по совокупности критериев Вальда, Сэвиджа, Гурвица, Лапласа для показателя f2 - ");
for i=1:numel(idx2)
    rez(idx2(1,i))=rez(idx2(1,i))+1;
    fprintf("a"+idx2(1,i));
    fprintf("; ");
end
 
idx=zeros(1,1);
[x,idxmax]=max(rez);
idx(1,1)=idxmax;
k=2;
for i=(idx(1,1)+1):n
    if rez(i)==max(rez)
        idx(1,k)=i;
        k=k+1;
    end
end
fprintf("\nРекомендуемое решение по совокупности предыдущих решений:\n");
for i=1:numel(idx)
    rez(idx(1,i))=rez(idx(1,i))+1;
    fprintf("a"+idx(1,i)+" {");
    for k=1:m
        fprintf("("+Q1(idx(1,i),k)+"; "+Q2(idx(1,i),k)+") ")
    end
    fprintf("}\n");
end
