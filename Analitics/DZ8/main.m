clc;
 
A = [1 2 3 4; 7 5 3 1];
[n, m]=size(A);
 
b = [1; 1];
f = [1; 1; 1; 1];
lb = [0;0;0;0];
Aeq=[];
Beq=[];
ub=[];
 
x = linprog(-f,A,b,Aeq,Beq,lb,ub);
 
A1=zeros(2,1);
C1=zeros(1,1);
k=1;
F=0;
for i=1:numel(x)
    if x(i)>0
        A1(:,k)=A(:,i);
        C1(1,k)=f(i);
        F=F+x(i);
        k=k+1;
    end
end
if numel(C1)==1
    if A1(:,1)~= A(:,1)
        A1(:,2)= A(:,1);
    else
        A1(:,2)= A(:,2);
    end
    C1(1,2)=1;
end

Y=C1*(inv(A1)); 
y=1/F;
 
pz=Y*y;
qz=x*y;
 
fprintf("Цена игры = "+y+"\n");
fprintf("Оптимальная смешанная стратегии игрока I:\n");
disp(pz);
fprintf("Оптимальная смешанная стратегия игрока II:\n");
disp(qz');
for i=1:n
    fprintf("Модификация М"+i);
    fprintf(" = "+(pz(1,i)*100)+" процентов от общего объёма выпуска\n");
end
