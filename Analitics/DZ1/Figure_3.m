function Figure_3(N, arr)
figure;
for i=1:N
    x=arr(2,i);
    y=arr(3,i);
    plot(x,y,'.');
    text(x,y,string(i));
    x1=[x x];
    y1=[50 y];
line(x1,y1,'Color','black','LineStyle','-');
    x1=[50 x];
    y1=[y y];
line(x1,y1,'Color','black','LineStyle','-');
    hold on;
end
grid on;
xlabel('f1');
ylabel('f2');