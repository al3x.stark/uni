function Figure_1(n, N, arr)
figure;
theta = linspace(0,2*pi);
x1 = n*cos(theta) + n;
y1 = n*sin(theta) + n;
plot(x1,y1); 
hold on;    % ((f1 - n)^2) + ((f2 - n)^2)) <= n^2
x2 = linspace(0,26);
y2 = x2 + n;
plot(x2,y2);
hold on;    % (-f1 + f2) <= n
x3 = linspace(0,45);
y3 = -x3 + n * 2;
plot(x3,y3);
hold on;    % (f1 + f2) >= n * 2
for i=1:N
    plot(arr(2,i),arr(3,i),'.');
    text(arr(2,i),arr(3,i),string(i));
    hold on;
end
grid on;
xlabel('f1');
ylabel('f2');
legend ('(f1-26)^2+(f2-26)^2 <= 26^2','-f1+f2 <= 26','f1+f2 >= 2*26','Location','southwest');
axis equal;