function Figure_2(N, arr, a)
figure;
for i=1:N
    x=arr(2,i);
    y=arr(3,i);
    plot(x,y,'.');
    z=find(a==i);
    if z>0
        x1=[x x];
        y1=[0 y];
 line(x1,y1,'Color','black','LineStyle','-');
        x1=[0 x];
        y1=[y y];
line(x1,y1,'Color','black','LineStyle','-');
    end
    text(x,y,string(i));
    hold on;
end
grid on;
xlabel('f1');
ylabel('f2');