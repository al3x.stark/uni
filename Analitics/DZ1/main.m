N=20;
n=26;
K1=1;
K2=0.85;
K3=0.75;
arr=zeros(6,N);
for i=1:N
    arr(1,i)=i;
end
%% Create dots
i=1;
while i<=N
   z=0;
   f1=(50-14)*rand(1)+14;
   f2=(50-8)*rand(1)+8;   
   if (((((f1 - n)^2) + ((f2 - n)^2)) <= n^2) && ((-f1 + f2) <= n) && ((f1 + f2) >= n * 2))
       for k=1:(i-1)
           if ((arr(2,k)==f1)&&(arr(3,k)==f2))  % repeat check
               z=1;
           end
       end
       if (z==1) 
           continue
       end
       arr(2,i)=f1;
       arr(3,i)=f2;
       i=i+1;
   end
end
%% Find the best
i=1;
a=arr(1,:);
while i<=N
    f1=arr(2,i);
    f2=arr(3,i);
    for k=1:N
        if (k~=i)
            if ((f1>=arr(2,k))&&(f2>=arr(3,k)))
                arr1 = a(a~=k);
                a=arr1;
            end
        end
    end
    i=i+1;
end
fprintf("P = ");
disp(a);
%% Indexes of effectiveness and clusters
for i=1:N
    f1=arr(2,i);
    f2=arr(3,i);
    b=0;
    for k=1:N
        if (k~=i)
            if ((f1<=arr(2,k))&&(f2<=arr(3,k)))
                b=b+1;
            end
        end
    end
    arr(4,i)=b;
    arr(5,i)=round((1/(1+(b/(N-1))))*100)/100;
    if (arr(5,i)<=K3)
        arr(6,i)=3;
    elseif (arr(5,i)<K2)
        q1=K2-arr(5,i);
        q2=arr(5,i)-K3;
        if (q1<q2)
            arr(6,i)=2;
        else
            arr(6,i)=3;
        end
    else
        q1=K1-arr(5,i);
        q2=arr(5,i)-K2;
        if (q1<q2)
            arr(6,i)=1;
        else
            arr(6,i)=2;
        end
    end
end
%% Figures

Figure_1(n, N, arr);
Figure_2(N, arr, a);
Figure_3(N, arr);
Figure_4(N, arr);

%%
format bank
disp(arr);
