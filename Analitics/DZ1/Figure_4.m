function Figure_4(N, arr)
figure;
G1=0;
Y1=0;
R1=0;
Klabel_1 = '';
Klabel_2 = '';
Klabel_3 = '';
i = 1;
while (Klabel_3 == "")
    Klabel = '';
    if ((arr(6,i)==1)&& G1==0)
        plot(arr(2,i),arr(3,i),'G*');
        text(arr(2,i),arr(3,i),string(i));
        G1=i;
        Klabel = 'Кластер 1';
    elseif ((arr(6,i)==2)&& Y1==0)
        plot(arr(2,i),arr(3,i),'Y*');
        text(arr(2,i),arr(3,i),string(i));
        Y1=i;
        Klabel = 'Кластер 2';
    elseif ((arr(6,i)==3)&& R1==0)
        plot(arr(2,i),arr(3,i),'R*');
        text(arr(2,i),arr(3,i),string(i));
        R1=i;
        Klabel = 'Кластер 3';
    end
    hold on;
    if (Klabel ~= "")
        if (Klabel_1 == "")
            Klabel_1 = Klabel;
        elseif (Klabel_2 == "")
            Klabel_2 = Klabel;
        else
            Klabel_3 = Klabel;
        end
    end
    i = i+1;
end
for i=1:N
    if ((i~=G1)&&(i~=Y1)&&(i~=R1))
        if (arr(6,i)==1)
            plot(arr(2,i),arr(3,i),'G*');
        elseif (arr(6,i)==2)
            plot(arr(2,i),arr(3,i),'Y*');
        elseif (arr(6,i)==3)
            plot(arr(2,i),arr(3,i),'R*');
        end
        text(arr(2,i),arr(3,i),string(i));
    end
    hold on;
end
grid on;
xlabel('f1');
ylabel('f2');
legend (Klabel_1,Klabel_2,Klabel_3);