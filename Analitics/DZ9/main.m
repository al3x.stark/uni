clc;
 
A = [1 2 3 4; 7 5 3 1];
 
pz=[2/3;1/3];
 
qz = [1/3;0;0;2/3];
 
vz=3;
 
k=100;
 
[n, m]=size(A);
 
arrB=zeros(k,m);
 
arrA=zeros(k,n);
 
arrV=zeros(k,3);
 
arrI=zeros(k,1);
 
arrJ=zeros(k,1);
 
arrP=zeros(n,1);
 
arrQ=zeros(m,1);
 
Pabs=zeros(k,n);
 
Qabs=zeros(k,m);
 
Vabs=zeros(k,1);
 
for i=1:k
    if (i==1)
        arrI(i)=randi(n);
        arrB(i,:)=A(arrI(i),:);
    else
        [~,arrI(i)]=max(arrA(i-1,:));
        arrB(i,:)=arrB(i-1,:)+A(arrI(i),:);
    end
    [~,arrJ(i)]=min(arrB(i,:));
    arrQ(arrJ(i))=arrQ(arrJ(i))+1;
    if (i==1)
        arrA(i,:)=A(:,arrJ(i))';
    else
        arrA(i,:)=arrA(i-1,:)+A(:,arrJ(i))';
    end
    [~,z]=max(arrA(i,:));
    arrP(z)=arrP(z)+1;
    Pabs(i,:)=abs(((arrP(:,1)')/i)-pz(:,1)');
    Qabs(i,:)=abs(((arrQ(:,1)')/i)-qz(:,1)');
    arrV(i,1)=arrB(i,arrJ(i))/i;
    [~,maxA]=max(arrA(i,:));
    arrV(i,2)=arrA(i,maxA)/i;
    arrV(i,3)=(arrV(i,1)+arrV(i,2))/2;
    Vabs(i)=abs(arrV(i,3)-vz);
end
 
fprintf("Было выполнено "+k+" итераций.\n");
fprintf("Цена игры = "+arrV(k,3)+"\n");
fprintf("Оптимальная смешанная стратегия игрока I:\n");
for i=1:n
    fprintf((arrP(i)/k)+"\t\t");
end
fprintf("\nОптимальная смешанная стратегия игрока II:\n");
for i=1:m
    fprintf((arrQ(i)/k)+"\t\t");
end
fprintf("\n")
 
figure
tiledlayout(n,1)
for i=1:n
    nexttile
    plot(Pabs(:,i))
    title(['Абсолютная погрешность для p',num2str(i)])
    xlabel('K') 
    ylabel('?p','rotation',0,'HorizontalAlignment','right')
end
 
figure
tiledlayout(m,1)
for i=1:m
    nexttile
    plot(Qabs(:,i))
    title(['Абсолютная погрешность для q',num2str(i)])
    xlabel('K') 
    ylabel('?q','rotation',0,'HorizontalAlignment','right') 
end
 
figure
plot(Vabs(:,1))
title('Абсолютная погрешность для v')
xlabel('K') 
ylabel('?v','rotation',0,'HorizontalAlignment','right')
