function Figure_1(N, arr)
figure;
for i=1:N
    plot(arr(2,i),arr(3,i),'.');
    hold on;
end

grid on;
xlabel('u1');
ylabel('u2');
axis equal;