clc;
N=100;

VecU=[0.2 0.3;0.5 0.8];     %верх - минимальные значений u, низ - максимальные значения u
d=2;                        %количество измерений пространства (двухмерное, трёхмерное и т.д.)
VecT=zeros(1,1);            %массив точек в БНГ
VecTBeg=[0 ; 1]; 
MasP=zeros(2^d,d);          %массив соответсвия точек и u
arrt=zeros(1,2);            %массив ребер
VecZap=zeros(1,d);

arr=zeros(5,N);
VecB=zeros(1,d);
%% T,П
for z=2:d
    VecT=zeros(2^z,z);
    n=(2^(z-1));
    for i=1:2^z
        for k=1:z
            if (i>(2^(z-1)))
                if (k==1)
                    VecT(i,k)=1;
                else
                    VecT(i,k)=VecT(n,k);
                    if (k==z)
                        n=n-1;
                    end
                end
            else
                if (k==1)
                    VecT(i,k)=0;
                else
                    VecT(i,k)=VecTBeg(i,k-1);
                end
            end
        end
    end
    VecTBeg=VecT;
end

fprintf('T = \n');
disp(VecT)

for i=1:2^d
    for k=1:d
        if(VecT(i,k)==1)
            MasP(i,k)=VecU(2,k);
        else
            MasP(i,k)=VecU(1,k);
        end
    end
end

fprintf('П = \n');
disp(MasP)
%% L
s=0;
for i=1:(2^d)-1
    for k=i:2^d
        z=0;
        for q=1:d
            if (VecT(i,q)~=VecT(k,q))
                z=z+1;
            end
        end
        if (z==1)
            s=s+1;
            arrt(s,1)=i;
            arrt(s,2)=k;
        end
    end
end

fprintf('t = \n');
disp(arrt)
arrL=zeros(s,1);

for i=1:2^d
    for k=1:d
        arrL(i,1)=arrL(i,1)+MasP(i,k);
    end
    arrL(i,1)=arrL(i,1)-1;
end

fprintf('L = \n');
disp(arrL)
%% B
c=1;
for i=1:s
    if (arrL(arrt(i,1))*arrL(arrt(i,2))<=0)
        w=0;
        sum=0;
        for k=1:d
            if (MasP(arrt(i,1),k)==MasP(arrt(i,2),k))
                VecZap(1,k)= MasP(arrt(i,1),k);
                sum=sum+MasP(arrt(i,1),k);
            else
                w=k;
            end
        end
        VecZap(1,w)=1-sum;
        if (c==1)
            VecB(c,:)=VecZap;
            c=c+1;
        else
            z=0;
            for b=1:(c-1)
                w=0;
                for k=1:d
                    if (round(VecB(b,k)*100)/100==round(VecZap(1,k)*100)/100)
                        w=w+1;
                    end
                end
                if (w==d)
                    z=1;
                end
            end
            if (z==0)
                VecB(c,:)=VecZap;
                c=c+1;
            end
        end
    end
end

fprintf('B = \n');
disp(VecB)
%% P(J)
for i=1:N
    arr(1,i)=i;
end

i=1;

while i<=N
    z=0;
    u1=79*rand(1,1);
    u2=79*rand(1,1);
    for k=1:(i-1)
        if ((arr(2,k)==u1)&&(arr(3,k)==u2))
            z=1;
        end
    end
    if (z==1)
        continue
    end
    arr(2,i)=u1;
    arr(3,i)=u2;
    arr(4,i)=0.2*((u1-70)^2)+0.8*(u2-20)^2;
    arr(5,i)=0.2*((u1-10)^2)+0.8*(u2-70)^2;
    i=i+1;
end

i=1;
b=arr(1,:);
while i<=N
    j1=arr(4,i);
    j2=arr(5,i);
    for k=1:N
        if (k~=i)
            if ((arr(4,k)>=j1)&&(arr(5,k)>=j2))
                arr1 = b(b~=k);
                b=arr1;
            end
        end
    end
    i=i+1;
end

fprintf("P(J) = ");
disp(b);
%% PS(J)
if(VecB(1,1)<VecB(2,1))
    VecB1=VecB(1,:);
    VecB(1,:)=VecB(2,:);
    VecB(2,:)=VecB1;
end

i=1;
c=b;

while i<=length(b)
    j1=arr(4,b(i));
    j2=arr(5,b(i));
    w=b(i);
    for k=1:length(b)
        if (b(k)~=w)
            if (((VecB(1,1))*arr(4,b(k))+(VecB(1,2))*arr(5,b(k))<(VecB(1,1))*j1+(VecB(1,2))*j2)&&((VecB(2,1))*arr(4,b(k))+(VecB(2,2))*arr(5,b(k))<(VecB(2,1))*j1+(VecB(2,2))*j2))
                arr1 = c(c~=w);
                c=arr1;
            end
        end
    end
    i=i+1;
end

fprintf("PS(J) = ");
disp(c);
%% Figures

format bank
disp(arr);

Figure_1(N, arr)
Figure_2(N, arr, b)
Figure_3(N, arr, b, c)
Figure_4(N, arr, b, c, VecB)
Figure_5(VecB)