function Figure_2(N, arr, b)
figure;
R1=0;
B1=0;
leg_1 = '';
leg_2 = '';
i = 1;

while (leg_2 == "")
    leg = '';
    z=find(b==i);
    if (z>0)
        if (R1==0)
            plot(arr(4,i),arr(5,i),'R .');
            leg = 'Парето для единичной матицы';
            R1=i;
        end
    else
        if(B1==0)
            plot(arr(4,i),arr(5,i),'B .');
            leg = 'Заведомо неэффективное решение';
            B1=i;
        end
    end
    hold on;
    if (leg ~= "")
        if (leg_1 == "")
            leg_1 = leg;
        else
            leg_2 = leg;
        end
    end
    i= i+1;
end

for i=1:N
    if ((i~=B1)&&(i~=R1))
        z=find(b==i);
        if (z>0)
            plot(arr(4,i),arr(5,i),'R .');
        else
            plot(arr(4,i),arr(5,i),'B .');
        end
    end
    hold on;
end

grid on;
legend (leg_1,leg_2);
xlabel('j1');
ylabel('j2');
axis equal;