function Figure_5(VecB)
figure;

plot(0,0,'R .');
x=0;
y=0;

x1=[x+1*VecB(1,1) x];
y1=[y+1*VecB(1,2) y];
line(x1,y1,'Color','black','LineStyle','-');

x1=[x+1*VecB(2,1) x];
y1=[y+1*VecB(2,2) y];
line(x1,y1,'Color','black','LineStyle','-');

x1=[x-1*(((y-1*VecB(1,2))-(y+1*VecB(1,2)))/(sqrt((((x-1*VecB(1,1))-(x+1*VecB(1,1)))^2)+(((y-1*VecB(1,2))-(y+1*VecB(1,2)))^2)))) x];
y1=[y+1*(((x-1*VecB(1,1))-(x+1*VecB(1,1)))/(sqrt((((x-1*VecB(1,1))-(x+1*VecB(1,1)))^2)+(((y-1*VecB(1,2))-(y+1*VecB(1,2)))^2)))) y];
line(x1,y1,'Color','black','LineStyle','-');

x1=[x-1*(((y+1*VecB(2,2))-(y-1*VecB(2,2)))/(sqrt((((x+1*VecB(2,1))-(x-1*VecB(2,1)))^2)+(((y+1*VecB(2,2))-(y-1*VecB(2,2)))^2)))) x];
y1=[y+1*(((x+1*VecB(2,1))-(x-1*VecB(2,1)))/(sqrt((((x+1*VecB(2,1))-(x-1*VecB(2,1)))^2)+(((y+1*VecB(2,2))-(y-1*VecB(2,2)))^2)))) y];
line(x1,y1,'Color','black','LineStyle','-');

hold on;
grid on;
xlabel('j1');
ylabel('j2');
axis equal;