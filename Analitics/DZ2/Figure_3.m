function Figure_3(N, arr, b, c)
figure;
G1=0;
R1=0;
B1=0;
leg_1 = '';
leg_2 = '';
leg_3 = '';
i = 1;

while (leg_3 == "")
    leg = '';
    x=arr(4,i);
    y=arr(5,i);
    z=find(c==i);
    if (z>0)
        if (G1==0)
            plot(x,y,'G *');
            leg = 'Оптимальное решение';
            G1=i;
        end
    else
        z=find(b==i);
        if (z>0)
            if ((R1==0))
                plot(x,y,'R .');
                leg = 'Неоптимальное решение';
                R1=i;
            end
        elseif (B1==0)
            plot(x,y,'B .');
            leg = 'Неэффективное решение';
            B1=i;
        end
    end
    hold on;
    if (leg ~= "")
        if (leg_1 == "")
            leg_1 = leg;
        elseif (leg_2 == "")
            leg_2 = leg;
        else
            leg_3 = leg;
        end
    end
    i= i+1;
end

for i=1:N
    if ((i~=G1)&&(i~=B1)&&(i~=R1))
        x=arr(4,i);
        y=arr(5,i);
        z=find(c==i);
        if (z>0)
            plot(x,y,'G *');
        else
            z=find(b==i);
            if (z>0)
                plot(x,y,'R .');
            else
                plot(x,y,'B .');
            end
        end
    end
    hold on;
end

grid on;
xlabel('f1');
ylabel('f2');
legend (leg_1,leg_2,leg_3);
end