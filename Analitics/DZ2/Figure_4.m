function Figure_4(N, arr, b, c, VecB)
figure;

for i=1:N
    x=arr(4,i);
    y=arr(5,i);
    z=find(c==i);
    if (z>0)
        plot(x,y,'G .');
        x1=[x-2000*(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))/(sqrt((((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))^2)+(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))^2)))) x];
        y1=[y+2000*(((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))/(sqrt((((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))^2)+(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))^2)))) y];
        line(x1,y1,'Color','Green','LineStyle','-');
        x1=[x-2000*(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))/(sqrt((((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))^2)+(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))^2)))) x];
        y1=[y+2000*(((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))/(sqrt((((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))^2)+(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))^2)))) y];
        line(x1,y1,'Color','green','LineStyle','-');
    else
        z=find(b==i);
        if (z>0)
            plot(x,y,'R .');
            x1=[x-2000*(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))/(sqrt((((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))^2)+(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))^2)))) x];
            y1=[y+2000*(((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))/(sqrt((((x-1000*VecB(1,1))-(x+1000*VecB(1,1)))^2)+(((y-1000*VecB(1,2))-(y+1000*VecB(1,2)))^2)))) y]; 
            line(x1,y1,'Color','Red','LineStyle','-');
            x1=[x-2000*(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))/(sqrt((((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))^2)+(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))^2)))) x]; 
            y1=[y+2000*(((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))/(sqrt((((x+1000*VecB(2,1))-(x-1000*VecB(2,1)))^2)+(((y+1000*VecB(2,2))-(y-1000*VecB(2,2)))^2)))) y];
            line(x1,y1,'Color','Red','LineStyle','-');
        else
            plot(x,y,'B .');
        end
    end
    hold on;
end

grid on;
xlabel('j1');
ylabel('j2');
axis equal;