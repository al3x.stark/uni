﻿using System;
using System.Diagnostics;

class Program
{
	static void Main(string[] args)
	{
		try
		{
			FailProcess();
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message);
		}

		Console.WriteLine("Failed to fail process!");
		Console.ReadKey();
	}

	static void FailProcess() => Process.GetCurrentProcess().Kill();
	static void FailProcess(params int[] procIds)
	{
		foreach (int procId in procIds)
		{
			try
			{
				Process target_proc = Process.GetProcesses().First(p => p.Id == procId);
				target_proc.Kill();
			}
            catch (InvalidOperationException)
            {
                Console.WriteLine($"Process with ID {procId} not found.");
            }
		}
	}
	static void FailProcess(params string[] procNames)
	{
		foreach (string procName in procNames)
		{
			try
			{
				Process target_proc = Process.GetProcesses().First(p => p.ProcessName == procName);
				target_proc.Kill();
			}
			catch
			{
				Console.WriteLine($"Process \"{procName}\" not found.");
			}
		}
	}
	static void FailProcess(bool killAllThatContains, params string[] procNames)
    {
		if (killAllThatContains)
        {
			foreach (Process process in Process.GetProcesses())
				foreach (string name in procNames)
                {
					if (process.ProcessName.Contains(name))
					{
						process.Kill();
						break;
					}
                }
			return;
		}
		FailProcess(procNames);
	}
}
