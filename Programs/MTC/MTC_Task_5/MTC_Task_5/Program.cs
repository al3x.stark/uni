﻿using System;

class Program
{
	static void Main(string[] args)
	{
		TransformToElephant();
		Console.WriteLine("Муха");
		Console.WriteLine("Муха");
		//... custom application code
	}

    static void TransformToElephant()
    {
        Console.SetOut(new NoFlyWriter(changeOnce: true));
    }

    class NoFlyWriter : StringWriter
    {
        readonly bool changeOnce;
        const string ELEPHANT = "Слон";
        TextWriter defaultOut = Console.Out;
        public NoFlyWriter(bool changeOnce = false)
        {
            this.changeOnce = changeOnce;
        }
        public override void WriteLine(string? value)
        {
            string? str;
            if (value == "Муха")
                str = ELEPHANT;
            else str = value;

            Console.SetOut(defaultOut);
            Console.WriteLine(str);

            if (value == "Муха" && changeOnce) return;
            Console.SetOut(this);
        }
        public override void Write(string? value)
        {
            string? str;
            if (value == "Муха")
                str = ELEPHANT;
            else str = value;

            Console.SetOut(defaultOut);
            Console.Write(str);

            if (value == "Муха" && changeOnce) return;
            Console.SetOut(this);
        }
    }
}
