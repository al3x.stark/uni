﻿using System;
using System.Globalization;

class Program
{
	static readonly IFormatProvider _ifp = CultureInfo.InvariantCulture;

	class Number
	{
		readonly int _number;
		public bool concatenation;

		public int Int { get => _number; }

        public Number(int number, bool concatenation = false)
        {
            _number = number;
            this.concatenation = concatenation;
        }

        public override string ToString()
		{
			return _number.ToString(_ifp);
		}

		public static string operator +(Number number1, object number2)
        {
			int num2;
			try
            {
				if (number2 is Number n2)
					num2 = n2.Int;
				else
					num2 = Convert.ToInt32(number2);
			}
            catch
            {
				if (number1.concatenation)
					return number1.ToString() + number2.ToString();
					// Сложение провести не удалось. Проведена конкатенация.
				else return number1.ToString();
			}
			return (number1.Int + num2).ToString();
        }
	}

	static void Main(string[] args)
	{
		int someValue1 = 10;
		int someValue2 = 5;

		string result = new Number(someValue1) + someValue2.ToString(_ifp);
		Console.WriteLine(result);
		Console.ReadKey();
	}
}

