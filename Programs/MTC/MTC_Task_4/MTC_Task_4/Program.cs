﻿using System;
using System.Diagnostics;

namespace MTC_Task_4
{
    class Program
    {
        static int memory = 0;
        static void Main(string[] args)
        {
            var random = new Random();
            int sortFactor = 100, maxValue = 2000, opCount = 10000, arrLength = 100;

            long qT=0, sT=0, qM=0, sM=0;
            Stopwatch stopwatch = new Stopwatch();

            string str = "";
            for (int i = 0; i < 100; i++) str += '_';
            Console.WriteLine(str);

            for (int k = 0; k < opCount; k++)
            {
                var arr = new List<int>();
                int max = 0;
                for (int i = 0; i < arrLength;)
                {
                    var r = random.Next(maxValue);
                    if (r >= max - sortFactor)
                    {
                        if (r > max)
                            max = r;
                        arr.Add(r);
                        i++;
                    }
                }

                memory = 0;
                stopwatch.Restart();
                QuickSort(arr).ToArray();
                stopwatch.Stop();
                var qSortT = stopwatch.ElapsedTicks;
                qM += memory;

                memory = 0;
                stopwatch.Restart();
                Sort(arr, sortFactor, 100000).ToArray();
                stopwatch.Stop();
                var sortT = stopwatch.ElapsedTicks;
                sM += memory;
                
                if ((k % (opCount/100)) == 0)
                    Console.Write("/");

                qT += qSortT;
                sT += sortT;
            }
            Console.WriteLine($"\n\nsortFactor = {sortFactor}\nmaxValue = {maxValue}\nCount of operations = {opCount}\nArray Length = {arrLength}" +
                $"\n\nQuickSort: \n\t{qT/opCount} tick/op\n\t{qM/opCount} bit/op\n\nSort: \n\t{sT/opCount} tick/op\n\t{sM/opCount} bit/op");
        }

        /// <summary>
        /// Возвращает отсортированный по возрастанию поток чисел
        /// </summary>
        /// <param name="inputStream">Поток чисел от 0 до maxValue. Длина потока не превышает миллиарда чисел.</param>
        /// <param name="sortFactor">Фактор упорядоченности потока. Неотрицательное число. Если в потоке встретилось число x, то в нём больше не встретятся числа меньше, чем (x - sortFactor).</param>
        /// <param name="maxValue">Максимально возможное значение чисел в потоке. Неотрицательное число, не превышающее 2000.</param>
        /// <returns>Отсортированный по возрастанию поток чисел.</returns>
        static IEnumerable<int> Sort(IEnumerable<int> inputStream, int sortFactor, int maxValue)
        {
            int max = sortFactor, intervalMax = 0, index = 0, ind = 0;
            List<int> list = inputStream.ToList();
            var sortedList = new List<int>();
            var output = new List<int>();
            while (ind < list.Count - 1)
            {
                if (max == maxValue)
                {
                    for (ind = index; ind < list.Count; ind++)
                    {
                        sortedList.Add(list[ind]); memory += 32;
                    }
                    break;
                }
                for (ind = index; ind < list.Count; ind++)
                {
                    if (list[ind] > max)
                    {
                        max = list[ind];
                        index = ind;
                        break;
                    }
                    if (list[ind] > intervalMax)
                        intervalMax = list[ind];
                    sortedList.Add(list[ind]); memory += 32;
                }
                if ((max - sortFactor) >= intervalMax)
                {
                    if (sortedList.Count < 2)
                    {
                        if (sortedList.Count > 0)
                        {
                            output.Add(sortedList[0]); memory += 32;
                            sortedList.Clear();
                        }
                    }
                    else
                    {
                        output.AddRange(QuickSort(sortedList)); memory += 32 * sortedList.Count;
                        sortedList.Clear();
                    }
                }
                intervalMax = max;
            }
            if (sortedList.Count < 2)
            {
                if (sortedList.Count > 0)
                {
                    output.Add(sortedList[0]); memory += 32;
                }
            }
            else
            {
                output.AddRange(QuickSort(sortedList)); memory += 32 * sortedList.Count;
            }
            if (index == list.Count - 1)
            {
                output.Add(list[index]); memory += 32;
            }

            return output;
        }
        static List<int> QuickSort(List<int> inputStream)
        {
            if (inputStream.Count() <= 1)
            {
                return inputStream;
            }

            var less = new List<int>();
            var equal = new List<int>();
            var greater = new List<int>();

            int pivot = inputStream.First();
            foreach (int i in inputStream)
            {
                if (i == pivot)
                    equal.Add(i);   
                else if (i < pivot)
                    less.Add(i);
                else greater.Add(i);

                memory += 32;
            }

            List<int> output = QuickSort(less);     memory += 32 * less.Count;
            output.AddRange(equal);                 memory += 32 * equal.Count;
            output.AddRange(QuickSort(greater));    memory += 32 * greater.Count;

            return output;
        }        
    }
}