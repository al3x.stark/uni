﻿using System;
using System.Collections.Generic;

namespace MTC_Task_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            var t = new[] { 1, 2, 3, 4 };
            foreach (var item in t.EnumerateFromTail(null))
            {
                Console.Write(item + " ");
            }
        }
    
        /// <summary>
        /// <para> Отсчитать несколько элементов с конца </para>
        /// <example> new[] {1,2,3,4}.EnumerateFromTail(2) = (1, ), (2, ), (3, 1), (4, 0)</example>
        /// </summary> 
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="tailLength">Сколько элеметнов отсчитать с конца  (у последнего элемента tail = 0)</param>
        /// <returns></returns>
        public static IEnumerable<(T item, int? tail)> EnumerateFromTail<T>(this IEnumerable<T> enumerable, int? tailLength)
        {
            var output = new List<(T, int?)>();
            int enCount = enumerable.Count();
            if (tailLength is null)
                tailLength = enCount;
            foreach (var item in enumerable)
            {
                if (enCount <= tailLength)
                    output.Add((item, enCount-1));
                else
                    output.Add((item, null));
                enCount--;
            }

            return output;
        }
    }
}