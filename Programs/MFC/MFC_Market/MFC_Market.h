﻿
// MFC_Market.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CMFCMarketApp:
// Сведения о реализации этого класса: MFC_Market.cpp
//

class CMFCMarketApp : public CWinApp
{
public:
	CMFCMarketApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CMFCMarketApp theApp;
