﻿
// MFC_MarketDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "MFC_Market.h"
#include "MFC_MarketDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CMFCMarketDlg



CMFCMarketDlg::CMFCMarketDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_MARKET_DIALOG, pParent)
	, Price(0)
	, Count(0)
	, Sum(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCMarketDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Price);
	DDX_Text(pDX, IDC_EDIT2, Count);
	DDX_Text(pDX, IDC_EDIT3, Sum);
	DDX_Control(pDX, IDC_CHECK1, resale);
	DDX_Control(pDX, IDC_CHECK2, wholesale);
}

BEGIN_MESSAGE_MAP(CMFCMarketDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_RADIO1, &CMFCMarketDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CMFCMarketDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CMFCMarketDlg::OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_RADIO4, &CMFCMarketDlg::OnBnClickedRadio4)
	ON_BN_CLICKED(IDC_RADIO5, &CMFCMarketDlg::OnBnClickedRadio5)
	ON_BN_CLICKED(IDC_CHECK1, &CMFCMarketDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CMFCMarketDlg::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCMarketDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// Обработчики сообщений CMFCMarketDlg

BOOL CMFCMarketDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	resale.SetCheck(BST_CHECKED);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CMFCMarketDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CMFCMarketDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CMFCMarketDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCMarketDlg::OnBnClickedRadio1()
{
	discount = 0.05;
}


void CMFCMarketDlg::OnBnClickedRadio2()
{
	discount = 0.1;
}


void CMFCMarketDlg::OnBnClickedRadio3()
{
	discount = 0.15;
}


void CMFCMarketDlg::OnBnClickedRadio4()
{
	discount = 0.2;
}


void CMFCMarketDlg::OnBnClickedRadio5()
{
	discount = 0;
}


void CMFCMarketDlg::OnBnClickedCheck1()
{
	wholesale.SetCheck(BST_UNCHECKED);
	resale.SetCheck(BST_CHECKED);
	sale = 0;
}


void CMFCMarketDlg::OnBnClickedCheck2()
{
	UpdateData(true);
	if (Count > 9)
	{
		wholesale.SetCheck(BST_CHECKED);
		resale.SetCheck(BST_UNCHECKED);
		sale = 0.5;
	}
	else
	{
		MessageBox(L"Опт доступен при количестве 10ти и более товаров", L"Error", ERROR);
		wholesale.SetCheck(BST_UNCHECKED);
		resale.SetCheck(BST_CHECKED);
		sale = 0;
	}
}


void CMFCMarketDlg::OnBnClickedButton1()
{
	UpdateData(true);
	r_discount = 1 - discount - sale;
	Sum = Price * Count * r_discount;
	UpdateData(false);
}
