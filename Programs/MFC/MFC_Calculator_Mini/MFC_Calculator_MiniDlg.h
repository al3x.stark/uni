﻿
// MFC_Calculator_MiniDlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CMFCCalculatorMiniDlg
class CMFCCalculatorMiniDlg : public CDialogEx
{
// Создание
public:
	CMFCCalculatorMiniDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_CALCULATOR_MINI_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	float Edit1 = 0;
	float Edit2 = 0;
	float Result = 0;
	int act;

	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();
	afx_msg void OnBnClickedRadio4();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnChangeEdit1();
};
