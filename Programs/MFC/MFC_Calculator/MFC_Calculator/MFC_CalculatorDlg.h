﻿
// MFC_CalculatorDlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CMFCCalculatorDlg
class CMFCCalculatorDlg : public CDialogEx
{
// Создание
public:
	CMFCCalculatorDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_CALCULATOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();	
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton0();
	afx_msg void OnBnClickedButtonPlus();
	afx_msg void OnBnClickedButtonMinus();
	afx_msg void OnBnClickedButtonMult();
	afx_msg void OnBnClickedButtonSplit();
	afx_msg void OnBnClickedButtonResult();
	afx_msg void OnBnClickedButtonClear();
	afx_msg void OnBnClickedButtonChange();
	afx_msg void OnBnClickedButtonPoint();
	
	float Edit;
	float number = 0;
	float mod = 0.1;
	bool point_flag = false;
	int op_flag = 0;
	bool first_tap_flag = false;
	int spam_flag = 0;
	float Memory;
	float last = 0;
	bool result_flag = true;
	bool r_flag = false;
	bool null_flag = true;
	afx_msg void OnBnClickedButtonMPlus();
	afx_msg void OnBnClickedButtonMMinus();
	afx_msg void OnBnClickedButtonMR();
	afx_msg void OnBnClickedButtonMClear();
	afx_msg void OnBnClickedButtonRadical();
	afx_msg void OnBnClickedButtonExp();
};
