﻿
// MFC_Calculator.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CMFCCalculatorApp:
// Сведения о реализации этого класса: MFC_Calculator.cpp
//

class CMFCCalculatorApp : public CWinApp
{
public:
	CMFCCalculatorApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CMFCCalculatorApp theApp;
