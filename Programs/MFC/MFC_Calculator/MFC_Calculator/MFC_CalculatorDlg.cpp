﻿
// MFC_CalculatorDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "MFC_Calculator.h"
#include "MFC_CalculatorDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CMFCCalculatorDlg



CMFCCalculatorDlg::CMFCCalculatorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_CALCULATOR_DIALOG, pParent)
	, Edit(0)
	, Memory(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCCalculatorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Edit);
	DDX_Text(pDX, IDC_EDIT2, Memory);
}

BEGIN_MESSAGE_MAP(CMFCCalculatorDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCCalculatorDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFCCalculatorDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFCCalculatorDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFCCalculatorDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFCCalculatorDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CMFCCalculatorDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CMFCCalculatorDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CMFCCalculatorDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CMFCCalculatorDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &CMFCCalculatorDlg::OnBnClickedButton0)
	ON_BN_CLICKED(IDC_BUTTON11, &CMFCCalculatorDlg::OnBnClickedButtonPlus)
	ON_BN_CLICKED(IDC_BUTTON12, &CMFCCalculatorDlg::OnBnClickedButtonMinus)
	ON_BN_CLICKED(IDC_BUTTON13, &CMFCCalculatorDlg::OnBnClickedButtonMult)
	ON_BN_CLICKED(IDC_BUTTON14, &CMFCCalculatorDlg::OnBnClickedButtonSplit)
	ON_BN_CLICKED(IDC_BUTTON15, &CMFCCalculatorDlg::OnBnClickedButtonResult)
	ON_BN_CLICKED(IDC_BUTTON16, &CMFCCalculatorDlg::OnBnClickedButtonClear)
	ON_BN_CLICKED(IDC_BUTTON17, &CMFCCalculatorDlg::OnBnClickedButtonChange)
	ON_BN_CLICKED(IDC_BUTTON18, &CMFCCalculatorDlg::OnBnClickedButtonPoint)
	ON_BN_CLICKED(IDC_BUTTON20, &CMFCCalculatorDlg::OnBnClickedButtonMPlus)
	ON_BN_CLICKED(IDC_BUTTON21, &CMFCCalculatorDlg::OnBnClickedButtonMMinus)
	ON_BN_CLICKED(IDC_BUTTON22, &CMFCCalculatorDlg::OnBnClickedButtonMR)
	ON_BN_CLICKED(IDC_BUTTON19, &CMFCCalculatorDlg::OnBnClickedButtonMClear)
	ON_BN_CLICKED(IDC_BUTTON23, &CMFCCalculatorDlg::OnBnClickedButtonRadical)
	ON_BN_CLICKED(IDC_BUTTON24, &CMFCCalculatorDlg::OnBnClickedButtonExp)
END_MESSAGE_MAP()


// Обработчики сообщений CMFCCalculatorDlg

BOOL CMFCCalculatorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CMFCCalculatorDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CMFCCalculatorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CMFCCalculatorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMFCCalculatorDlg::OnBnClickedButton1()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 1;
	else
	{
		Edit = Edit + mod;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}

void CMFCCalculatorDlg::OnBnClickedButton2()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 2;
	else
	{
		Edit = Edit + mod * 2;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}

void CMFCCalculatorDlg::OnBnClickedButton3()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 3;
	else
	{
		Edit = Edit + mod * 3;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton4()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 4;
	else
	{
		Edit = Edit + mod * 4;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton5()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 5;
	else
	{
		Edit = Edit + mod * 5;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton6()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 6;
	else
	{
		Edit = Edit + mod * 6;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton7()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 7;
	else
	{
		Edit = Edit + mod * 7;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton8()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 8;
	else
	{
		Edit = Edit + mod * 8;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton9()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10 + 9;
	else
	{
		Edit = Edit + mod * 9;
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButton0()
{
	UpdateData(true);
	if (first_tap_flag)
	{
		Edit = 0;
		first_tap_flag = false;
	}
	if (!point_flag)
		Edit = Edit * 10;
	else
	{
		mod *= 0.1;
	}
	UpdateData(false);
	spam_flag = 0;
	result_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButtonResult()
{
	if (spam_flag == 0)
	{
		UpdateData(true);
		if (result_flag)
		{
			last = Edit;
			result_flag = false;
			r_flag = true;
		}
		if (null_flag)
		{
			number = Edit;
			null_flag = false;
		}
		else
		{
			if (last != 0)
				Edit = last;
			switch (op_flag)
			{
			case 1:
				number += Edit;
				Edit = number;
				break;
			case 2:
				number -= Edit;
				Edit = number;
				break;
			case 3:
				number *= Edit;
				Edit = number;
				break;
			case 4:
				if (Edit == 0)
					MessageBox(L"Делить на 0 нельзя", L"Error", ERROR);
				else
				{
					number /= Edit;
					Edit = number;
				}
				break;
			case 5:
				number = pow(number, Edit);
				Edit = number;
			}

		}
		UpdateData(false);
	}
}


void CMFCCalculatorDlg::OnBnClickedButtonPlus()
{
	if (spam_flag != 1)
	{
		UpdateData(true);
		result_flag = false;
		if (!r_flag)
			CMFCCalculatorDlg::OnBnClickedButtonResult();
		else
			r_flag = false;
		op_flag = 1;
		first_tap_flag = true;
		point_flag = false;
		mod = 0.1;
		UpdateData(false);
		spam_flag = 1;
		last = 0;
	}
}


void CMFCCalculatorDlg::OnBnClickedButtonMinus()
{
	if (spam_flag != 2)
	{
		UpdateData(true);
		result_flag = false;
		if (!r_flag)
			CMFCCalculatorDlg::OnBnClickedButtonResult();
		else
			r_flag = false;
		op_flag = 2;
		first_tap_flag = true;
		point_flag = false;
		mod = 0.1;
		UpdateData(false);
		spam_flag = 2;
		last = 0;
	}
}


void CMFCCalculatorDlg::OnBnClickedButtonMult()
{
	if (spam_flag != 3)
	{
		UpdateData(true);
		result_flag = false;
		if (!r_flag)
			CMFCCalculatorDlg::OnBnClickedButtonResult();
		else
			r_flag = false;
		op_flag = 3;
		first_tap_flag = true;
		point_flag = false;
		mod = 0.1;
		UpdateData(false);
		spam_flag = 3;
		last = 0;
	}
}


void CMFCCalculatorDlg::OnBnClickedButtonSplit()
{
	if (spam_flag != 4)
	{
		UpdateData(true);
		result_flag = false;
		if (!r_flag)
			CMFCCalculatorDlg::OnBnClickedButtonResult();
		else
			r_flag = false;
		op_flag = 4;
		first_tap_flag = true;
		point_flag = false;
		mod = 0.1;
		UpdateData(false);
		spam_flag = 4;
		last = 0;
	}
}


void CMFCCalculatorDlg::OnBnClickedButtonClear()
{
	number = 0;
	Edit = 0;
	op_flag = 0;
	point_flag = false;
	mod = 0.1;
	first_tap_flag = false;
	UpdateData(false);
	last = 0;
	result_flag = false;
	r_flag = false;
	null_flag = true;
}


void CMFCCalculatorDlg::OnBnClickedButtonChange()
{
	UpdateData(true);
	Edit *= -1;
	number *= -1;
	UpdateData(false);
}


void CMFCCalculatorDlg::OnBnClickedButtonPoint()
{
	UpdateData(true);
	
	point_flag = true;
	UpdateData(false);
}



void CMFCCalculatorDlg::OnBnClickedButtonMPlus()
{
	UpdateData(true);
	Memory += Edit;
	UpdateData(false);
}



void CMFCCalculatorDlg::OnBnClickedButtonMMinus()
{
	UpdateData(true);
	Memory -= Edit;
	UpdateData(false);
}


void CMFCCalculatorDlg::OnBnClickedButtonMR()
{
	UpdateData(true);
	Edit = Memory;
	spam_flag = 0;
	UpdateData(false);
}


void CMFCCalculatorDlg::OnBnClickedButtonMClear()
{
	UpdateData(true);
	Memory = 0;
	UpdateData(false);
}


void CMFCCalculatorDlg::OnBnClickedButtonRadical()
{
	UpdateData(true);
	if (Edit >= 0)
		Edit = sqrt(Edit);
	else
		MessageBox(L"Ошибка", L"Error", ERROR);
	UpdateData(false);
}


void CMFCCalculatorDlg::OnBnClickedButtonExp()
{
	if (spam_flag != 5)
	{
		UpdateData(true);
		result_flag = false;
		if (!r_flag)
			CMFCCalculatorDlg::OnBnClickedButtonResult();
		else
			r_flag = false;
		op_flag = 5;
		first_tap_flag = true;
		point_flag = false;
		mod = 0.1;
		UpdateData(false);
		spam_flag = 5;
		last = 0;
	}
}
