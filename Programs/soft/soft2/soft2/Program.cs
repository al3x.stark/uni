﻿using System;

class Program
{
    public static void Main(string[] args)
    {
        string fileText;
        try
        {
            Console.Write("Введите адрес файла .txt: ");
            string? path = Console.ReadLine();
            var reader = File.OpenText(path);
            fileText = reader.ReadToEnd();
        }
        catch { return; }
        var rows = fileText.Split('\n');
        int max, min, iNum, result = 0;
        foreach (var row in rows)
        {
            var nums = row.Split('\t');
            max = min = Convert.ToInt32(nums[0]);
            foreach (var num in nums)
            {
                iNum = Convert.ToInt32(num);
                max = max < iNum ? iNum : max;
                min = min > iNum ? iNum : min;
            }
            result += max - min;
        }
        Console.WriteLine(result);
    }
}