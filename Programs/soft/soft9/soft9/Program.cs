﻿class Program
{
    public static void Main(string[] args)
    {
        int count = 0, result = 0;
        string fileText;
        try
        {
            Console.Write("Введите адрес файла .txt: ");
            string? path = Console.ReadLine();
            var reader = File.OpenText(path);
            fileText = reader.ReadToEnd();
        }
        catch { return; }

        for (int i = 0; i < fileText.Length; i++)
        {
            if (fileText[i] == '{')
                count++;
            else if (fileText[i] == '}')
            {
                result += count;
                count--;
            }
            else if (fileText[i] == '<')
            {
                int j;
                for (j = i + 1; j < fileText.Length; j++)
                {
                    if (fileText[j] == '!') j++;
                    else if (fileText[j] == '>') break;
                }
                i = j;
            }
        }
        Console.WriteLine(result);
    }
}
