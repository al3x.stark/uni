﻿using System.Numerics;

namespace soft4
{
    class Num
    {
        public string value;
        public int length;
        public Num(string value, int length)
        {
            this.value = value;
            this.length = length;
        }
    }
    class Program
    {
        public static void Main(string[] args)
        {
            int n, result = 1, count = 0;
            BigInteger sum;
            var step = new List<BigInteger>();
            BigInteger[] prevStep = new BigInteger[] {1};
            if (!int.TryParse(Console.ReadLine(), out n) || n < 1) return;
            for (int i = 1; i < n;)
            {
                count += i - 1;
                i++;
                for (int j = 0; j < i / 2; j++)
                {
                    if (j == 0)
                    {
                        step.Add(1);
                        result += 2;
                    }
                    else if (j == 1)
                    {
                        step.Add(i - 1);
                        result += (i - 1).ToString().Length * 2;
                    }
                    else if (j == 2)
                    {
                        step.Add(count);
                        result += count.ToString().Length * 2;
                    }
                    else
                    {
                        sum = prevStep[j - 1] + prevStep[j];
                        step.Add(sum);
                        result += sum.ToString().Length * 2;
                    }
                }
                if (i % 2 != 0)
                {
                    sum = prevStep[i / 2 - 1] * 2;
                    step.Add(sum);
                    result += sum.ToString().Length;
                }
                prevStep = step.ToArray();
                step.Clear();
            }
            Console.WriteLine();
            Console.WriteLine(result);
        }
    }
}