﻿using System;
using System.IO;

namespace soft7
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] fileText;
            try
            {
                Console.Write("Введите адрес файла .txt: ");
                string? path = Console.ReadLine();
                var reader = File.OpenText(path);
                fileText = reader.ReadToEnd().Split('\n');
            }
            catch { return; }
            string str;
            int[,] net = new int[1000, 1000];
            string[] point = new string[2];
            int result = 0, change, bright = 0, ind,
                rowStart, rowEnd, columnStart, columnEnd;
            foreach (string s in fileText)
            {
                str = s;
                ind = 0;
                while (str[ind] != ' ') ind++;
                if (str.Substring(0, ind) == "turn")
                {
                    ind++;
                    while (str[ind] != ' ') ind++;
                }
                switch (str.Substring(0, ind))
                {
                    case "turn on":
                        bright = 1;
                        break;
                    case "turn off":
                        bright = -1;
                        break;
                    case "toggle":
                        bright = 2;
                        break;
                }
                str = str.Remove(0, ++ind); ind = 0;
                while (str[ind] != ' ') ind++;
                point = str.Substring(0, ind).Split(',');
                rowStart = int.Parse(point[0]);
                columnStart = int.Parse(point[1]);

                ind += 9;
                point = str[ind..].Split(',');
                rowEnd = int.Parse(point[0]);
                columnEnd = int.Parse(point[1]);

                change = 0;
                for (int i = rowStart; i <= rowEnd; i++)
                {
                    for (int j = columnStart; j <= columnEnd; j++)
                    {
                        if (bright < 0 && net[i, j] == 0) continue;
                        net[i, j] += bright;
                        change += bright;
                    }
                }
                result += change;
            }
            Console.WriteLine(result);
        }
    }
}
