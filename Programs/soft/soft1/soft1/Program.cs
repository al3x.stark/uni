﻿using System;

class Program
{   
    static void Main(string[] args)
    {
        int count = 0, num;
        string fileText;
        try
        {
            Console.Write("Введите адрес файла .txt: ");
            string? path = Console.ReadLine();
            var reader = File.OpenText(path);
            fileText = reader.ReadToEnd();
        }
        catch { return; }
        var ops = fileText.Split('\n');
        foreach (var op in ops)
        {
            try
            {
                num = Convert.ToInt32(op.Remove(0, 1));
                if (op[0] == '-')
                    count -= num;
                else count += num;
            }
            catch { }
        }
        Console.WriteLine(count);
    }
}