﻿using System;
using System.IO;
using System.Linq;

namespace soft6
{
    class Program
    {
        static void Main(string[] args)
        {
            int cOne, cTwo, ind, found, result;
            string str;
            string fileText;
            try
            {
                Console.Write("Введите адрес файла .txt: ");
                string? path = Console.ReadLine();
                var reader = File.OpenText(path);
                fileText = reader.ReadToEnd().Trim();
            }
            catch { return; }
            result = fileText.Length;

            char[] arr = fileText.ToLower().ToArray();
            arr = arr.GroupBy(x => x).Select(g => g.Key).ToArray();
            foreach (var a in arr)
            {
                str = fileText;
                ind = 0;
                while (true)
                {
                    found = 0;
                    if (ind != 0) ind--;
                    for (int i = ind; i < str.Length - 1; i++)
                    {
                        cOne = str[i];
                        cTwo = str[i + 1];
                        if (Math.Abs(cOne - cTwo) == 32)
                        {
                            found = 2;
                            ind = i;
                            break;
                        }
                        else if (cOne == a || cOne == a - 32)
                        {
                            found = 1;
                            ind = i;
                            break;
                        }
                    }
                    if (found > 0)
                        str = str.Remove(ind, found);
                    else break;
                }
                if (str[str.Length-1] == a)
                    str = str.Remove(str.Length - 1, 1);
                if (result > str.Length) result = str.Length;
            }
            Console.WriteLine(result);
        }
    }
}
