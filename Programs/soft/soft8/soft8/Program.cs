﻿using System;
using System.Collections.Generic;
using System.IO;

namespace soft8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] table = new int[6, 50];
            List<int> prev;
            string[] commands;
            try
            {
                Console.Write("Введите адрес файла .txt: ");
                string? path = Console.ReadLine();
                var reader = File.OpenText(path);
                commands = reader.ReadToEnd().Split('\n');
            }
            catch { return; }
            string str;
            int ind, op=-1, row, column, xy, b, result = 0;
            string[] rect = new string[2];
            foreach (string com in commands)
            {
                ind = 0;
                str = com;
                while (str[ind] != ' ') ind++;
                if (str.Substring(0, ind) == "rotate")
                {
                    ind++;
                    while (str[ind] != ' ') ind++;
                }
                switch (str.Substring(0, ind))
                {
                    case "rect":
                        op = 0;
                        break;
                    case "rotate row":
                        op = 1;
                        break;
                    case "rotate column":
                        op = 2;
                        break;
                }
                if (op == 0)
                {
                    rect = str[(++ind)..].Split('x');
                    row = int.Parse(rect[1]);
                    column = int.Parse(rect[0]);
                    for (int i = 0; i < row; i++)
                        for (int j = 0; j < column; j++)
                            table[i, j] = 1;
                }
                else
                {
                    str = str.Remove(0, ind + 3); ind = 0;
                    while (str[ind] != ' ') ind++;
                    xy = int.Parse(str.Substring(0, ind));
                    b = int.Parse(str[(ind + 4)..]);
                    prev = new List<int>();
                    if (op == 1)
                    {
                        for (int j = 0; j < 50; j++)
                        {
                            if (table[xy, j] == 1)
                                prev.Add(j);
                            table[xy, j] = 0;
                        }
                        foreach (int el in prev)
                            table[xy, (el + b) % 50] = 1;
                    }
                    else
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            if (table[i, xy] == 1)
                                prev.Add(i);
                            table[i, xy] = 0;
                        }
                        foreach (int el in prev)
                            table[(el + b) % 6, xy] = 1;
                    }
                }
            }
            foreach (int el in table)
                result += el;
            Console.WriteLine(result);
        }
    }
}
