﻿using System;
using System.IO;

namespace soft5
{
    class Program
    {
        static void Main(string[] args)
        {
            int cOne, cTwo, ind = 0;
            bool found = false;
            string fileText;
            try
            {
                Console.Write("Введите адрес файла .txt: ");
                string? path = Console.ReadLine();
                var reader = File.OpenText(path);
                fileText = reader.ReadToEnd();
            }
            catch { return; }
            while (true)
            {
                found = false;
                if (ind != 0) ind--;
                for (int i = ind; i < fileText.Length - 1; i++)
                {
                    cOne = fileText[i];
                    cTwo = fileText[i + 1];
                    if (Math.Abs(cOne - cTwo) == 32)
                    {
                        found = true;
                        ind = i;
                        break;
                    }
                }
                if (found)
                    fileText = fileText.Remove(ind, 2);
                else break;
            }
            Console.WriteLine(fileText.Length);
        }
    }
}
