﻿class Program
{
    public static void Main(string[] args)
    {
        string fileText;
        try
        {
            Console.Write("Введите адрес файла .txt: ");
            string? path = Console.ReadLine();
            var reader = File.OpenText(path);
            fileText = reader.ReadToEnd();
        }
        catch { return; }
        var triangles = fileText.Split('\n');
        int count = 0, sOne, sTwo, sThree;
        foreach (var triangle in triangles)
        {
            var sides = triangle.Trim().Split("  ");
            if (sides.Length == 3)
            {
                sOne = Convert.ToInt32(sides[0]);
                sTwo = Convert.ToInt32(sides[1]);
                sThree = Convert.ToInt32(sides[2]);
                if (sOne + sTwo > sThree && sOne + sThree > sTwo && sTwo + sThree > sOne)
                    count++;
            }
        }
        Console.WriteLine(count);
    }
}