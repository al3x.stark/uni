﻿class Program
{
    public static void Main()
    {
        string str = "1113222113", newStr = "";
        char c;
        int i, j, count;
        Console.Write("|________________________________________|\n|");
        for (int k = 0; k < 40; k++)
        {
            i = 0;
            newStr = "";
            while (i < str.Length)
            {
                c = str[i];
                count = 1;
                for (j = i + 1; j < str.Length; j++)
                {
                    if (str[j] != c) break;
                    count++;
                }
                i = j;
                newStr += $"{count}{c}";
            }
            str = newStr;
            Console.Write('/');
        }
        Console.WriteLine('|');
        Console.WriteLine(newStr.Length);
    }
}