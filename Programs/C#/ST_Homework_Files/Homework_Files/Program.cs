﻿using System;
using System.IO;
using System.IO.Compression;

namespace Homework_Files
{
    class Program
    {
        // Даны все числа от 1 до N. Необходимо разделить числа на несколько групп так,
        // что если одно число делится на другое, то эти числа попадают в разные группы. 
        // В результате этого разбиения получается M групп.

        /// <summary>
        /// Получение промежутка чисел из консоли
        /// </summary>
        /// <returns></returns>
        static int GetNFromConsole()
        {
            int N;
            Console.Write("\nВведте число N: ");
            while (!Int32.TryParse(Console.ReadLine(), out N) || N < 1)
            {
                if (N < 1)
                    Console.WriteLine("Допустимы только натуральные числа");
                else
                    Console.WriteLine("Ошибка");

                Console.Write("\nВведте число N: ");
            }

            return N;
        }

        /// <summary>
        /// Возвращает адрес файла со значением N
        /// </summary>
        /// <returns></returns>
        static string GetNFileDirection()
        {
            bool fileExist = false;
            string fileName = string.Empty;
            while (!fileExist)
            {
                Console.Write("Введите расположение файла: ");
                fileName = Console.ReadLine().Trim();                       /// Удаление символов-разделителей.
                if (File.Exists(fileName)) fileExist = true;
                else Console.WriteLine("Файл не найден. Повторите ввод.\n");
            }

            return fileName;
        }

        /// <summary>
        /// Получение промежутка чисел из файла
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        static int GetNFromFile(string fileName)
        {
            bool rec = false;                                         ///   Значение true означает, что сейчас идет запись строки, означающей искомое число.
            char c; int N = 0;
            string number = string.Empty;                             ///   Строка, означающая искомое число.

            using (StreamReader sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    c = (char)sr.Read();                              ///   Посимвольное чтение из файла.
                    if (!rec)
                    {
                        if (Char.IsDigit(c) && c != '0')              ///   Проверка на числовое значение.
                        {
                            number += c;
                            rec = true;
                        }
                    }
                    else
                    {
                        if (Char.IsDigit(c)) number += c;
                        else
                        {
                            if (number.Length < 10 || number == "1000000000") break;           ///  Проверка на удовлетворение требований.
                            else
                            {
                                rec = false;
                                number = string.Empty;
                            }
                        }
                    }
                }
            }

            if (!rec || (number.Length > 9 && number != "1000000000"))
                Console.WriteLine("Число, удовлетворяющее условиям, не было найдено в представленном файле.");
            else N = Convert.ToInt32(number);

            return N;
        }

        /// <summary>
        /// Предоставляет 2 способа получения чмсла N: вручную и из файла.
        /// </summary>
        /// <returns></returns>
        static int GetN()
        {
            int N = 0;
            Console.WriteLine("Необходимо задать число N (0 < N <= 1_000_000_000).");
            while (N == 0)
            {
                Console.Write("\n 1. Ввести вручную\n 2. Получить из файла\n\nВыберите способ: ");
                switch (Console.ReadLine())
                {
                    case "1":
                        N = GetNFromConsole();
                        break;
                    case "2":
                        N = GetNFromFile(GetNFileDirection());
                        break;
                    default:
                        Console.WriteLine("Ошибка");
                        break;
                }
            }

            return N;
        }

        /// <summary>
        /// Вычеркивание кратных заданному числу чисел согласно алгоритму "Решето Эратосфена"
        /// </summary>
        /// <param name="sieveOfEratosthenes"></param>
        /// <param name="el"></param>
        /// <param name="pn"></param>
        /// <param name="pn_two"></param>
        /// <returns></returns>
        static bool[] CrossOutMultiple(bool[] sieveOfEratosthenes, int el, bool pn, bool pn_two)
        {
            int const_1 = 2, const_2 = 1;
            if (pn) const_1 = el;           /// Проверка на поиск простых чисел
            if (pn_two) const_2 = 2;        /// Проверка на поиск простых нечетных чисел

            for (int flag = el * const_1; flag < sieveOfEratosthenes.Length; flag += el * const_2)
                sieveOfEratosthenes[flag] = true;

            return sieveOfEratosthenes;
        }

        /// <summary>
        /// Возвращает булевый массив с отмеченными простыми числами на промежутке от 1 до N
        /// </summary>
        /// <param name="N"></param>
        /// <returns></returns>
        static bool[] PrimeNumbers(int N)
        {
            int el = 2;

            bool[] sieveOfEratosthenes = new bool[N+1];                                         /// Вычеркивание 
            sieveOfEratosthenes = CrossOutMultiple(sieveOfEratosthenes, el, true, false);       /// четных чисел

            for (el = 3; el*el < sieveOfEratosthenes.Length; el++)                                  /// 
            {                                                                                       ///
                if (!sieveOfEratosthenes[el])                                                       ///  Вычеркивание
                    sieveOfEratosthenes = CrossOutMultiple(sieveOfEratosthenes, el, true, true);    /// нечетных чисел
                el++;                                                                               ///
            }                                                                                       ///

            return sieveOfEratosthenes;
        }

        /// <summary>
        /// Возвращает булевый массив с отмеченными, некратными друг-другу, числами на промежутке от 1 до N
        /// </summary>
        /// <param name="N"></param>
        /// <param name="alreadyUsed"></param>
        /// <returns></returns>
        static bool[] GroupBuilder(int N, bool[] alreadyUsed)
        {
            bool[] sieveOfEratosthenes = new bool[N + 1];

            for (int el = 4; el < sieveOfEratosthenes.Length; el++)
            {
                if (!alreadyUsed[el])
                {                                                                                               ///
                    if (!sieveOfEratosthenes[el])                                                               ///  Вычеркивание
                        sieveOfEratosthenes = CrossOutMultiple(sieveOfEratosthenes, el, false, false);          ///  кратных чисел
                    el++;                                                                                       ///
                }
                else sieveOfEratosthenes[el] = true;                                                            /// Вычеркивание чисел из других групп
            }

            return sieveOfEratosthenes;
        }

        /// <summary>
        /// Отмечает уже использующиеся в других группах числа
        /// </summary>
        /// <param name="sieveOfEratosthenes"></param>
        /// <param name="alreadyUsed"></param>
        /// <returns></returns>
        static bool[] MarkAlreadyUsed(bool[] sieveOfEratosthenes, bool[] alreadyUsed)
        {
            for (int count = 2; count < sieveOfEratosthenes.Length; count++)
            {
                if (!sieveOfEratosthenes[count])
                    alreadyUsed[count] = true;
            }

            return alreadyUsed;
        }

        /// <summary>
        /// Вывод группы в консоль
        /// </summary>
        /// <param name="count"></param>
        /// <param name="sieveOfEratosthenes"></param>
        /// <param name="alreadyUsed"></param>
        static void ShowGroup(int count, bool[] sieveOfEratosthenes, bool[] alreadyUsed)
        {
            Console.Write($"\n{count}. ");
            for (int el = 2; el < sieveOfEratosthenes.Length; el++)
                if (sieveOfEratosthenes[el] == false && alreadyUsed[el] == false) Console.Write($"{el} ");
        }

        /// <summary>
        /// Создает файл в папке программы. Возвращает адрес файла.
        /// </summary>
        /// <returns></returns>
        static string GetPath(bool hugeN)
        {
            string filePath = Directory.GetCurrentDirectory();
            filePath = filePath.Remove(filePath.Length - 38);                           /// Переназначение адреса на главную папку.
            Console.WriteLine($"Результат находится по этому адресу: {filePath}");

            if (hugeN)                                                                  ///
            {                                                                           ///       Если N > 1000,
                Directory.CreateDirectory(@$"{filePath}Groups");                        /// то создается папка с файлами,
                filePath += @"Groups\Group_1.txt";                                      ///     где файл = группа
            }                                                                           ///
            else filePath += "Groups.txt";

            return filePath;
        }

        /// <summary>
        /// Запись группы в файл
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="count"></param>
        /// <param name="sieveOfEratosthenes"></param>
        /// <param name="alreadyUsed"></param>
        static void WriteGroup(StreamWriter sw, bool hugeN, int count, bool[] sieveOfEratosthenes, bool[] alreadyUsed)
        {
            int lineBreak = 0;
            if (!hugeN) sw.Write($"\n{count}. ");
            for (int el = 2; el < sieveOfEratosthenes.Length; el++)
                if (sieveOfEratosthenes[el] == false && alreadyUsed[el] == false)
                {
                    if (hugeN)                                                              ///
                    {                                                                       ///    В случае N > 1000
                        sw.Write("{0,-14}", el);                                            /// происходит выравнивание
                        lineBreak++;                                                        /// для улцчшения читаемости
                        if (lineBreak == 8) { sw.Write("\n"); lineBreak = 0; }              ///
                    }
                    else sw.Write($"{el} ");                                                /// Простая запись
                }
        }

        /// <summary>
        /// Изменение строки с целью создания нового файла
        /// </summary>
        /// <param name="count"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        static string ChangeFileName(int count, string filePath)
        {
            filePath = filePath.Remove(filePath.LastIndexOf('_')+1);
            filePath += $"{count}.txt";

            return filePath;
        }

        /// <summary>
        /// Запрос на отображение процесса выполнения программы
        /// </summary>
        /// <returns></returns>
        static bool YNRequest(string request)
        {
            bool choice = false, check = false;

            while (!check)
            {
                Console.Write("{0}\n ( Y / N ): ", request);

                switch (Console.ReadLine().ToLower())
                {
                    case "y":
                        choice = true;
                        check = true;
                        break;
                    case "n":
                        choice = false;
                        check = true;
                        break;
                    default:
                        Console.WriteLine("Ошибка\n");
                        break;
                }
            }

            return choice;
        }

        /// <summary>
        /// Отображение процесса выполнения программы
        /// </summary>
        /// <param name="mark"></param>
        /// <param name="date"></param>
        /// <param name="M"></param>
        /// <param name="count"></param>
        static void CheckProgress(bool mark, DateTime date, double M, int count = 0)
        {
            string part_1, ending;
            TimeSpan timeSpan;

            if (count == 0) { part_1 = "Простые числа"; ending = "ы"; count = 2; }
            else { part_1 = $"Группа {count}"; ending = "а"; }

            if (!mark)
            {
                Console.Write($"-- {part_1} найден{ending}.");
                timeSpan = DateTime.Now.Subtract(date);
                Console.WriteLine($" Время = {timeSpan.TotalSeconds }ms");
            }
            else
            {
                double check = (M - count) % 10;
                Console.Write($"--- {part_1} помечен{ending} и записан{ending}.");
                timeSpan = DateTime.Now.Subtract(date);
                Console.WriteLine($" Время = {timeSpan.TotalSeconds }ms");
                if (check < 5 && (M - count < 10 || M - count > 20))
                {
                    if (check > 1) Console.WriteLine($"Осталось {M - count} группы.\n");
                    else if (M - count != 0) Console.WriteLine($"Осталась {M - count} группа.\n");
                }
                else Console.WriteLine($"Осталось {M - count} групп.\n");
            }
        }

        /// <summary>
        /// Архивация файла/папки
        /// </summary>
        /// <param name="hugeN"></param>
        /// <param name="M"></param>
        static void Archive(bool hugeN, double M)
        {
            string filePath = Directory.GetCurrentDirectory();
            filePath = filePath.Remove(filePath.Length - 38);                           /// Переназначение адреса на главную папку.

            if (!hugeN)                                                                 /// Архивация файла
            {
                using (FileStream openStream = new FileStream(filePath + ".txt", FileMode.Open))
                {
                    using (FileStream createStream = File.Create(filePath + "Groups.zip"))
                    {
                        using (GZipStream gzipStream = new GZipStream(createStream, CompressionMode.Compress))
                        {
                            openStream.CopyTo(gzipStream);
                            Console.WriteLine("\nСжатие файла Groups завершено.\n Было: {0} байт\n Cтало: {1} байт", openStream.Length, createStream.Length);
                        }
                    }
                }
            }
            else                                                                        /// Архивация папки
            {
                /////////////////////////////////////////////
                /// Я не нашел иного способа, кроме как создавать копию папки 
                /// и архивировать уже ее. Изначальная папка всегда занята
                /// каким-то процессом. Я так и не понял каким и как его завершить.
                /// При этом если архивировать файл, то все работает нормально.
                /////////////////////////////////////////////
                
                Directory.CreateDirectory(filePath + "GRPS");

                for (var count = 1; count <= M; count++)
                    File.Copy(filePath + @$"Groups\Group_{count}.txt", filePath + @$"GRPS\Group_{count}.txt");

                ZipFile.CreateFromDirectory(filePath + "GRPS", filePath + "Groups.zip");

                FileInfo[] GRPS = new DirectoryInfo(filePath + "GRPS").GetFiles();
                long grpsLength = 0, zipLength = new FileInfo(filePath + "Groups.zip").Length;

                foreach (var file in GRPS)
                {
                    grpsLength += file.Length;
                    File.Delete(file.FullName);
                }
                Directory.Delete(filePath + "GRPS");

                Console.WriteLine("\nСжатие папки Groups завершено.\n Было: {0} байт\n Cтало: {1} байт", grpsLength, zipLength);
            }
        }

        static void Main(string[] args)
        {
            DateTime date;
            TimeSpan timeSpan;
            bool hugeN = false;
            bool checkProgress = false;
            int N = GetN(), opr = 0;
            double M;
            Console.WriteLine($"\nN = {N}");

            while (opr == 0)
            {
                Console.Write("\n 1. Вывести значение M (число групп)\n 2. Записать группы в файл\n\nВыберите способ: ");
                switch (Console.ReadLine())
                {
                    case "1":
                        opr = 1;
                        break;
                    case "2":
                        opr = 2;
                        break;
                    default:
                        Console.WriteLine("Ошибка");
                        break;
                }
            }

            if (opr == 1)
            {
                date = DateTime.Now;
                M = Math.Floor(Math.Log2(N)) + 1;                                   ///  Расчет числа групп
                timeSpan = DateTime.Now.Subtract(date);
            }
            else
            {
                date = DateTime.Now;
                M = Math.Floor(Math.Log2(N)) + 1;                                   ///  Расчет числа групп
                if (N >= 1000) hugeN = true;                                        ///  От того, насколько большое значение N, зависит отображение результата.

                bool[] alreadyUsed = new bool[N + 1];                               ///  Булевый массив, где значение true означает, что элемент уже используется в одной из групп чисел.

                Console.WriteLine("");

                string filePath = GetPath(hugeN);
                StreamWriter sw = new StreamWriter(filePath);

                if (!hugeN)
                {
                    using (sw)
                    {
                        sw.Write("\nГруппы:\n\n1. 1");                                  /// Запись в файл первой группы (она всегда присутствует).

                        if (N > 1)
                        {
                            bool[] sieveOfEratosthenes = PrimeNumbers(N);               ///  Булевый массив, где, согласно алгоритму "Решето Эратосфена", значение true означает перечеркнутый элемент.

                            WriteGroup(sw, hugeN, 2, sieveOfEratosthenes, alreadyUsed);
                            alreadyUsed = MarkAlreadyUsed(sieveOfEratosthenes, alreadyUsed);

                            for (int count = 3; count <= M; count++)
                            {
                                sieveOfEratosthenes = GroupBuilder(N, alreadyUsed);

                                WriteGroup(sw, hugeN, count, sieveOfEratosthenes, alreadyUsed);
                                alreadyUsed = MarkAlreadyUsed(sieveOfEratosthenes, alreadyUsed);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\n//////////////////////////////");
                    Console.WriteLine("\n Так как значение N >= 1000, результат будет записан в несколько файлов.\n Файлы вы сможете найти в папке Groups.\n");
                    Console.WriteLine("//////////////////////////////\n\nНажмите любую клавишу, что бы продолжить.");
                    Console.ReadKey();


                    if (N > 5000000) 
                    {
                        Console.WriteLine("\nЗначение N довольно большое, и выполнение может занять некоторое время.");
                        checkProgress = YNRequest("Вы хотите видеть процесс выполнения?"); 
                    }

                    date = DateTime.Now;
                    Console.WriteLine("\nПроцесс запущен.\n");

                    sw.Write("1");                                          /// Запись первой группы.
                    sw.Flush();

                    bool[] sieveOfEratosthenes = PrimeNumbers(N);

                    if (checkProgress) CheckProgress(false, date, M);

                    filePath = ChangeFileName(2, filePath);
                    sw = new StreamWriter(filePath);
                    WriteGroup(sw, hugeN, 2, sieveOfEratosthenes, alreadyUsed);
                    sw.Flush();
                    alreadyUsed = MarkAlreadyUsed(sieveOfEratosthenes, alreadyUsed);

                    if (checkProgress) CheckProgress(true, date, M);

                    for (int count = 3; count <= M; count++)
                    {
                        sieveOfEratosthenes = GroupBuilder(N, alreadyUsed);

                        if (checkProgress) CheckProgress(false, date, M, count);

                        filePath = ChangeFileName(count, filePath);
                        sw = new StreamWriter(filePath);
                        WriteGroup(sw, hugeN, count, sieveOfEratosthenes, alreadyUsed);
                        sw.Flush();
                        alreadyUsed = MarkAlreadyUsed(sieveOfEratosthenes, alreadyUsed);

                        if (checkProgress) CheckProgress(true, date, M, count);
                    }
                }

                timeSpan = DateTime.Now.Subtract(date);
                //sw.Dispose();

                if (YNRequest("\nВы хотите архивировать результат?"))
                    Archive(hugeN, M);

            }

            Console.WriteLine($"\n\nM = {M}");
            Console.WriteLine($"Время выполнения = {timeSpan.TotalSeconds }ms\n");
        }
    }
}
