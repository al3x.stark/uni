﻿using System;
using System.Collections.Generic;

namespace Homework_array
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("\n///////////////////////////////////////////////");
                Console.WriteLine("\n1. Доход/Расход/Результат.\n1. Треугольник Паскаля.\n3. Матрицы.");
                Console.Write("\nВведите номер: ");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("\n///////////////////////////////////////////////");

                        #region " Доход/Расход/Результат "
                        int number;
                        Console.Write("\nВведте число месяцев: ");

                        while (!Int32.TryParse(Console.ReadLine(), out number) || number < 4)
                        {
                            if (number < 4)
                                Console.WriteLine("Минимальное число - 4");
                            else
                                Console.WriteLine("Ошибка");

                            Console.Write("\nВведте число месяцев: ");
                        }
                        int[] income = new int[number];
                        int[] expens = new int[income.Length];
                        int[] result = new int[income.Length];

                        List<int> badMounth = new List<int>();
                        int worse = 1;
                        int goodMounthCounter = income.Length;
                        int min_c, min, start = 0, mounth;
                        bool check = false;

                        Random rnd = new Random();

                        Console.WriteLine("\n{0,7}{1,18}{2,20}{3,20}\n", "Месяц", "Доходы", "Издержки", "Итог");

                        for (int ctr = 0; ctr < income.Length; ctr++)
                        {
                            income[ctr] = rnd.Next(50, 200) * 1000;
                            expens[ctr] = rnd.Next(0, 150) * 1000;
                            result[ctr] = income[ctr] - expens[ctr];
                            Console.WriteLine("{0,5}{1,20:0,0}{2,20:0,0}{3,20:0,0}", ctr + 1, income[ctr], expens[ctr], result[ctr]);
                        }

                        min = result[0];
                        mounth = 0;

                        for (int ctr = 1; ctr < result.Length; ctr++)
                        {
                            if (result[ctr] < min)
                            {
                                min = result[ctr];
                                mounth = ctr;
                            }
                        }

                        badMounth.Add(mounth);

                        for (int ctr = mounth+1; ctr < result.Length; ctr++)
                            if (result[ctr] == min)
                                badMounth.Add(ctr);

                        min_c = min;
                        while (worse < 3 && badMounth.Count < result.Length)
                        {
                            for (int ctr = 0; ctr < result.Length; ctr++)
                            {
                                if (result[ctr] > min)
                                {
                                    min = result[ctr];
                                    mounth = ctr;
                                    break;
                                }
                            }

                            for (int ctr = 0; ctr < result.Length; ctr++)
                            {
                                if (result[ctr] < min && result[ctr] > min_c)
                                {
                                    min = result[ctr];
                                    mounth = ctr;
                                }
                            }

                            badMounth.Add(mounth);
                            worse++;

                            for (int ctr = mounth + 1; ctr < result.Length; ctr++)
                                if (result[ctr] == min)
                                    badMounth.Add(ctr);

                            min_c = min;
                        }

                        string badMounthStr = "";
                        for (int i = 0; i < badMounth.Count; i++)
                        {
                            badMounthStr += Convert.ToString(badMounth[i] + 1);
                            if (i != badMounth.Count - 1)
                                badMounthStr += ", ";
                        }
                        Console.WriteLine($"\nМесяцы с наихудшим итогом: {badMounthStr}");

                        for (int i = 0; i < result.Length; i++)
                        {
                            if (result[i] < 1)
                                goodMounthCounter--;
                        }

                        Console.WriteLine($"Число месяцев с положительным итогом: {goodMounthCounter}\n\n");

                        #endregion

                        break;
                    case "2":
                        Console.WriteLine("\n///////////////////////////////////////////////");

                        #region " Треугольник Паскаля "
                        Console.Write("\nВведите число ступеней треугольника Паскаля: ");
                        int numb;
                        while (!Int32.TryParse(Console.ReadLine(), out numb) || numb < 1 || numb > 25)
                        {
                            if (numb < 1)
                                Console.WriteLine("Минимальное число - 1");
                            else if (numb > 25)
                                Console.WriteLine("Максимальное число - 25");
                            else
                                Console.WriteLine("Ошибка");

                            Console.WriteLine("\nВведите число ступеней треугольника Паскаля: ");
                        }
                        Console.WriteLine("\n");
                        int bricksCount = numb + 1;
                        decimal n, k, n_f, k_f, nk, nk_f;

                        int intent;
                        bricksCount = 1;
                        for (int i = 0; i < numb; i++)
                        {
                            intent = numb - i;
                            for (int count = 0; count < intent / 2; count++)
                                Console.Write("        ");
                            if (intent % 2 != 0)
                                Console.Write("    ");

                            n = i;
                            n_f = 1;
                            while (n > 0)
                            {
                                n_f *= n;
                                n--;
                            }

                            for (int j = 0; j < bricksCount; j++)
                            {
                                if (j == 0 || j == bricksCount - 1)
                                    Console.Write("{0,-8}", "1");
                                else
                                {
                                    k = j;
                                    k_f = 1;
                                    while (k > 0)
                                    {
                                        k_f *= k;
                                        k--;
                                    }

                                    nk = i - j;
                                    nk_f = 1;
                                    while (nk > 0)
                                    {
                                        nk_f *= nk;
                                        nk--;
                                    }

                                    Console.Write("{0,-8}", Convert.ToString(n_f / k_f / nk_f));
                                }
                            }

                            Console.WriteLine();
                            bricksCount++;
                        }
                        #endregion

                        break;
                    case "3":
                        Console.WriteLine("\n///////////////////////////////////////////////");

                        #region " Матрицы "
                        int f_str, s_str, f_col, s_col;
                        int i_count = 0, j_count = 0, cell;

                        Console.Write("\nВведите число строк первой матрицы: ");
                        while (!Int32.TryParse(Console.ReadLine(), out f_str) || f_str < 1)
                        {
                            if (f_str < 1)
                                Console.WriteLine("Минимальное число - 1");
                            else
                                Console.WriteLine("Ошибка");

                            Console.Write("\nВведите число строк первой матрицы: ");
                        }
                        Console.Write("Введите число столбцов первой матрицы: ");
                        while (!Int32.TryParse(Console.ReadLine(), out f_col) || f_col < 1)
                        {
                            if (f_col < 1)
                                Console.WriteLine("Минимальное число - 1");
                            else
                                Console.WriteLine("Ошибка");

                            Console.Write("\nВведите число столбцов первой матрицы: ");
                        }

                        int[,] first_mx = new int[f_str, f_col];
                        Console.WriteLine();
                        foreach (int node in first_mx)
                        {
                            for (int i = 0; i < f_str; i++)
                            {
                                Console.Write("|");

                                for (int j = 0; j < f_col; j++)
                                {
                                    if (i < i_count || (i == i_count && j < j_count))
                                        Console.Write("{0,5}", first_mx[i, j]);
                                    else Console.Write("    _");
                                }
                                Console.Write("    |\n");
                            }

                            Console.Write($"\nВведите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                            while (!Int32.TryParse(Console.ReadLine(), out cell))
                            {
                                Console.WriteLine("Ошибка\n");
                                Console.Write($"Введите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                            }
                            first_mx[i_count, j_count] = cell;

                            if (j_count == f_col - 1)
                            {
                                i_count++;
                                j_count = 0;
                            }
                            else j_count++;
                            Console.WriteLine();
                        }

                        for (int i = 0; i < f_str; i++)
                        {
                            Console.Write("|");
                            for (int j = 0; j < f_col; j++)
                                Console.Write("{0,5}", first_mx[i, j]);
                            Console.Write("    |\n");
                        }

                        i_count = 0;
                        j_count = 0;

                        bool check_mx = false;
                        Console.WriteLine("\nКакую операцию вы хотете произвести?\n");
                        Console.WriteLine("1. Операция с матрицей\n2. Умножение на число");
                        while (!check_mx)
                        {
                            Console.Write("\nВыберите операцию: ");

                            switch (Console.ReadLine())
                            {
                                case "1":

                                    #region " 2 матрицы "
                                    int opr_n;
                                    check_mx = true;
                                    Console.WriteLine("\n1. Сложение\n2. Вычитание\n3. Умножение\n");
                                    Console.Write("Выберите операцию: ");
                                    while (!Int32.TryParse(Console.ReadLine(), out opr_n) || opr_n < 1 || opr_n > 3)
                                    {
                                        Console.WriteLine("Ошибка");
                                        Console.Write("\nВыберите операцию: ");
                                    }
                                    if (opr_n == 3)
                                    {
                                        s_str = f_col;
                                        Console.Write("\nВведите число столбцов второй матрицы: ");
                                        while (!Int32.TryParse(Console.ReadLine(), out s_col) || s_col < 1)
                                        {
                                            if (s_col < 1)
                                                Console.WriteLine("Минимальное число - 1");
                                            else
                                                Console.WriteLine("Ошибка");

                                            Console.Write("\nВведите число столбцов второй матрицы: ");
                                        }
                                    }
                                    else
                                    {
                                        s_str = f_str;
                                        s_col = f_col;
                                    }

                                    char sign = ' ';
                                    switch (opr_n)
                                    {
                                        case 1:
                                            sign = '+';
                                            break;
                                        case 2:
                                            sign = '-';
                                            break;
                                        case 3:
                                            sign = '*';
                                            break;
                                    }


                                    int[,] second_mx = new int[s_str, s_col];
                                    int i_str;
                                    Console.WriteLine();

                                    if (s_str > f_str)
                                        i_str = (s_str - f_str) / 2;
                                    else
                                        i_str = (f_str - s_str) / 2;

                                    foreach (int node in second_mx)
                                    {

                                        if (f_str < s_str)
                                        {
                                            for (int i = 0; i < s_str; i++)
                                            {
                                                if (i < i_str || i >= i_str + f_str)
                                                {
                                                    Console.Write(" ");
                                                    for (int j = 0; j <= f_col; j++)
                                                        Console.Write("     ");
                                                }
                                                else if (i - i_str < f_str)
                                                {
                                                    Console.Write("|");
                                                    for (int j = 0; j < f_col; j++)
                                                        Console.Write("{0,5}", first_mx[i - i_str, j]);
                                                    Console.Write("    |");
                                                }

                                                if (i == s_str / 2) Console.Write("    {0}    ", sign);
                                                else Console.Write("         ");

                                                Console.Write("|");
                                                for (int j = 0; j < s_col; j++)
                                                {
                                                    if (i < i_count || (i == i_count && j < j_count))
                                                        Console.Write("{0,5}", second_mx[i, j]);
                                                    else Console.Write("    _");
                                                }
                                                Console.Write("    |");

                                                if (i == s_str / 2) Console.Write("    =    ");
                                                else Console.Write("         ");

                                                if (i < i_str || i >= i_str + f_str)
                                                    Console.Write("\n");
                                                else if (i - i_str < f_str)
                                                {
                                                    Console.Write("|");
                                                    for (int j = 0; j < s_col; j++)
                                                        Console.Write("{0,5}", '?');
                                                    Console.Write("    |\n");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int i = 0; i < f_str; i++)
                                            {
                                                Console.Write("|");
                                                for (int j = 0; j < f_col; j++)
                                                    Console.Write("{0,5}", first_mx[i, j]);
                                                Console.Write("    |");

                                                if (i < i_str || i >= i_str + s_str)
                                                    for (int j = 0; j <= s_col + 2; j++)
                                                        Console.Write("     ");
                                                else if (i - i_str < s_str)
                                                {
                                                    if (i - i_str == s_str / 2) Console.Write("    {0}    ", sign);
                                                    else Console.Write("         ");
                                                    Console.Write("|");
                                                    for (int j = 0; j < s_col; j++)
                                                    {
                                                        if (i - i_str < i_count || (i - i_str == i_count && j < j_count))
                                                            Console.Write("{0,5}", second_mx[i - i_str, j]);
                                                        else Console.Write("    _");
                                                    }
                                                    Console.Write("    |");
                                                }

                                                if (i - i_str == s_str / 2) Console.Write("    =    ");
                                                else Console.Write("         ");

                                                Console.Write("|");
                                                for (int j = 0; j < s_col; j++)
                                                    Console.Write("{0,5}", '?');
                                                Console.Write("    |\n");
                                            }
                                        }

                                        Console.Write($"\nВведите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                                        while (!Int32.TryParse(Console.ReadLine(), out cell))
                                        {
                                            Console.WriteLine("Ошибка\n");
                                            Console.Write($"Введите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                                        }
                                        second_mx[i_count, j_count] = cell;

                                        if (j_count == s_col - 1)
                                        {
                                            i_count++;
                                            j_count = 0;
                                        }
                                        else j_count++;
                                        Console.WriteLine();
                                    }

                                    int[,] result_mx = new int[f_str, s_col];
                                    switch (sign)
                                    {
                                        case '+':
                                            for (int i = 0; i < f_str; i++)
                                                for (int j = 0; j < s_col; j++)
                                                    result_mx[i, j] = first_mx[i, j] + second_mx[i, j];
                                            break;
                                        case '-':
                                            for (int i = 0; i < f_str; i++)
                                                for (int j = 0; j < s_col; j++)
                                                    result_mx[i, j] = first_mx[i, j] - second_mx[i, j];
                                            break;
                                        case '*':
                                            int x = 0;
                                            for (int i = 0; i < f_str; i++)
                                            {
                                                for (int j = 0; j < s_col; j++)
                                                {
                                                    for (int g = 0; g < f_col; g++)
                                                        x += first_mx[i, g] * second_mx[g, j];
                                                    result_mx[i, j] = x;
                                                    x = 0;
                                                }
                                            }
                                            break;
                                    }

                                    if (f_str < s_str)
                                    {
                                        for (int i = 0; i < s_str; i++)
                                        {
                                            if (i < i_str || i >= i_str + f_str)
                                            {
                                                Console.Write(" ");
                                                for (int j = 0; j <= f_col; j++)
                                                    Console.Write("     ");
                                            }
                                            else if (i - i_str < f_str)
                                            {
                                                Console.Write("|");
                                                for (int j = 0; j < f_col; j++)
                                                    Console.Write("{0,5}", first_mx[i - i_str, j]);
                                                Console.Write("    |");
                                            }

                                            if (i == s_str / 2) Console.Write("    {0}    ", sign);
                                            else Console.Write("         ");

                                            Console.Write("|");
                                            for (int j = 0; j < s_col; j++)
                                                Console.Write("{0,5}", second_mx[i, j]);
                                            Console.Write("    |");

                                            if (i == s_str / 2) Console.Write("    =    ");
                                            else Console.Write("         ");

                                            if (i < i_str || i >= i_str + f_str)
                                                Console.Write("\n");
                                            else if (i - i_str < f_str)
                                            {
                                                Console.Write("|");
                                                for (int j = 0; j < s_col; j++)
                                                    Console.Write("{0,5}", result_mx[i, j]);
                                                Console.Write("    |\n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < f_str; i++)
                                        {
                                            Console.Write("|");
                                            for (int j = 0; j < f_col; j++)
                                                Console.Write("{0,5}", first_mx[i, j]);
                                            Console.Write("    |");

                                            if (i < i_str || i >= i_str + s_str)
                                                for (int j = 0; j <= s_col + 2; j++)
                                                    Console.Write("     ");
                                            else if (i - i_str < s_str)
                                            {
                                                if (i - i_str == s_str / 2) Console.Write("    {0}    ", sign);
                                                else Console.Write("         ");
                                                Console.Write("|");
                                                for (int j = 0; j < s_col; j++)
                                                    Console.Write("{0,5}", second_mx[i - i_str, j]);
                                                Console.Write("    |");
                                            }

                                            if (i - i_str == s_str / 2) Console.Write("    =    ");
                                            else Console.Write("         ");

                                            Console.Write("|");
                                            for (int j = 0; j < s_col; j++)
                                                Console.Write("{0,5}", result_mx[i, j]);
                                            Console.Write("    |\n");
                                        }
                                    }
                                    #endregion

                                    break;
                                case "2":

                                    #region " Умножение на число "
                                    check_mx = true;
                                    int numb_mx;
                                    result_mx = new int[f_str, f_col];

                                    Console.WriteLine("");
                                    for (int i = 0; i < f_str; i++)
                                    {
                                        Console.Write("|");
                                        for (int j = 0; j < f_col; j++)
                                            Console.Write("{0,5}", first_mx[i, j]);
                                        Console.Write("    |");

                                        if (i != f_str / 2)
                                            Console.Write("                   ");
                                        else
                                            Console.Write("    *    ?    =    ");

                                        Console.Write("|");
                                        for (int j = 0; j < f_col; j++)
                                            Console.Write("{0,5}", '?');
                                        Console.Write("    |\n");
                                    }
                                    Console.Write("\nВведите число: ");
                                    while (!Int32.TryParse(Console.ReadLine(), out numb_mx))
                                    {
                                        Console.WriteLine("Ошибка");
                                        Console.Write("\nВведите число: ");
                                    }

                                    for (
                                        int i = 0; i < f_str; i++)
                                        for (int j = 0; j < f_col; j++)
                                            result_mx[i, j] = first_mx[i, j] * numb_mx;

                                    Console.WriteLine("");
                                    for (int i = 0; i < f_str; i++)
                                    {
                                        Console.Write("|");
                                        for (int j = 0; j < f_col; j++)
                                            Console.Write("{0,5}", first_mx[i, j]);
                                        Console.Write("    |");

                                        if (i != f_str / 2)
                                            Console.Write("                   ");
                                        else
                                            Console.Write("    *    {0}    =    ", numb_mx);

                                        Console.Write("|");
                                        for (int j = 0; j < f_col; j++)
                                            Console.Write("{0,5}", result_mx[i, j]);
                                        Console.Write("    |\n");
                                    }
                                    #endregion

                                    break;
                                default:
                                    Console.WriteLine("Некорректный выбор. Повторите ввод.");
                                    break;
                            }
                        }
                        #endregion

                        break;

                    default:
                        Console.WriteLine("Некорректный номер.\n");
                        break;
                }
            }
        }
    }
}
