﻿using System;

namespace Homework_2
{
    class Program
    {
        static void Main(string[] args)
        {

            //  Подготовка переменных для вывода //


            //  ФИО
            string name = "Иванов Иван Иванович";
            string nameLine = "Имя: " + name;

            //  Возраст
            byte age = 15;                                  
            string ageLine = "Возраст: {0}";                                        //  Форматирование

            //  Рост
            double height = 170.5;                          
            string heightLine = $"Рост: {height}\n";                                //  Интерполяция строк

            //  Оценки
            int math = 5, russian = 4; long history = 2;    
            string marksLine = "Оценки:";
            string mathLine = $"Математика: {math} ";                               //  Интерполяция строк
            string russianLine = $"Русский язык: {russian}";                        //  Интерполяция строк
            string historyLine = $"История: {history}";                             //  Интерполяция строк

            //  Среднее значение
            float average = ((float)math + (float)russian + (float)history) / 3;
            string averageLine = $"Средний балл = {average:#.##}";                  //  Интерполяция строк


            // Вывод //


            Console.SetCursorPosition((Console.WindowWidth - nameLine.Length) / 2, Console.WindowHeight / 2 - 4);      //  Установка текста на центр экрана консоли
            Console.WriteLine(nameLine);

            Console.SetCursorPosition((Console.WindowWidth - ageLine.Length - 2) / 2, Console.WindowHeight / 2 - 3);   //  Установка текста на центр экрана консоли
            Console.WriteLine(ageLine, age);

            Console.SetCursorPosition((Console.WindowWidth - heightLine.Length) / 2, Console.WindowHeight / 2 - 2);    //  Установка текста на центр экрана консоли
            Console.WriteLine(heightLine);

            Console.SetCursorPosition((Console.WindowWidth - marksLine.Length) / 2, Console.WindowHeight / 2 - 1);     //  Установка текста на центр экрана консоли
            Console.WriteLine(marksLine);

            Console.SetCursorPosition((Console.WindowWidth - mathLine.Length) / 2, Console.WindowHeight / 2);          //  Установка текста на центр экрана консоли
            Console.WriteLine(mathLine);

            Console.SetCursorPosition((Console.WindowWidth - russianLine.Length) / 2, Console.WindowHeight / 2 + 1);   //  Установка текста на центр экрана консоли
            Console.WriteLine(russianLine);

            Console.SetCursorPosition((Console.WindowWidth - historyLine.Length) / 2, Console.WindowHeight / 2 + 2);   //  Установка текста на центр экрана консоли
            Console.WriteLine(historyLine);

            Console.SetCursorPosition((Console.WindowWidth - averageLine.Length) / 2, Console.WindowHeight / 2 + 3);   //  Установка текста на центр экрана консоли
            Console.WriteLine(averageLine);


            Console.ReadKey();

        }
    }
}
