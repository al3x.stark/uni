﻿using System;

namespace Homework_Game
{
    class Program
    {
        static void Main(string[] args)
        {

            bool exit = false;
            bool min_main_namber_incorrect = true;
            int min_main_number;

            ////  Меню  ////
            while (!exit)
            {
                string menu_number;
                bool menu_number_incorrect = true;
                Console.WriteLine("------------ МЕНЮ -----------\n");
                Console.WriteLine("1. Одииночная игра");
                Console.WriteLine("2. Совместная игра\n");
                Console.WriteLine("0. Выход");
                Console.WriteLine("\nВыберите пункт меню.");

                while (menu_number_incorrect)
                {
                    Console.WriteLine("Введите номер: ");
                    menu_number = Console.ReadLine();
                    switch (menu_number)
                    {
                        case "0":
                            Console.WriteLine("\nСпасибо за игру!\n");
                            menu_number_incorrect = false;
                            exit = true;
                            break;
                        case "1":
                            #region Single play

                            menu_number_incorrect = false;
                            min_main_namber_incorrect = true;
                            min_main_number = 0;

                            Console.WriteLine("\n------------ ОДИНОЧНАЯ ИГРА -----------");
                            Console.WriteLine("\nВведите имя:");
                            string name = Console.ReadLine();


                            #region Data input
                            Console.WriteLine($"\nПривет {name}!");
                            Console.WriteLine("Давайте установим диапазон для изначального числа.\n" +
                               "Из этого диапазона случайным образом выберется ваше число.");
                            Console.WriteLine("Введите \"0\", чтобы вернуться в меню.\n");

                            ////  Ввод диапазона главного числа  ////
                            while (min_main_namber_incorrect)
                            {
                               Console.WriteLine("Введите наименьшее число: ");
                                if (!int.TryParse(Console.ReadLine(), out min_main_number))
                                    Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                else if (min_main_number < 0)
                                    Console.WriteLine("Введено отрицательное число.");
                                else min_main_namber_incorrect = false;
                            }

                            if (min_main_number != 0)
                            {
                                bool max_main_namber_incorrect = true;
                                int max_main_number = 0;

                                while (max_main_namber_incorrect)
                                {
                                    Console.WriteLine("Введите наибольшее число: ");
                                    if (!int.TryParse(Console.ReadLine(), out max_main_number))
                                        Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                    else if (max_main_number < 0)
                                        Console.WriteLine("Введено отрицательное число.");
                                    else if (max_main_number < min_main_number)
                                        Console.WriteLine("Максимальное число меньше минимального.\n");
                                    else max_main_namber_incorrect = false;
                                }

                                if (max_main_number != 0)
                                {
                                    Console.WriteLine($"\nДиапазон: {min_main_number} - {max_main_number}\n");

                                    ////  Случайный выбор главного числа  ////
                                    Random rnd = new Random();
                                    int main_number = rnd.Next(min_main_number, max_main_number);
                                    Console.WriteLine($"Ваше число: {main_number}\n");

                                    ////  Ввод диапазона используемого числа  ////
                                    bool min_used_namber_incorrect = true;
                                    int min_used_number = 0;

                                    Console.WriteLine("Теперь установим диапазон для используемых чисел.");
                                    Console.WriteLine("Введите \"0\", чтобы вернуться в меню.\n");

                                    while (min_used_namber_incorrect)
                                    {
                                        Console.WriteLine("Введите наименьшее число: ");
                                        if (!int.TryParse(Console.ReadLine(), out min_used_number))
                                            Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                        else if (min_used_number < 0)
                                            Console.WriteLine("Введено отрицательное число.");
                                        else if (min_used_number >= main_number)
                                            Console.WriteLine("Введенное вами число превышает или равно основному числу.");
                                        else min_used_namber_incorrect = false;
                                    }

                                    if (min_used_number != 0)
                                    {
                                        bool max_used_namber_incorrect = true;
                                        int max_used_number = 0;

                                        while (max_used_namber_incorrect)
                                        {
                                            Console.WriteLine("Введите наибольшее число: ");
                                            if (!int.TryParse(Console.ReadLine(), out max_used_number))
                                                Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                            else if (max_used_number < 0)
                                                Console.WriteLine("Введено отрицательное число.");
                                            else if (max_used_number < min_used_number)
                                                Console.WriteLine("Максимальное число меньше минимального.\n");
                                            else if (max_used_number >= main_number)
                                                Console.WriteLine("Введенное вами число превышает или равно основному числу.");
                                            else max_used_namber_incorrect = false;
                                        }

                                        if (max_used_number != 0)
                                        {
                                            Console.WriteLine($"\nДиапазон: {min_used_number} - {max_used_number}\n");
                                            #endregion
                                            
                                            //// Выбор уровня сложности  ////
                                            bool difficulty_incorrect = true;

                                            Console.WriteLine("Выберите уровень сложности:");
                                            Console.WriteLine("1. Простой.");
                                            Console.WriteLine("2. Сложный.\n");
                                            Console.WriteLine("0. Меню.\n");
                                            while (difficulty_incorrect)
                                            {
                                                Console.WriteLine("Введите номер: ");
                                                string difficulty = Console.ReadLine();

                                                if (difficulty == "0")
                                                    difficulty_incorrect = false;
                                                else if (difficulty == "1")
                                                {
                                                    #region Easy
                                                    difficulty_incorrect = false;
                                                    int rnd_number;
                                                    Console.WriteLine("\n------------ СТАРТ ------------\n");
                                                    Console.WriteLine($"Ваше число: {main_number}");
                                                    Console.WriteLine($"Диапазон используемых чисел: {min_used_number} - {max_used_number}");

                                                    bool end_of_a_game = false;
                                                    int help = 0;

                                                    while (!end_of_a_game)
                                                    {

                                                        ////  Ход игрока  ////
                                                        int player_number = 0;
                                                        bool player_number_incorrect = true;
                                                        rnd_number = 0;
                                                        help++;

                                                        Console.WriteLine($"\nХодит \"{name}\".");
                                                        if (help == 3)
                                                        {
                                                            Console.WriteLine("Если хотите выйти, введите \"0\".");     //  Вывод справочной информации
                                                            help = 0;
                                                        }
                                                        Console.WriteLine($"Текущее число: {main_number}");

                                                        while (player_number_incorrect)
                                                        {
                                                            Console.WriteLine("\nВведите число из диапазона " +
                                                                $"{min_used_number} - {max_used_number} которое хотите вычесть.");
                                                            if (!int.TryParse(Console.ReadLine(), out player_number))
                                                                Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                                            else if (player_number == 0)
                                                                break;
                                                            else if (player_number < min_used_number || player_number > max_used_number)
                                                                Console.WriteLine("Число не входит в диапазон.");
                                                            else player_number_incorrect = false;
                                                        }

                                                        if (player_number == 0)
                                                            break;

                                                        main_number -= player_number;
                                                        if (main_number < 1)
                                                        {
                                                            Console.WriteLine($"\nПобедитель - \"{name}\"\n");
                                                            end_of_a_game = true;
                                                            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.\n");
                                                            Console.ReadKey();
                                                            break;
                                                        }
                                                        else
                                                        {

                                                            ////  Ход ИИ  ////
                                                            Console.WriteLine("\nХодит \"ИИ\".");
                                                            Console.WriteLine($"Текущее число: {main_number}");
                                                            rnd_number = rnd.Next(min_used_number, max_used_number);
                                                            Console.WriteLine($"\n\"ИИ\" отнимает {rnd_number}");
                                                            main_number -= rnd_number;
                                                        }
                                                        if (main_number < 1)
                                                        {
                                                            Console.WriteLine($"\nПобедитель - \"ИИ\"\n");
                                                            end_of_a_game = true;
                                                            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.\n");
                                                            Console.ReadKey();
                                                            break;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else if (difficulty == "2")
                                                {
                                                    #region Hard
                                                    difficulty_incorrect = false;
                                                    Console.WriteLine("------------ СТАРТ ------------\n");
                                                    Console.WriteLine($"Ваше число: {main_number}");
                                                    Console.WriteLine($"Диапазон используемых чисел: {min_used_number} - {max_used_number}");

                                                    bool end_of_a_game = false;
                                                    int help = 0;

                                                    while (!end_of_a_game)
                                                    {

                                                        ////  Ход игрока  ////
                                                        int player_number = 0;
                                                        bool player_number_incorrect = true;
                                                        help++;

                                                        Console.WriteLine($"\nХодит \"{name}\".");
                                                        if (help == 3)
                                                        {
                                                            Console.WriteLine("Если хотите выйти, введите \"0\".");     //  Вывод справочной информации
                                                            help = 0;
                                                        }
                                                        Console.WriteLine($"Текущее число: {main_number}");

                                                        while (player_number_incorrect)
                                                        {
                                                            Console.WriteLine("\nВведите число из диапазона " +
                                                                $"{min_used_number} - {max_used_number} которое хотите вычесть.");
                                                            if (!int.TryParse(Console.ReadLine(), out player_number))
                                                                Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                                            else if (player_number == 0)
                                                                break;
                                                            else if (player_number < min_used_number || player_number > max_used_number)
                                                                Console.WriteLine("Число не входит в диапазон.");
                                                            else player_number_incorrect = false;
                                                        }

                                                        if (player_number == 0)
                                                            break;

                                                        main_number -= player_number;
                                                        if (main_number < 1)
                                                        {
                                                            Console.WriteLine($"\nПобедитель - \"{name}\"\n");
                                                            end_of_a_game = true;
                                                            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.\n");
                                                            Console.ReadKey();
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            
                                                            ////  Ход ИИ  ////
                                                            Console.WriteLine("\nХодит \"ИИ\".");
                                                            Console.WriteLine($"Текущее число: {main_number}");

                                                            int range = 0;
                                                            bool win_range = false;
                                                            int seeking;

                                                            /// Определение диапазона ///
                                                            while (!win_range)
                                                            {
                                                                if (main_number >= (range * (min_used_number + max_used_number) + 1))
                                                                    if (main_number > (range * (min_used_number + max_used_number) + max_used_number))
                                                                        range++;
                                                                    else win_range = true;
                                                                else break;
                                                            }

                                                            /// Алгоритм при диапазоне победы ///
                                                            if (win_range)
                                                            {
                                                                range--;
                                                                seeking = main_number - (range * (min_used_number + max_used_number)
                                                                    + max_used_number + 1 + (min_used_number / 2));
                                                                if (seeking >= min_used_number && seeking <= max_used_number)
                                                                {
                                                                    Console.WriteLine($"\n\"ИИ\" отнимает {seeking}");
                                                                    main_number -= seeking;
                                                                }
                                                                else
                                                                {
                                                                    if (seeking < min_used_number)
                                                                    {
                                                                        main_number -= min_used_number;
                                                                        Console.WriteLine($"\n\"ИИ\" отнимает {min_used_number}");
                                                                    }
                                                                    else
                                                                    {
                                                                        main_number -= max_used_number;
                                                                        Console.WriteLine($"\n\"ИИ\" отнимает {max_used_number}");
                                                                    }
                                                                }
                                                            }

                                                            /// Алгоритм при диапазоне проигрыша ///
                                                            else
                                                            {
                                                                range--;
                                                                if (((range + 1) * (min_used_number + max_used_number) - main_number) <
                                                                    (main_number - range * (min_used_number + max_used_number) + max_used_number + 1))
                                                                {
                                                                    main_number -= min_used_number;
                                                                    Console.WriteLine($"\n\"ИИ\" отнимает {min_used_number}");
                                                                }
                                                                else
                                                                {
                                                                    main_number -= max_used_number;
                                                                    Console.WriteLine($"\n\"ИИ\" отнимает {max_used_number}");
                                                                }
                                                            }

                                                        }
                                                        if (main_number < 1)
                                                        {
                                                            Console.WriteLine($"\nПобедитель - \"ИИ\"\n");
                                                            end_of_a_game = true;
                                                            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.\n");
                                                            Console.ReadKey();
                                                            break;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            #endregion
                            break;
                        case "2":
                            #region Multi play
                            menu_number_incorrect = false;
                            int number_of_players = 0;
                            bool number_of_players_incorrect = true;
                            Console.WriteLine("------------ СОВМЕСТНАЯ ИГРА -----------\n");
                            Console.WriteLine("Введите число игроков или \"0\", чтобы вернуться в меню.\n");

                            #region Data input

                            ////  Ввод числа игроков  ////
                            while (number_of_players_incorrect)
                            {
                                Console.WriteLine("Введите число: ");
                                if (!int.TryParse(Console.ReadLine(), out number_of_players))
                                    Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                else if (number_of_players < 0)
                                    Console.WriteLine("Введено отрицательное число.");
                                else number_of_players_incorrect = false;
                            }

                            if (number_of_players != 0)
                            {

                                ////  Ввод имен  ////
                                bool name_incorrect = true;
                                string[] names = new string[number_of_players];
                                for (int i = 0; i < number_of_players; i++)
                                {
                                    name_incorrect = true;
                                    while (name_incorrect)
                                    {
                                        Console.WriteLine($"\nИгрок {i + 1}, введите свое имя.");
                                        names[i] = Console.ReadLine();
                                        name_incorrect = false;
                                        for (int j = 0; j < i; j++)
                                        {
                                            if (names[j] == names[i])
                                            {
                                                name_incorrect = true;
                                                break;
                                            }
                                        }
                                        if (name_incorrect)
                                            Console.WriteLine("Такое имя уже занято.");
                                    }
                                }

                                ////  Ввод диапазона главного числа  ////
                                min_main_namber_incorrect = true;
                                min_main_number = 0;

                                Console.WriteLine("\nДавайте установим диапазон для изначального числа.\n" +
                                    "Из этого диапазона случайным образом выберется ваше число.");
                                Console.WriteLine("Введите \"0\", чтобы вернуться в меню.\n");
                                while (min_main_namber_incorrect)
                                {
                                    Console.WriteLine("Введите наименьшее число: ");
                                    if (!int.TryParse(Console.ReadLine(), out min_main_number))
                                        Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                    else if (min_main_number < 0)
                                        Console.WriteLine("Введено отрицательное число.");
                                    else min_main_namber_incorrect = false;
                                }

                                if (min_main_number != 0)
                                {
                                    bool max_main_namber_incorrect = true;
                                    int max_main_number = 0;

                                    while (max_main_namber_incorrect)
                                    {
                                        Console.WriteLine("Введите наибольшее число: ");
                                        if (!int.TryParse(Console.ReadLine(), out max_main_number))
                                            Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                        else if (max_main_number < 0)
                                            Console.WriteLine("Введено отрицательное число.");
                                        else if (max_main_number < min_main_number)
                                            Console.WriteLine("Максимальное число меньше минимального.\n");
                                        else max_main_namber_incorrect = false;
                                    }

                                    if (max_main_number != 0)
                                    {
                                        Console.WriteLine($"\nДиапазон: {min_main_number} - {max_main_number}");

                                        ////  Случайный выбор главного числа  ////
                                        Random rnd = new Random();
                                        int main_number = rnd.Next(min_main_number, max_main_number);
                                        Console.WriteLine($"Ваше число: {main_number}\n");

                                        ////  Ввод диапазона используемого числа  ////
                                        bool min_used_namber_incorrect = true;
                                        int min_used_number = 0;

                                        Console.WriteLine("Теперь установим диапазон для используемых чисел.");
                                        Console.WriteLine("Введите \"0\", чтобы вернуться в меню.\n");

                                        while (min_used_namber_incorrect)
                                        {
                                            Console.WriteLine("Введите наименьшее число: ");
                                            if (!int.TryParse(Console.ReadLine(), out min_used_number))
                                                Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                            else if (min_used_number < 0)
                                                Console.WriteLine("Введено отрицательное число.");
                                            else if (min_used_number >= main_number)
                                                Console.WriteLine("Введенное вами число превышает или равно основному числу.");
                                            else min_used_namber_incorrect = false;
                                        }

                                        if (min_used_number != 0)
                                        {
                                            bool max_used_namber_incorrect = true;
                                            int max_used_number = 0;

                                            while (max_used_namber_incorrect)
                                            {
                                                Console.WriteLine("Введите наибольшее число: ");
                                                if (!int.TryParse(Console.ReadLine(), out max_used_number))
                                                    Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                                else if (max_used_number < 0)
                                                    Console.WriteLine("Введено отрицательное число.");
                                                else if (max_used_number < min_used_number)
                                                    Console.WriteLine("Максимальное число меньше минимального.\n");
                                                else if (max_used_number >= main_number)
                                                    Console.WriteLine("Введенное вами число превышает или равно основному числу.");
                                                else max_used_namber_incorrect = false;
                                            }

                                            if (max_used_number != 0)
                                            {
                                                Console.WriteLine($"\nДиапазон: {min_used_number} - {max_used_number}\n");

                                                #endregion

                                                Console.WriteLine("------------ СТАРТ ------------\n");
                                                Console.WriteLine($"Число игроков: {number_of_players}");
                                                Console.WriteLine($"Ваше число: {main_number}");
                                                Console.WriteLine($"Диапазон используемых чисел: {min_used_number} - {max_used_number}");

                                                bool end_of_a_game = false;
                                                int help = 0;

                                                while (!end_of_a_game)
                                                {
                                                    help++;
                                                    
                                                    for (int i = 0; i < number_of_players; i++)
                                                    {

                                                        //// Ход игрока ////
                                                        int player_number = 0;
                                                        bool player_number_incorrect = true;

                                                        Console.WriteLine($"\nХодит \"{names[i]}\"");
                                                        if (help == 2)
                                                        {
                                                            Console.WriteLine("Если хотите выйти, введите \"0\".");     //  Вывод справочной информации
                                                            help = 0;
                                                        }
                                                        Console.WriteLine($"Текущее число - {main_number}\n");

                                                        while (player_number_incorrect)
                                                        {
                                                            Console.WriteLine("Введите число из диапазона " +
                                                                $"{min_used_number} - {max_used_number} которое хотите вычесть.");
                                                            if (!int.TryParse(Console.ReadLine(), out player_number))
                                                                Console.WriteLine("Некорректный ввод. Повторите попытку.");
                                                            else if (player_number == 0)
                                                                break;
                                                            else if (player_number < min_used_number || player_number > max_used_number)
                                                                Console.WriteLine("Число не входит в диапазон.");
                                                            else player_number_incorrect = false;
                                                        }

                                                        if (player_number == 0)
                                                        {
                                                            end_of_a_game = true;
                                                            break;
                                                        }

                                                        main_number -= player_number;
                                                        if (main_number < 1)
                                                        {
                                                            Console.WriteLine($"\nПобедитель - \"{names[i]}\"\n");
                                                            end_of_a_game = true;
                                                            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить.\n");
                                                            Console.ReadKey();
                                                            break;
                                                        }                                                           
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                            break;
                        default:
                            Console.WriteLine("Некорректный ввод. Повторите попытку.");
                            break;
                    }
                }
            }
        }
    }
}
