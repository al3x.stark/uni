﻿using System;

namespace Homework_method
{
    /// <summary>
    /// Операции с матрицами
    /// </summary>
    class Matrix
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public Matrix() { Menu(); }

        /// <summary>
        /// Ввод количества строк в матрице
        /// </summary>
        /// <returns></returns>
        static int SetStrData()
        {
            int str;

            Console.Write("\nВведите число строк матрицы: ");
            while (!Int32.TryParse(Console.ReadLine(), out str) || str < 1)
            {
                if (str < 1)
                    Console.WriteLine("Минимальное число - 1");
                else
                    Console.WriteLine("Ошибка");

                Console.Write("\nВведите число строк матрицы: ");
            }

            return str;
        }

        /// <summary>
        /// Ввод количества столбцов в матрице
        /// </summary>
        /// <returns></returns>
        static int SetColData()
        {
            int col;

            Console.Write("Введите число столбцов матрицы: ");
            while (!Int32.TryParse(Console.ReadLine(), out col) || col < 1)
            {
                if (col < 1)
                    Console.WriteLine("Минимальное число - 1");
                else
                    Console.WriteLine("Ошибка");

                Console.Write("\nВведите число столбцов матрицы: ");
            }

            return col;
        }

        /// <summary>
        /// Заполнение матрицы
        /// </summary>
        /// <param name="str"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        static int[,] SetCellData(int str, int col)
        {
            int i_count = 0, j_count = 0, cell;
            int[,] mtrx = new int[str, col];
            Console.WriteLine();
            foreach (int node in mtrx)
            {
                for (int i = 0; i < str; i++)
                {
                    Console.Write("|");

                    for (int j = 0; j < col; j++)
                    {
                        if (i < i_count || (i == i_count && j < j_count))
                            Console.Write("{0,5}", mtrx[i, j]);
                        else Console.Write("    _");
                    }
                    Console.Write("    |\n");
                }

                Console.Write($"\nВведите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                while (!Int32.TryParse(Console.ReadLine(), out cell))
                {
                    Console.WriteLine("Ошибка\n");
                    Console.Write($"Введите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                }
                mtrx[i_count, j_count] = cell;

                if (j_count == col - 1)
                {
                    i_count++;
                    j_count = 0;
                }
                else j_count++;
                Console.WriteLine();
            }

            return mtrx;
        }

        /// <summary>
        /// Графический вывод матрицы
        /// </summary>
        /// <param name="mtrx"></param>
        static void ShowMtrx(int[,] mtrx)
        {

            for (int i = 0; i < mtrx.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < mtrx.GetLength(1); j++)
                    Console.Write("{0,5}", mtrx[i, j]);
                Console.Write("    |\n");
            }
        }

        /// <summary>
        /// Определение второй матрицы, подсчет суммы/разности матриц и вывод результата с возможностью его сохранения
        /// </summary>
        /// <param name="mtrx"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        static int[,] SumAndDiff(int[,] mtrx, char sign)
        {
            int str = mtrx.GetLength(0);
            int col = mtrx.GetLength(1);

            int[,] sec_mtrx = new int[str, col];

            int i_count = 0, j_count = 0;
            int[,] result = new int[str, col];

            foreach (int node in sec_mtrx)
            {

                for (int i = 0; i < str; i++)
                {
                    Console.Write("|");
                    for (int j = 0; j < col; j++)
                        Console.Write("{0,5}", mtrx[i, j]);
                    Console.Write("    |");

                    if (i == str / 2) Console.Write("    {0}    ", sign);
                    else Console.Write("         ");

                    Console.Write("|");
                    for (int j = 0; j < col; j++)
                    {
                        if (i < i_count || (i == i_count && j < j_count))
                            Console.Write("{0,5}", sec_mtrx[i, j]);
                        else Console.Write("    _");
                    }
                    Console.Write("    |");

                    if (i == str / 2) Console.Write("    =    ");
                    else Console.Write("         ");

                    Console.Write("|");
                    for (int j = 0; j < col; j++)
                        Console.Write("{0,5}", '?');
                    Console.Write("    |\n");

                }

                Console.Write($"\nВведите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                while (!Int32.TryParse(Console.ReadLine(), out sec_mtrx[i_count, j_count]))
                {
                    Console.WriteLine("Ошибка\n");
                    Console.Write($"Введите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                }

                if (j_count == col - 1)
                {
                    i_count++;
                    j_count = 0;
                }
                else j_count++;
                Console.WriteLine();
            }

            for (int i = 0; i < str; i++)
            {
                Console.Write("|");
                for (int j = 0; j < col; j++)
                    Console.Write("{0,5}", mtrx[i, j]);
                Console.Write("    |");

                if (i == str / 2) Console.Write("    {0}    ", sign);
                else Console.Write("         ");

                Console.Write("|");
                for (int j = 0; j < col; j++)
                {
                    if (i < i_count || (i == i_count && j < j_count))
                        Console.Write("{0,5}", sec_mtrx[i, j]);
                    else Console.Write("    _");
                }
                Console.Write("    |");

                if (i == str / 2) Console.Write("    =    ");
                else Console.Write("         ");

                Console.Write("|");
                switch (sign)
                {
                    case '+':
                        for (int j = 0; j < col; j++)
                        {
                            result[i, j] = mtrx[i, j] + sec_mtrx[i, j];
                            Console.Write("{0,5}", result[i, j]);
                        }
                        break;
                    case '-':
                        for (int j = 0; j < col; j++)
                        {
                            result[i, j] = mtrx[i, j] - sec_mtrx[i, j];
                            Console.Write("{0,5}", result[i, j]);
                        }
                        break;
                }
                Console.Write("    |\n");
            }

            bool check = false;
            Console.WriteLine("\nСохранить результат?");
            while (!check)
            {
                Console.WriteLine("\n(Y/N): ");
                switch (Console.ReadLine())
                {
                    case "Y":
                        check = true;
                        mtrx = result;
                        break;
                    case "N":
                        check = true;
                        break;
                    case "y":
                        check = true;
                        mtrx = result;
                        break;
                    case "n":
                        check = true;
                        break;
                    default:
                        Console.WriteLine("Ошибка.");
                        break;
                }
            }
            return mtrx;
        }

        /// <summary>
        /// Определение второй матрицы, подсчет произведения матриц и вывод результата с возможностью его сохранения
        /// </summary>
        /// <param name="mtrx"></param>
        /// <returns></returns>
        static int[,] Multi(int[,] mtrx)
        {

            int i_count = 0, j_count = 0, x = 0;
            int f_str = mtrx.GetLength(0);
            int f_col = mtrx.GetLength(1);
            int s_str = f_col, s_col;


            Console.Write("\nВведите число столбцов второй матрицы: ");
            while (!Int32.TryParse(Console.ReadLine(), out s_col) || s_col < 1)
            {
                if (s_col < 1)
                    Console.WriteLine("Минимальное число - 1");
                else
                    Console.WriteLine("Ошибка");

                Console.Write("\nВведите число столбцов второй матрицы: ");
            }

            int[,] sec_mtrx = new int[s_str, s_col];
            int[,] result = new int[f_str, s_col];
            int i_str;
            Console.WriteLine();

            if (s_str > f_str)
                i_str = (s_str - f_str) / 2;
            else
                i_str = (f_str - s_str) / 2;

            foreach (int node in sec_mtrx)
            {
                if (f_str < s_str)
                {
                    for (int i = 0; i < s_str; i++)
                    {

                        if (i < i_str || i >= i_str + f_str)
                        {
                            Console.Write(" ");
                            for (int j = 0; j <= f_col; j++)
                                Console.Write("     ");
                        }
                        else if (i - i_str < f_str)
                        {
                            Console.Write("|");
                            for (int j = 0; j < f_col; j++)
                                Console.Write("{0,5}", mtrx[i - i_str, j]);
                            Console.Write("    |");
                        }

                        if (i == s_str / 2) Console.Write("    *    ");
                        else Console.Write("         ");

                        Console.Write("|");
                        for (int j = 0; j < s_col; j++)
                        {
                            if (i < i_count || (i == i_count && j < j_count))
                                Console.Write("{0,5}", sec_mtrx[i, j]);
                            else Console.Write("    _");
                        }
                        Console.Write("    |");

                        if (i == s_str / 2) Console.Write("    =    ");
                        else Console.Write("         ");

                        if (i < i_str || i >= i_str + f_str)
                            Console.Write("\n");
                        else if (i - i_str < f_str)
                        {
                            Console.Write("|");
                            for (int j = 0; j < s_col; j++)
                                Console.Write("{0,5}", '?');
                            Console.Write("    |\n");
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < f_str; i++)
                    {
                        Console.Write("|");
                        for (int j = 0; j < f_col; j++)
                            Console.Write("{0,5}", mtrx[i, j]);
                        Console.Write("    |");

                        if (i < i_str || i >= i_str + s_str)
                            for (int j = 0; j <= s_col + 2; j++)
                                Console.Write("     ");
                        else if (i - i_str < s_str)
                        {
                            if (i - i_str == s_str / 2) Console.Write("    *    ");
                            else Console.Write("         ");
                            Console.Write("|");
                            for (int j = 0; j < s_col; j++)
                            {
                                if (i - i_str < i_count || (i - i_str == i_count && j < j_count))
                                    Console.Write("{0,5}", sec_mtrx[i - i_str, j]);
                                else Console.Write("    _");
                            }
                            Console.Write("    |");
                        }

                        if (i - i_str == s_str / 2) Console.Write("    =    ");
                        else Console.Write("         ");

                        Console.Write("|");
                        for (int j = 0; j < s_col; j++)
                            Console.Write("{0,5}", '?');
                        Console.Write("    |\n");
                    }
                }

                Console.Write($"\nВведите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                while (!Int32.TryParse(Console.ReadLine(), out sec_mtrx[i_count, j_count]))
                {
                    Console.WriteLine("Ошибка\n");
                    Console.Write($"Введите элемент строки {i_count + 1}, столбца {j_count + 1}: ");
                }

                if (j_count == s_col - 1)
                {
                    i_count++;
                    j_count = 0;
                }
                else j_count++;
                Console.WriteLine();
            }

            if (f_str < s_str)
            {
                for (int i = 0; i < s_str; i++)
                {
                    if (i < i_str || i >= i_str + f_str)
                    {
                        Console.Write(" ");
                        for (int j = 0; j <= f_col; j++)
                            Console.Write("     ");
                    }
                    else if (i - i_str < f_str)
                    {
                        Console.Write("|");
                        for (int j = 0; j < f_col; j++)
                            Console.Write("{0,5}", mtrx[i - i_str, j]);
                        Console.Write("    |");
                    }

                    if (i == s_str / 2) Console.Write("    *    ");
                    else Console.Write("         ");

                    Console.Write("|");
                    for (int j = 0; j < s_col; j++)
                        Console.Write("{0,5}", sec_mtrx[i, j]);
                    Console.Write("    |");

                    if (i == s_str / 2) Console.Write("    =    ");
                    else Console.Write("         ");

                    if (i < i_str || i >= i_str + f_str)
                        Console.Write("\n");
                    else if (i - i_str < f_str)
                    {
                        Console.Write("|");
                        for (int j = 0; j < s_col; j++)
                        {
                            for (int g = 0; g < f_col; g++)
                                x += mtrx[i, g] * sec_mtrx[g, j];
                            result[i, j] = x;
                            Console.Write("{0,5}", x);
                            x = 0;
                        }
                        Console.Write("    |\n");
                    }
                }
            }
            else
            {
                for (int i = 0; i < f_str; i++)
                {
                    Console.Write("|");
                    for (int j = 0; j < f_col; j++)
                        Console.Write("{0,5}", mtrx[i, j]);
                    Console.Write("    |");

                    if (i < i_str || i >= i_str + s_str)
                        for (int j = 0; j <= s_col + 2; j++)
                            Console.Write("     ");
                    else if (i - i_str < s_str)
                    {
                        if (i - i_str == s_str / 2) Console.Write("    *    ");
                        else Console.Write("         ");
                        Console.Write("|");
                        for (int j = 0; j < s_col; j++)
                            Console.Write("{0,5}", sec_mtrx[i - i_str, j]);
                        Console.Write("    |");
                    }

                    if (i - i_str == s_str / 2) Console.Write("    =    ");
                    else Console.Write("         ");

                    Console.Write("|");
                    for (int j = 0; j < s_col; j++)
                    {
                        for (int g = 0; g < f_col; g++)
                            x += mtrx[i, g] * sec_mtrx[g, j];
                        result[i, j] = x;
                        Console.Write("{0,5}", x);
                        x = 0;
                    }
                    Console.Write("    |\n");
                }
            }

            bool check = false;
            Console.WriteLine("\nСохранить результат?");
            while (!check)
            {
                Console.WriteLine("\n(Y/N): ");
                switch (Console.ReadLine())
                {
                    case "Y":
                        check = true;
                        mtrx = result;
                        break;
                    case "N":
                        check = true;
                        break;
                    case "y":
                        check = true;
                        mtrx = result;
                        break;
                    case "n":
                        check = true;
                        break;
                    default:
                        Console.WriteLine("Ошибка.");
                        break;
                }
            }
            return mtrx;
        }

        /// <summary>
        /// Умножение матрицы на число и вывод результата с возможностью его сохранения
        /// </summary>
        /// <param name="mtrx"></param>
        /// <returns></returns>
        static int[,] Times(int[,] mtrx)
        {
            int str = mtrx.GetLength(0);
            int col = mtrx.GetLength(1);
            int[,] result = new int[str, col];
            int numb;

            Console.WriteLine("");
            for (int i = 0; i < str; i++)
            {
                Console.Write("|");
                for (int j = 0; j < col; j++)
                    Console.Write("{0,5}", mtrx[i, j]);
                Console.Write("    |");

                if (i != str / 2)
                    Console.Write("                   ");
                else
                    Console.Write("    *    ?    =    ");

                Console.Write("|");
                for (int j = 0; j < col; j++)
                    Console.Write("{0,5}", '?');
                Console.Write("    |\n");
            }

            Console.Write("\nВведите число: ");
            while (!Int32.TryParse(Console.ReadLine(), out numb))
            {
                Console.WriteLine("Ошибка");
                Console.Write("\nВведите число: ");
            }

            Console.WriteLine();
            for (int i = 0; i < str; i++)
            {
                Console.Write("|");
                for (int j = 0; j < col; j++)
                    Console.Write("{0,5}", mtrx[i, j]);
                Console.Write("    |");

                if (i != str / 2)
                    Console.Write("                   ");
                else
                    Console.Write("    *    {0}    =    ", numb);

                Console.Write("|");
                for (int j = 0; j < col; j++)
                {
                    result[i, j] = mtrx[i, j] * numb;
                    Console.Write("{0,5}", result[i, j]);
                }
                Console.Write("    |\n");
            }

            bool check = false;
            Console.WriteLine("\nСохранить результат?");
            while (!check)
            {
                Console.WriteLine("\n(Y/N): ");
                switch (Console.ReadLine())
                {
                    case "Y":
                        check = true;
                        mtrx = result;
                        break;
                    case "N":
                        check = true;
                        break;
                    case "y":
                        check = true;
                        mtrx = result;
                        break;
                    case "n":
                        check = true;
                        break;
                    default:
                        Console.WriteLine("Ошибка.");
                        break;
                }
            }
            return mtrx;
        }

        /// <summary>
        /// Вывод меню с выбором операций
        /// </summary>
        static void Menu()
        {
            int[,] mtrx = SetCellData(SetStrData(), SetColData());
            bool check, exit = false;
            while (!exit)
            {
                check = false;
                Console.WriteLine("\n//////// ОПЕРАЦИИ С МАТРИЦАМИ ////////\n");
                ShowMtrx(mtrx);
                Console.WriteLine("\nКакую операцию вы хотете произвести?\n");
                Console.WriteLine("1. Сложение матриц\n2. Вычитание матриц\n3. Умножение матриц\n4. Умножение на число\n5. Новая матрица\n\n0. Выход");
                while (!check)
                {
                    Console.Write("\nВыберите операцию: ");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            mtrx = SumAndDiff(mtrx, '+');
                            check = true;
                            break;
                        case "2":
                            mtrx = SumAndDiff(mtrx, '-');
                            check = true;
                            break;
                        case "3":
                            mtrx = Multi(mtrx);
                            check = true;
                            break;
                        case "4":
                            mtrx = Times(mtrx);
                            check = true;
                            break;
                        case "5":
                            mtrx = SetCellData(SetStrData(), SetColData());
                            check = true;
                            break;
                        case "0":
                            check = true;
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Некорректный выбор. Повторите ввод.");
                            break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Операции со строкой
    /// </summary>
    class StrOpr
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public StrOpr() { Menu(); }

        /// <summary>
        /// Возвращает слова с минимальной/максимальной длиной
        /// </summary>
        /// <param name="str"></param>
        /// <param name="opr"></param>
        /// <returns></returns>
        static string GetWords(string str, bool opr)
        {
            string word = "", result;

            int i = 0;
            while (i < str.Length && (str[i] == ' ' || str[i] == ',' || str[i] == '.')) i++;
            if (i == str.Length) return "Слова не найдены.";
            while (i < str.Length && str[i] != ' ' && str[i] != ',' && str[i] != '.') { word += str[i]; i++; }
            int length = word.Length;
            result = word + ", ";
            word = "";

            while (i < str.Length)
            {
                while (i < str.Length && (str[i] == ' ' || str[i] == ',' || str[i] == '.')) i++;
                if (i == str.Length) break;
                while (i < str.Length && str[i] != ' ' && str[i] != ',' && str[i] != '.') { word += str[i]; i++; }

                if (opr)
                {
                    if (word.Length < length)
                    {
                        length = word.Length;
                        result = "";
                    }
                }
                else
                {
                    if (word.Length > length)
                    {
                        length = word.Length;
                        result = "";
                    }
                }

                if (word.Length == length)
                {
                    result = result + word + ", ";
                }
                word = "";
            }

            result = result.Remove(result.LastIndexOf(','), 2);
            return result;
        }

        /// <summary>
        /// Убирает повторяющиеся символы
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static string RemoveRepeat(string str)
        {
            char symbol = str[0];
            string result = "" + str[0];
            int check1, check2;

            foreach (char c in str)
            {
                check1 = c.CompareTo(symbol);
                if (check1 != 0)
                {
                    check2 = (int)c;
                    if ((check2 > 64 && check2 < 91) || (check2 > 1039 && check2 < 1072))
                    {
                        if (check1 != -32) { result += c; symbol = c; }
                    }
                    else if ((check2 > 96 && check2 < 123) || (check2 > 1071 && check2 < 1104))
                    {
                        if (check1 != 32) { result += c; symbol = c; }
                    }
                    else { result += c; symbol = c; }
                }
            }

            return result;
        }

        /// <summary>
        /// Вывод меню с выбором операций
        /// </summary>
        static void Menu()
        {
            bool check, exit = false;
            Console.Write("\nВведите строку: ");
            string str = Console.ReadLine();
            while (str == "")
            {
                Console.Write("Строка пуста.\n\nПовторите ввод: ");
                str = Console.ReadLine();
            }
            while (!exit)
            {
                check = false;
                Console.WriteLine("\n\n//////// ОПЕРАЦИИ СО СТРОКОЙ ////////\n");
                Console.WriteLine(str);
                Console.WriteLine("\nКакую операцию вы хотете произвести?\n");
                Console.WriteLine("1. Найти слова с минимальной длиной\n2. Найти слова с максимальной длиной\n3. Убрать повторяющиеся символы\n4. Изменить строку\n\n0. Выход");
                while (!check)
                {
                    Console.Write("\nВыберите операцию: ");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            Console.WriteLine("\nСлова с минимальной длиной: " + GetWords(str, true));
                            check = true;
                            break;
                        case "2":
                            Console.WriteLine("\nСлова с максимальной длиной: " + GetWords(str, false));
                            check = true;
                            break;
                        case "3":
                            Console.WriteLine("\nРезультат: " + RemoveRepeat(str));
                            check = true;
                            break;
                        case "4":
                            Console.Write("\nВведите строку: ");
                            str = Console.ReadLine();
                            while (str == "")
                            {
                                Console.Write("Строка пуста.\n\nПовторите ввод: ");
                                str = Console.ReadLine();
                            }
                            check = true;
                            break;
                        case "0":
                            check = true;
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Некорректный выбор. Повторите ввод.");
                            break;
                    }
                }
            }
        }
    
    }

    class Program
    {
        /// <summary>
        /// Возвращает строку-обозначение для найденной арифметической/геометрической прогрессии или сообщение о ее отсутствии
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="arr_str"></param>
        /// <param name="index"></param>
        /// <param name="opr"></param>
        /// <returns></returns>
        static string Progression(double[] arr, string arr_str, int index, bool opr)
        {
            string result = "";
            int start = index, end = index, count;
            double d = 0;
            bool found = false;
            for (count = index+2; count < arr.Length; count++)
            {
                end++;
                if (!found)
                    if (opr) d = arr[end] - arr[start];
                    else d = arr[end] / arr[start];
                if (opr)
                {
                    if (arr[count] - d != arr[end])
                    {
                        if (found) break;
                        start = end;
                    }
                    else found = true;
                }
                else
                {
                    if (arr[count] / d != arr[end])
                    {
                        if (found) break;
                        start = end;
                    }
                    else found = true;
                }
            }
            if (found && count == arr.Length)
                end = count;
            result += Convert.ToString(end) + " ";
            count = 0;
            char symbol = ' ';
            if (!found) return "Прогрессия не найдена.";
            else
            {
                foreach (char c in arr_str)
                {
                    if (count == end + 1) break;
                    if (count == start) symbol = '-';
                    result += symbol;
                    if (c == ' ') count++;
                }
                result = result.Remove(result.LastIndexOf("-"), 1);
                return result;
            }
        }

        /// <summary>
        /// Поиск в представленном множестве чисел арифметической и геометрической прогрессии и их вывод
        /// </summary>
        static void ArithGeo()
        {
            int n;
            Console.Write("\nВведите количество элементов в множестве чисел: ");
            while (!Int32.TryParse(Console.ReadLine(), out n) || n < 3)
            {
                if (n < 3) Console.WriteLine("Число элементов не может быть меньше трех\n");
                else Console.WriteLine("Ошибка\n");
                Console.Write("\nВведите количество элементов в множестве чисел: ");
            }
            Console.WriteLine("\n//////////////////////////////////////\n");
            double[] arr = new double[n];
            int i;
            string arr_str = "", res_str, ind_str;
            for (i = 0; i < n; i++)
            {
                Console.Write($"Введите число {i + 1}: ");
                while (!Double.TryParse(Console.ReadLine(), out arr[i]))
                {
                    Console.WriteLine("Ошибка\n");
                    Console.Write($"Введите число {i + 1}: ");
                }
                arr_str += Convert.ToString(arr[i]) + " ";
                Console.WriteLine("\n " + arr_str);
                Console.WriteLine();

            }
            int j, index;
            bool opr;
            for (int opr_i = 0; opr_i < 2; opr_i++)
            {
                if (opr_i == 0) opr = true;
                else opr = false;
                res_str = Progression(arr, arr_str, 0, opr);
                if (opr) Console.WriteLine("//////// АРИФМЕТИЧЕСКАЯ ПРОГРЕССИЯ ////////\n");
                else Console.WriteLine("//////// ГЕОМЕТРИЧЕСКАЯ ПРОГРЕССИЯ ////////\n");
                
                if (res_str != "Прогрессия не найдена.")
                {
                    Console.WriteLine("Прогрессия найдена на следующих оотрезках:\n");
                    i = 1;
                    while (res_str != "Прогрессия не найдена.")
                    {
                        j = 0; ind_str = "";
                        while (res_str[j] != ' ') { ind_str += res_str[j]; j++; }
                        index = Convert.ToInt32(ind_str);
                        res_str = res_str.Remove(0, j + 1);
                        Console.WriteLine($" {i}:\n\n  {arr_str}\n  {res_str}");
                        Console.WriteLine();
                        i++;
                        res_str = Progression(arr, arr_str, index, opr);
                    }
                }
                else Console.WriteLine(res_str + "\n");
            }
        }
        
        /// <summary>
        /// Функция Аккермана
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        static int Akkerman(int n, int m)
        {
            int result;

            if (n == 0) result = m + 1;
            else if (m == 0) result = Akkerman(n - 1, 1);
            else result = Akkerman(n - 1, Akkerman(n, m - 1));

            return result;
        }

        /// <summary>
        /// Запрашивает 2 значения и выводит результат функции Аккермана
        /// </summary>
        static void AkkFunction()
        {
            int n, m;
            Console.WriteLine("\n//////// ФУНКЦИЯ АККЕРМАНА ////////\n");
            Console.Write("Введите целое неотрицательное число N: ");
            while(!Int32.TryParse(Console.ReadLine(), out n) || n < 0)
            {
                if (n < 0)
                    Console.WriteLine("Число должно быть неотрицательным\n");
                else
                    Console.WriteLine("Ошибка\n");

                Console.Write("Введите целое неотрицательное число N: ");
            }
            Console.Write("Введите целое неотрицательное число M: ");
            while (!Int32.TryParse(Console.ReadLine(), out m) || m < 0)
            {
                if (m < 0)
                    Console.WriteLine("Число должно быть неотрицательным\n");
                else
                    Console.WriteLine("Ошибка\n");

                Console.Write("Введите целое неотрицательное число M: ");
            }
            Console.WriteLine($"\nРезультат: {Akkerman(n, m)}");
        }

        /// <summary>
        /// Вывод меню с выбором операций
        /// </summary>
        static void Menu()
        {
            bool check, exit = false;
            while (!exit)
            {
                check = false;
                Console.WriteLine("\n\n//////////// МЕНЮ ////////////\n");
                Console.WriteLine("1. Операции с матрицами\n2. Операции со строкой\n3. Арифметическая и геометрическая прогрессии\n4. Функция Аккермана\n\n0. Выход");
                while (!check)
                {
                    Console.Write("\nВыберите операцию: ");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            new Matrix();
                            check = true;
                            break;
                        case "2":
                            new StrOpr();
                            check = true;
                            break;
                        case "3":
                            ArithGeo();
                            check = true;
                            break;
                        case "4":
                            AkkFunction();
                            check = true;
                            break;
                        case "0":
                            check = true;
                            exit = true;
                            break;
                        default:
                            Console.WriteLine("Некорректный выбор. Повторите ввод.");
                            break;
                    }
                }
            }
        }

        static void Main(string[] args) { Menu(); }
    }
}
