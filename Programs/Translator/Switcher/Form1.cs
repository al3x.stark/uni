﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Switcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.ForeColor = Color.Red;
            label2.ForeColor = Color.Red;
            label3.ForeColor = Color.Red;
        }



        int firstI;
        int secondI;
        int count;
        int index;

        bool error;
        bool record;
        bool done;

        string str_begin;
        string str_end;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            label1.Text = "";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            label2.Text = "";
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            label3.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            error = false;
            record = false;
            done = false;

            str_begin = "";
            str_end = "";

            label2.Text = "";
            label3.Text = "";

            if (textBox1.Text.Length == 0)
            {
                label1.Text = "Empty";
                error = true;
            }

            if (textBox3.Text.Length == 0)
            {
                label2.Text = "Empty";
                error = true;
            }
            else if (!Int32.TryParse(textBox3.Text, out firstI) || firstI < 1)
            {
                label2.Text = "Error";
                error = true;
            }

            if (textBox4.Text.Length == 0)
            {
                label3.Text = "Empty";
                error = true;
            }
            else if (!Int32.TryParse(textBox4.Text, out secondI) || secondI < 1)
            {
                label3.Text = "Error";
                error = true;
            }

            if (firstI > secondI)
            {
                label2.Text = "                 Logical error";
                error = true;
            }

            if (!error)
            {
                count = 0;
                index = 0;
                while (count != textBox1.Text.Length)
                {
                    if (textBox1.Text[count] == ' ')
                        while (count < textBox1.Text.Length && textBox1.Text[count] == ' ') count++;
                    else
                    {
                        index++;
                        if (record)
                        {
                            while (count < textBox1.Text.Length && textBox1.Text[count] != ' ')
                            { str_end += textBox1.Text[count]; count++; }
                            str_end += ' ';

                            if (index == secondI)
                            {
                                record = false;
                                done = true;
                            }
                        }
                        else if (done)
                        {
                            while (count < textBox1.Text.Length && textBox1.Text[count] != ' ')
                            { str_begin += textBox1.Text[count]; count++; }
                            str_begin += ' ';
                        }
                        else
                        {
                            if (index == firstI)
                            {
                                record = true;
                                while (count < textBox1.Text.Length && textBox1.Text[count] != ' ')
                                { str_end += textBox1.Text[count]; count++; }
                                str_end += ' ';

                                if (index == secondI)
                                {
                                    record = false;
                                    done = true;
                                }
                            }
                            else
                            {
                                while (count < textBox1.Text.Length && textBox1.Text[count] != ' ')
                                { str_begin += textBox1.Text[count]; count++; }
                                str_begin += ' ';
                            }
                        }
                    }
                }

                if (done)
                    textBox2.Text = str_begin + str_end;
                else
                {
                    if (record) label3.Text = "Not found";
                    else { label2.Text = "Not found"; label3.Text = "Not found"; }
                }
            }
            else textBox2.Text = "";

        }
    }
}
