﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Number_Reader
{
    public partial class NameReader : Form
    {
        public NameReader()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        string str_number;         // Предположительный результат ввода.
        string word;               // Слово, проверяемое в данный момент.
        string fullStr_number;     // Хранит строку-подсказку.
        string fullNumber;         // Хранит строку вывода (римский).
        int fullANumber;           // Хранит строку вывода (арабский).

        int i_number;              // Индекс предполагаемого числа.
        int s_index;               // Индекс предыдущего числа.
        int letter_count;          // Счетчик букв во время проверки слова.
        int words;                 // Счетчик слов.
        int count;                 // Позиция в строке ввода, где заканчивается слово.
        int position;              // Позиция в строке ввода, где начинается слово.

        bool error;                // True, когда выявлена ошибка. 
        bool notFinished;          // Проверяет, дописано ли слово до конца. 

        char firstLetter;          // Первая буква проверяемого слова

        string[] numbers = new string[]                                         //
                {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",   //
                    "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII",   //  Массив числовых значений для вывода.
                    "XIX", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",   //
                    "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};    //

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            error = false;                      //
            notFinished = false;                //
            s_index = 0;                        //
            words = 0;                          //
            count = 0;                          //
            position = 0;                       //
            fullANumber = 0;                    //
                                                //    Установка
            fullStr_number = "";                //
            fullNumber = "";                    //     значений
            word = "";                          //
            str_number = "";                    //   по умолчанию.
                                                //
            textBox2.Clear();                   //
            textBox3.Clear();                   //
                                                //
            label1.Text = "";                   //
            label1.ForeColor = Color.Black;     //
                                                //
            button1.Visible = false;            //
            button1.Text = "";                  //

            letter_count = 1;                   // Первая буква проверяется всегда, поэтому значение по умолчанию = 1.

            if (textBox1.Text.Length != 0)      // Проверяет, пуста ли строка.
            {
                while (count < textBox1.Text.Length)
                {
                    while (count != textBox1.Text.Length && textBox1.Text[count] == ' ') { position++; count++; }   //  Пропускает пробелы.
                    if (count == textBox1.Text.Length) break;                                                       // Если строка заканчивается пробелом - завершает цикл.

                    if (notFinished)                                    //
                    {                                                   //
                        label1.ForeColor = Color.Red;                   //      Профилактика
                        label1.Text = $"Ошибка в слове \"{word}\".";    //   незавершенных слов.
                        button1.Visible = false;                        //
                        break;                                          //
                    }                                                   //

                    word = "";                          //
                    str_number = "";                    //  Установка значений по умолчанию
                    letter_count = 1;                   //

                    if (words == 3)                                                             //
                    {                                                                           //
                        label1.ForeColor = Color.Red;                                           //
                        label1.Text = $"Значение \"{fullStr_number}\" - законченое.";           //
                        textBox2.Clear();                                                       //  Профилактика
                        textBox3.Clear();                                                       //   логических
                        break;                                                                  //    ошибок.
                    }


                        while (count != textBox1.Text.Length && textBox1.Text[count] != ' ') count++;   //
                    word = textBox1.Text.Substring(position, count - position);                     //  Запись слова.

                    firstLetter = word[0];      //  Чтение первой буквы.


                    ////   Определение потенциального числа   ////
                    //// опереаясь на первую букву и другие параметры.  ////

                    #region * Буквы *

                    switch (word[0])
                    {
                        #region " О "
                        case 'о':
                            if (word.Length < 5) { str_number = "один"; i_number = 0; }
                            else { str_number = "одиннадцать"; i_number = 10; }
                            break;
                        #endregion

                        #region " Д "
                        case 'д':
                            if (word.Length == 1) { str_number = "два"; i_number = 1; }
                            else
                            {
                                if (word[1] == 'е')
                                {
                                    if (word.Length == 2) { str_number = "девять"; i_number = 8; }
                                    else if (word[2] == 'с') { str_number = "десять"; i_number = 9; }
                                    else if (word[2] == 'в')
                                    {
                                        if (word.Length < 5) { str_number = "девять"; i_number = 8; }
                                        else if (word[4] == 'н') { str_number = "девяносто"; i_number = 26; }
                                        else if (word[4] == 'т')
                                        {
                                            if (word.Length > 5 && word[5] == 'н') { str_number = "девятнадцать"; i_number = 18; }
                                            else if (word.Length > 6) { str_number = "девятьсот"; i_number = 35; }
                                            else { str_number = "девять"; i_number = 8; }
                                        }
                                        else error = true;
                                    }
                                }
                                else if (word[1] == 'в')
                                {
                                    if (word.Length == 2) { str_number = "два"; i_number = 1; }
                                    else if (word[2] == 'а')
                                    {
                                        if (word.Length > 3) { str_number = "двадцать"; i_number = 19; }
                                        else { str_number = "два"; i_number = 1; }
                                    }
                                    else if (word[2] == 'е')
                                    {
                                        if (word.Length == 3 || word[3] == 'н') { str_number = "двенадцать"; i_number = 11; }
                                        else if (word[3] == 'с') { str_number = "двести"; i_number = 28; }
                                        else error = true;
                                    }
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " С "
                        case 'с':
                            if (word.Length == 1) { str_number = "семь"; i_number = 6; }
                            else
                            {
                                if (word[1] == 'т') { str_number = "сто"; i_number = 27; }
                                else if (word[1] == 'о') { str_number = "сорок"; i_number = 21; }
                                else if (word[1] == 'е')
                                {
                                    if (word.Length < 4) { str_number = "семь"; i_number = 6; }
                                    else if (word[3] == 'н') { str_number = "семнадцать"; i_number = 16; }
                                    else if (word[3] == 'ь')
                                    {
                                        if (word.Length == 4) { str_number = "семь"; i_number = 6; }
                                        else if (word[4] == 'с') { str_number = "семьсот"; i_number = 33; }
                                        else if (word[4] == 'д') { str_number = "семьдесят"; i_number = 24; }
                                        else error = true;
                                    }
                                    else error = true;
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " Т "
                        case 'т':
                            if (word.Length < 4) { str_number = "три"; i_number = 2; }
                            else
                            {
                                if (word[3] == 'н') { str_number = "тринадцать"; i_number = 12; }
                                else if (word[3] == 'д') { str_number = "тридцать"; i_number = 20; }
                                else if (word[3] == 'с') { str_number = "триста"; i_number = 29; }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " Ч "
                        case 'ч':
                            if (word.Length < 6) { str_number = "четыре"; i_number = 3; }
                            else
                            {
                                if (word[5] == 'н') { str_number = "четырнадцать"; i_number = 13; }
                                else if (word[5] == 'е')
                                {
                                    if (word.Length == 6) { str_number = "четыре"; i_number = 3; }
                                    else { str_number = "четыреста"; i_number = 30; }
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " П "
                        case 'п':
                            if (word.Length < 4) { str_number = "пять"; i_number = 4; }
                            else
                            {
                                if (word[3] == 'н') { str_number = "пятнадцать"; i_number = 14; }
                                else if (word[3] == 'ь')
                                {
                                    if (word.Length == 4) { str_number = "пять"; i_number = 4; }
                                    else
                                    {
                                        if (word[4] == 'д') { str_number = "пятьдесят"; i_number = 22; }
                                        else if (word[4] == 'с') { str_number = "пятьсот"; i_number = 31; }
                                        else error = true;
                                    }
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " Ш "
                        case 'ш':
                            if (word.Length < 5) { str_number = "шесть"; i_number = 5; }
                            else
                            {
                                if (word[4] == 'н') { str_number = "шестнадцать"; i_number = 15; }
                                else if (word[4] == 'ь')
                                {
                                    if (word.Length == 5) { str_number = "шесть"; i_number = 5; }
                                    else
                                    {
                                        if (word[5] == 'д') { str_number = "шестьдесят"; i_number = 23; }
                                        else if (word[5] == 'с') { str_number = "шестьсот"; i_number = 32; }
                                        else error = true;
                                    }
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        #region " В "
                        case 'в':
                            if (word.Length < 6) { str_number = "восемь"; i_number = 7; }
                            else
                            {
                                if (word[5] == 'н') { str_number = "восемнадцать"; i_number = 17; }
                                else if (word[5] == 'ь')
                                {
                                    if (word.Length == 6) { str_number = "восемь"; i_number = 7; }
                                    else
                                    {
                                        if (word[6] == 'д') { str_number = "восемьдесят"; i_number = 25; }
                                        else if (word[6] == 'с') { str_number = "восемьсот"; i_number = 34; }
                                        else error = true;
                                    }
                                }
                                else error = true;
                            }
                            break;
                        #endregion

                        default:
                            error = true;
                            break;
                    }
                    #endregion


                    if (!error && word.Length > str_number.Length)      // 
                        error = true;                                   //
                                                                        //
                    if (!error)                                         //
                        for (int i = 1; i < word.Length; i++)           //
                        {                                               //
                            if (word[i] == str_number[i])               //    Проверка
                                letter_count++;                         //
                            else                                        //   грамотности.
                            {                                           //
                                letter_count = 0;                       //
                                error = true;                           //
                                break;                                  //
                            }                                           //
                        }                                               //

                    #region * Проверка грамотности *
                    if (words > 0)                                                              
                    {                                                                           
                        if (str_number.Length != letter_count)                                  
                            notFinished = true;                                                 
                        if (s_index < 9)                                                        
                        {                                                                       
                            label1.ForeColor = Color.Red;                                       
                            if (notFinished)                                                    
                                label1.Text = $"Ошибка в слове \"{word}\".";                    
                            else                                                                
                            {                                                                   
                                if (i_number < 9)                                               
                                    label1.Text = "Число единичного формата " +                 
                                    "перед числом единичного формата.";  
                                else if (i_number > 9 && i_number < 19)
                                    label1.Text = "Число единичного формата " +
                                "перед числом формата \"11-19\".";
                                else if (i_number > 18 && i_number < 27 || i_number == 9)                         
                                    label1.Text = "Число единичного формата " +                 
                                "перед числом десятичного формата.";                            
                                else
                                    label1.Text = "Число единичного формата " +
                                             "перед сотнями.";
                            }
                            textBox2.Clear();                                                  
                            textBox3.Clear();                                                   
                            break;                                                              
                        }
                        else if (s_index < 27 && s_index > 18 && i_number > 8 || s_index == 9)
                        {
                            label1.ForeColor = Color.Red;
                            if (notFinished)
                                label1.Text = $"Ошибка в слове \"{word}\".";
                            else
                            {
                                if (s_index == 9 && i_number < 9)
                                    label1.Text = "Число \"десять\" " +
                                "перед числом единичного формата.";
                                else if (i_number > 9 && i_number < 19)
                                    label1.Text = "Число десятичного формата " +
                                "перед числом формата \"11-19\".";
                                else if (i_number > 18 && i_number < 27 || i_number == 9)
                                    label1.Text = "Число десятичного формата " +
                                "перед числом десятичного формата.";
                                else
                                    label1.Text = "Число десятичного формата " +
                                             "перед сотнями.";
                            }
                            textBox2.Clear();
                            textBox3.Clear();
                            break;
                        }
                        else if (s_index < 19 )
                        {
                            label1.ForeColor = Color.Red;
                            if (notFinished)
                                label1.Text = $"Ошибка в слове \"{word}\".";
                            else
                            {
                                if (i_number < 9)
                                    label1.Text = "Число формата \"11-19\" " +
                                "перед числом единичного формата.";
                                else if (i_number > 9 && i_number < 19)
                                    label1.Text = "Число формата \"11-19\" " +
                                "перед числом формата \"11-19\".";
                                else if (i_number > 18 && i_number < 27 || i_number == 9)
                                    label1.Text = "Число формата \"11-19\" " +
                                "перед числом десятичного формата.";
                                else
                                    label1.Text = "Число формата \"11-19\" " +
                                             "перед сотнями.";
                            }
                            textBox2.Clear();
                            textBox3.Clear();
                            break;
                        }                                                   
                        else if (i_number > 26)                    
                        {                                                    
                            label1.ForeColor = Color.Red;                    
                            if (notFinished)
                                label1.Text = $"Ошибка в слове \"{word}\".";
                            else
                                label1.Text = "Сотни перед сонями.";            
                            textBox2.Clear();                                 
                            textBox3.Clear();                                 
                            break;                                             
                        }                                                     
                    }
                    #endregion

                    ////  Ошибка  ////
                    if (error)
                    {
                        textBox2.Clear();
                        textBox3.Clear();
                        label1.ForeColor = Color.Red;
                        label1.Text = $"Ошибка в слове \"{word}\".";
                        break;
                    }

                    ////  Числовое значение подтверждено  ////
                    else if (str_number.Length == letter_count)
                    {
                        fullNumber += numbers[i_number];                //  Добавление числа в пул.
                        if (i_number < 19)                              //
                            fullANumber += i_number + 1;                //
                        else if (i_number < 27)                         //  Формирование арабского числа.
                            fullANumber += (i_number - 17)*10;          //
                        else fullANumber += (i_number - 26)*100;        //
                        textBox2.Text = fullNumber;                     //  Вывод числа (римский).
                        textBox3.Text = Convert.ToString(fullANumber);  //  Вывод числа (арабский).
                        s_index = i_number;                             //  Сохранение индекса числа ддля дальнейшей логической проверки.
                        words++;                                        //  Увеличение счетчика подтвержденных чисел.
                        position = count;                               //  Смещение позиции начала слова для корректного считывания следующего слова.
                        fullStr_number += str_number + " ";             //  Добавление слова к подсказке.
                    }

                    ////  Подсказка  ////
                    else
                    {
                        textBox2.Clear();
                        textBox3.Clear();
                        button1.Visible = true;                         //
                        button1.Text = fullStr_number + str_number;     //  Выводит подсказку.
                        label1.Text = "Возможно вы подразумеваете ";    //
                        notFinished = true;     //  Слово не закончено.
                    }
                }
            }
            else label1.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fullStr_number != "")                               //
                textBox1.Text = fullStr_number + str_number;        //  Выводит подсказку.
            else textBox1.Text = str_number;                        //
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
