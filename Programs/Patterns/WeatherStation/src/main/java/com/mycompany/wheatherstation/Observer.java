package com.mycompany.wheatherstation;

public interface Observer {
 public void update();
}
