﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestQuipu
{
    class Page
    {
        public bool Trash { get; private set; } = false;
        public string Path { get; }
        public int TegsCount { get; private set; } = 0;
        public Page(string path)
        {
            Path = path;
        }
        public int CountTegs(WebClient webClient)
        {
            if (Path != null)
            {
                try
                {
                    string path = webClient.DownloadString(Path);
                    TegsCount = new Regex("</a>").Matches(path).Count;
                }
                catch 
                {
                    Trash = true;
                }                
            }
            return TegsCount;
        }
    }
}
