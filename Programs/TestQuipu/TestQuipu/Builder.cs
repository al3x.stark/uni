﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TestQuipu
{
    class Builder
    {        
        public static List<Page> GetPages(string input)
        {
            List<Page> pages = new List<Page>();
            foreach (string url in input.Split(new char[] { ' ', '\n', '\t', '\r' }))
            {
                if (url.Length > 5 && url.StartsWith("http"))
                    pages.Add(new Page(url));
            }
            return pages;
        }
        public static string GetFile()
        {
            string input;
            try
            {
                string path = FileIOService.GetFilePath();
                if (path == string.Empty)
                    return null;
                input = FileIOService.LoadData(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка");
                return null;
            }
            if (input == string.Empty)
            {
                MessageBox.Show("В выбранном файле отсутствуют данные.", "Ошибка");
                return null;
            }
            return input;
        }
    }
}
