﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestQuipu
{
    class Worker
    {
        public static string FindTegsInHTMLList(List<Page> pages)
        {
            return FindTegsInHTMLList(pages, null);
        }
        public static string FindTegsInHTMLList(List<Page> pages, System.ComponentModel.BackgroundWorker backgroundWorker)
        {
            string output = string.Empty;
            int tegs;
            WebClient webClient = new WebClient();
            double iteration = (double)100 / pages.Count;
            for (int i = 0; i < pages.Count; i++)
            {
                Page page = pages[i];
                output += $"{page.Path}:   {page.CountTegs(webClient)}\n";

                if (backgroundWorker != null)
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        return null;
                    }

                    if (backgroundWorker.WorkerReportsProgress)
                    {
                        backgroundWorker.ReportProgress((int)((i + 1) * iteration));
                    }
                }
            }
            if (backgroundWorker != null && backgroundWorker.WorkerReportsProgress)
            {
                backgroundWorker.ReportProgress(100);
            }
            return output;
        }
        public static string FindMaxTegsPage(List<Page> pages)
        {
            string maxTegsString = string.Empty;
            int maxTegs = 0;
            foreach (Page page in pages)
            {
                if (page.TegsCount > maxTegs)
                {
                    maxTegs = page.TegsCount;
                    maxTegsString = $"{page.Path}:   {maxTegs}\n";
                }
                else if (page.TegsCount == maxTegs)
                    maxTegsString += $"{page.Path}:   {maxTegs}\n";
            }
            return maxTegsString;
        }
    }
}
