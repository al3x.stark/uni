﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestQuipu
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string output, maxTegs;
        List<Page> pages;
        BackgroundWorker backgroundWorker;
        public MainWindow()
        {
            InitializeComponent();
            backgroundWorker = ((BackgroundWorker)this.FindResource("backgroundWorker"));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cancelBtn.IsEnabled = false;
            startBtn.IsEnabled = false;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            output = string.Empty;
            List<Page> paths = (List<Page>)e.Argument;
            output = Worker.FindTegsInHTMLList(paths, backgroundWorker);
            if (output != string.Empty && output != null )
                maxTegs = Worker.FindMaxTegsPage(paths);                
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prgrsBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Произошла ошибка");
            }
            else if (!e.Cancelled)
            {
                if (output == string.Empty)
                    MessageBox.Show("Url не были найдены в представленном файле.", "Ошибка");
                else
                {
                    maxTegsBlock.Text = maxTegs + "\n";
                    textBlock.Text = output;
                }
            }
            cancelBtn.IsEnabled = false;
            fileBtn.IsEnabled = true;
            prgrsBar.Value = 0;
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            cancelBtn.IsEnabled = true;
            fileBtn.IsEnabled = false;
            startBtn.IsEnabled = false;

            backgroundWorker.RunWorkerAsync(pages);
        }

        private void fileBtn_Click(object sender, RoutedEventArgs e)
        {
            string file = Builder.GetFile();
            if (file is null) return;

            pages = Builder.GetPages(file);
            maxTegsBlock.Text = string.Empty;
            maxTegs = string.Empty;
            if (pages.Count > 0)
            {
                textBlock.Text = file;
                startBtn.IsEnabled = true;
            }
            else startBtn.IsEnabled = false;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            backgroundWorker.CancelAsync();
        }
    }
}
