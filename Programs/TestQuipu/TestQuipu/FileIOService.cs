﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQuipu
{
    class FileIOService
    {
        public static string GetFilePath()
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "TXT Files (*.txt)| *.txt"
            };
            openFileDialog.ShowDialog();
            return openFileDialog.FileName;
        }
        public static string LoadData(string path)
        {
            using (var reader = File.OpenText(path))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
