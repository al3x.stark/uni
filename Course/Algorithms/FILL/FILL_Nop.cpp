﻿#include <iostream>
#include <ctime>
#include <chrono>
using namespace std;

class List
{
public:

	List();

	void Push(int data);
	int Pop();
	int GetSize();
	void Clear();
	void Build(int number);
	void Show();

protected:

	class Node
	{
	public:

		Node* next;
		int data;

		Node(int data = 0, Node* next = nullptr)
		{
			this->data = data;
			this->next = next;
		}

	};

	int size;
	bool build_flag = false;
	bool sort_flag = false;

public:
	Node* head;

};

class Turn : List
{
public:
	Turn();
	int GetData(int index);
	void SetData(int index, int new_data);
	void Menu();
	void Sort();
};

List::List()
{
	size = 0;
	head = nullptr;
}

void List::Push(int data)
{
	if (head == nullptr)
	{
		head = new Node(data);
	}
	else
	{
		Node* count = this->head;
		while (count->next != nullptr)
		{
			count = count->next;
		}
		count->next = new Node(data);
	}
	size++;
}

int List::Pop()
{
	int t_data;
	Node* trash = head;
	head = head->next;
	t_data = trash->data;
	delete trash;
	size--;
	if (size == 0)
	{
		build_flag = false;
		sort_flag = false;
	}

	return t_data;
}

int List::GetSize()
{
	return size;
}

void List::Clear()
{
	while (size != 0)
		Pop();
	build_flag = false;
	sort_flag = false;
}

void List::Build(int number)
{
	srand(time(NULL));
	for (int i = 0; i < number; i++)
		Push(rand() % 10000);
	build_flag = true;
}

void List::Show()
{
	int i = 0;
	Node* count = head;
	if (count != nullptr)
		while (count != nullptr)
		{
			i++;
			cout << i << ". " << count->data << endl;
			count = count->next;
		}
	else
		cout << "Очередь пуста." << endl;
	cout << endl;
}

Turn::Turn()
{
	Menu();
}

int Turn::GetData(int index)
{
	int count = size - index - 1;
	int new_data, true_data;
	while (index != 0)
	{
		new_data = Pop();
		Push(new_data);
		index--;
	}
	true_data = Pop();
	Push(true_data);
	while (count != 0)
	{
		new_data = Pop();
		Push(new_data);
		count--;
	}

	return true_data;
}

void Turn::SetData(int index, int true_data)
{
	int count = size - index - 1;
	int new_data;
	while (index != 0)
	{
		new_data = Pop();
		Push(new_data);
		index--;
	}
	new_data = Pop();
	Push(true_data);
	while (count != 0)
	{
		new_data = Pop();
		Push(new_data);
		count--;
	}
}

void Turn::Menu()
{
	while (true)
	{
		int number = 0;
		bool flag = true;
		int index, n_nodes, new_data, t;
		clock_t time = 0;

		cout << "МЕНЮ" << endl << endl;
		if (!build_flag)
			cout << "1. Построить очередь." << endl;
		else
			cout << "1. Перестроить очередь." << endl;
		cout << "2. Показать очередь." << endl;
		cout << "3. Cортировать очередь" << endl;
		cout << "4. Показать длину очереди." << endl;
		cout << "5. Push()." << endl;
		cout << "6. Pop()." << endl;
		cout << "7. GetData()." << endl;
		cout << "8. SetData()." << endl;
		cout << "9. Очистить очередь." << endl << endl;
		cout << "Номер операции: ";

		cin >> number;
		cout << endl;

		switch (number)
		{
		case 1:
			n_nodes = 0;
			while (n_nodes < 1)
			{
				cout << "Введите количество элементов: ";
				cin >> n_nodes;
				cout << endl;
				if (n_nodes > 0 && !build_flag)
					Build(n_nodes);
				else if (build_flag && n_nodes > 0)
				{
					Clear();
					Build(n_nodes);
				}
				else
					cout << "Введено некорректное число." << endl;
			}
			Show();
			break;
		case 2:
			Show();
			break;
		case 3:
			if (build_flag)
			{
				if (size != 1 && !sort_flag)
				{
					//time = clock();
					//std::chrono::time_point<std::chrono::system_clock> start, end;
					//start = std::chrono::system_clock::now();
					Sort();
					//end = std::chrono::system_clock::now();
					//t = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
					//time = clock() - time;
				}
				Show();
				//cout << "time = " << (float)time << " ticks (" << t << "s)" << endl << endl;
			}
			else
				cout << "Очередь пуста." << endl << endl;
			break;
		case 4:
			cout << "Длина очереди = " << GetSize() << endl << endl;
			break;
		case 5:
			cout << "Введите значение элемента: ";
			cin >> new_data;
			cout << endl;
			Push(new_data);
			build_flag = true;
			cout << "Элемент добавлен." << endl << endl;
			Show();
			break;
		case 6:
			if (build_flag)
			{
				new_data = Pop();
				cout << "Элемент со значением " << new_data << " удален." << endl << endl;
				Show();
			}
			else
				cout << "Очередь пуста." << endl << endl;
			break;
		case 7:
			if (build_flag)
			{
				Show();
				while (flag)
				{
					cout << "Введите номер элемента: ";
					cin >> index;
					index--;
					cout << endl;
					if (index < size && index > -1)
						flag = false;
					else
						cout << "Введен некорретный номер." << endl;
				}
				flag = true;
				cout << "Значение элемента " << index + 1 << " = " << GetData(index) << endl << endl;
			}
			else
				cout << "Очередь пуста." << endl << endl;
			break;
		case 8:
			if (build_flag)
			{
				Show();
				while (flag)
				{
					cout << "Введите номер элемента: ";
					cin >> index;
					index--;
					cout << endl;
					if (index < size && index > -1)
						flag = false;
					else
						cout << "Введен некорретный номер." << endl;
				}
				flag = true;
				cout << "Введите новое значение элемента: ";
				cin >> new_data;
				SetData(index, new_data);
				cout << "Значение элемента " << index + 1 << " изменено на " << new_data << endl << endl;
			}
			else
				cout << "Очередь пуста." << endl << endl;
			break;
		case 9:
			if (build_flag)
			{
				Clear();
				cout << "Очередь очищена." << endl << endl;
			}
			else
				cout << "Очередь пуста." << endl << endl;
			break;
		default:
			break;
		}
	}
}

void Turn::Sort()
{
	int count_1 = 0, count_2, r_count, data_1, data_2, r_data;
	while (count_1 < size - 2)
	{
		data_1 = GetData(count_1);
		r_count = count_1 + 1;
		r_data = GetData(r_count);
		count_2 = r_count + 1;
		while (count_2 < size)
		{
			data_2 = GetData(count_2);
			if (r_data > data_2)
			{
				r_data = data_2;
				r_count = count_2;
			}
			count_2++;
		}
		if (data_1 > r_data)
		{
			SetData(count_1, r_data);
			SetData(r_count, data_1);
		}
	}
	if (size > 2)
	{
		count_2--;
		if (r_count == count_2)
			r_count--;
	}
	else
	{
		count_2 = 1;
		r_count = 0;
	}
	r_data = GetData(r_count);
	data_2 = GetData(count_2);
	if (r_data > data_2)
	{
		SetData(count_2, r_data);
		SetData(r_count, data_2);
	}
	sort_flag = true;
}

int main()
{

	setlocale(LC_ALL, "ru");

	Turn big_one;


	return 0;

}
