﻿#include <iostream>
#include <ctime>
using namespace std;

class List
{
public:

	List();

	void Push(int data);
	int Pop();
	int GetSize();
	void Clear();
	void Build(int number);
	void Show();
	unsigned long long int Nop;

protected:

	class Node
	{
	public:

		Node* next;
		int data;

		Node(int data = 0, Node* next = nullptr)
		{
			this->data = data;
			this->next = next;
		}

	};

	int size;
	bool build_flag = false;
	bool sort_flag = false;

public:
	Node* head;
};

class Turn : List
{
public:
	Turn();
	int GetData(int index);
	void SetData(int index, int new_data);
	void Menu();
	void Sort();
};

List::List()
{
	size = 0;
	head = nullptr;
	Nop = 0;
}

void List::Push(int data)
{
	Nop++;
	if (head == nullptr)
	{
		head = new Node(data);  Nop += 7;
	}
	else
	{
		Node* count = this->head;  Nop += 7;
		Nop++;
		while (count->next != nullptr)
		{
			Nop++;
			count = count->next;  Nop++;
		}  Nop++;
		count->next = new Node(data);  Nop += 7;
	}  Nop++;
	size++;  Nop += 2;
}

int List::Pop()
{
	int t_data;  Nop++;
	Node* trash = head;  Nop += 7;
	head = head->next;  Nop++;
	t_data = trash->data;  Nop++;
	delete trash;  Nop++;
	size--;  Nop += 2;

	Nop++;  
	return t_data;
}

int List::GetSize()
{
	return size;
}

void List::Clear()
{
	while (size != 0)
		Pop();
}

void List::Build(int number)
{
	srand(time(NULL));
	for (int i = 0; i < number; i++)
		Push(rand() % 10000);
}

void List::Show()
{
	int i = 0;
	Node* count = head;
	while (count != nullptr)
	{
		i++;
		cout << i << ". " << count->data << endl;
		count = count->next;
	}
	cout << endl;
}

Turn::Turn()
{
	Menu();
}

int Turn::GetData(int index)
{
	Nop++;
	int count = size - index - 1;  Nop += 4;
	int new_data, true_data;  Nop += 2;
	Nop++;
	while (index != 0)
	{
		Nop++;
		new_data = Pop();  Nop++;
		Push(new_data);
		index--;  Nop += 2;
	}  Nop++;
	true_data = Pop();  Nop++;
	Push(true_data);
	Nop++;
	while (count != 0)
	{
		Nop++;
		new_data = Pop();  Nop++;
		Push(new_data);
		count--;  Nop += 2;
	}  Nop++;

	Nop++;  
	return true_data;
}

void Turn::SetData(int index, int true_data)
{
	Nop += 2;
	int count = size - index - 1;  Nop += 4;
	int new_data;  Nop++;
	Nop++;
	while (index != 0)
	{
		Nop++;
		new_data = Pop();  Nop++;
		Push(new_data);
		index--;  Nop += 2;
	}  Nop++;
	new_data = Pop();  Nop++;
	Push(true_data);
	Nop++;
	while (count != 0)
	{
		Nop++;
		new_data = Pop();  Nop++;
		Push(new_data);
		count--;  Nop += 2;
	}  Nop++;
}

void Turn::Menu()
{
	while (true)
	{
		int number = 0;
		bool flag = true, op_flag = true;
		int index, n_nodes, new_data;
		//clock_t time;

		cout << "МЕНЮ" << endl << endl;
		if (!build_flag)
			cout << "1. Построить очередь." << endl;
		else
			cout << "1. Перестроить очередь." << endl;
		cout << "2. Показать очередь." << endl;
		cout << "3. Cортировать очередь" << endl;
		cout << "4. Показать длину очереди." << endl;
		cout << "5. Push()." << endl;
		cout << "6. Pop()." << endl;
		cout << "7. GetData()." << endl;
		cout << "8. SetData()." << endl;
		cout << "9. Очистить очередь." << endl << endl;
		while (op_flag)
		{
			cout << "Номер операции: ";

			cin >> number;
			cout << endl;

			switch (number)
			{
			case 1:
				n_nodes = 0;
				while (n_nodes < 1)
				{
					cout << "Введите количество элементов: ";
					cin >> n_nodes;
					cout << endl;
					if (n_nodes > 0 && !build_flag)
					{
						Build(n_nodes);
						build_flag = true;
					}
					else if (build_flag && n_nodes > 0)
					{
						Clear();
						Build(n_nodes);
						sort_flag = false;
						build_flag = true;
					}
					else
						cout << "Введено некорректное число." << endl;
				}
				Show();
				op_flag = false;
				break;
			case 2:
				if (size != 0)
					Show();
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			case 3:
				if (build_flag)
				{
					if (size != 1 && !sort_flag)
					{
						Nop = 0;
						//time = clock();
						Sort();
						//time = clock() - time;
						sort_flag = true;
					}
					Show();
					cout << "Nop = " << Nop << endl << "n = " << size << endl << endl;
					//cout << "time = " << (float)time << " ticks (" << (float)time/CLOCKS_PER_SEC << "s)" << endl << "n = " << size << endl << endl;
				}
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			case 4:
				cout << "Длина очереди = " << GetSize() << endl << endl;
				op_flag = false;
				break;
			case 5:
				cout << "Введите значение элемента: ";
				cin >> new_data;
				cout << endl;
				Push(new_data);
				cout << "Элемент добавлен." << endl << endl;
				build_flag = true;
				sort_flag = false;
				Show();
				op_flag = false;
				break;
			case 6:
				if (build_flag)
				{
					new_data = Pop();
					cout << "Элемент со значением " << new_data << " удален." << endl << endl;
					if (size == 0)
					{
						build_flag = false;
						sort_flag = false;
					}
					Show();
				}
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			case 7:
				if (build_flag)
				{
					Show();
					while (flag)
					{
						cout << "Введите номер элемента: ";
						cin >> index;
						index--;
						cout << endl;
						if (index < size && index > -1)
							flag = false;
						else
							cout << "Введен некорретный номер." << endl;
					}
					cout << "Значение элемента " << index + 1 << " = " << GetData(index) << endl << endl;
				}
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			case 8:
				if (build_flag)
				{
					Show();
					while (flag)
					{
						cout << "Введите номер элемента: ";
						cin >> index;
						index--;
						cout << endl;
						if (index < size && index > -1)
							flag = false;
						else
							cout << "Введен некорретный номер." << endl;
					}
					cout << "Введите новое значение элемента: ";
					cin >> new_data;
					SetData(index, new_data);
					sort_flag = false;
					cout << "Значение элемента " << index + 1 << " изменено на " << new_data << endl << endl;
				}
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			case 9:
				if (build_flag)
				{
					Clear();
					build_flag = false;
					sort_flag = false;
					cout << "Очередь очищена." << endl << endl;
				}
				else
					cout << "Очередь пуста." << endl << endl;
				op_flag = false;
				break;
			default:
				cout << "Введен некорректный номер. Повторите ввод." << endl;
				break;
			}
		}
	}
}

void Turn::Sort()
{
	int count_1 = 0, count_2 = 2, r_count = 1, data_1, data_2, r_data;  Nop += 9;
	Nop += 2;
	while (count_1 < size - 2)
	{
		Nop += 2;
		data_1 = GetData(count_1);  Nop++;
		r_data = GetData(r_count);  Nop++;
		Nop++;
		while (count_2 < size)
		{
			Nop++;
			data_2 = GetData(count_2);  Nop++;
			if (r_data > data_2)
			{
				r_data = data_2; Nop++;
				r_count = count_2; Nop++;
			}  Nop++;
			count_2++;  Nop += 2;
		}  Nop++;
		if (data_1 > r_data)
		{
			SetData(count_1, r_data);
			SetData(r_count, data_1);
		}  Nop++;
		count_1++;  Nop += 2;
		r_count = count_1 + 1;  Nop += 2;
		count_2 = r_count + 1;  Nop += 2;
	}  Nop += 2;
	count_2--;  Nop += 2;
	if (r_count == count_2)
	{
		r_count--;  Nop += 2;
	}  Nop++;
	r_data = GetData(r_count);  Nop++;
	data_2 = GetData(count_2);  Nop++;
	if (r_data > data_2)
	{
		SetData(count_2, r_data);
		SetData(r_count, data_2);
	}  Nop++;
}

int main()
{

	setlocale(LC_ALL, "ru");

	Turn big_one;


	return 0;

}