﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Translator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.CodeBox.GotFocus += OnFocus;
        }
        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            CodeBox.BackColor = Color.White;
            ResultBox.Text = Verification.Output(CodeBox.Text);
            ErrorBox.Text = Verification.MarkColumn();
            ErrorBox2.Text = Verification.MarkStr();
            CodeBox.Text = Verification.GetInput();
            MarkError();
        }

        private void ErrorBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void MarkError()
        {
            if (Verification.getErr())
            {
                int start = 0;
                int i = 0;
                while (i < Verification.getStr_num())
                {
                    while (CodeBox.Text[start] != '\n' && start != CodeBox.Text.Length - 1) start++;
                    if (start == CodeBox.Text.Length - 1) break;
                    else { i++; start++; }
                }
                start += Verification.getCh_num();
                CodeBox.Select(start, Verification.getErr_count());
                CodeBox.SelectionBackColor = Color.Yellow;
            }
        }

        private void CodeBox_TextChanged(object sender, EventArgs e)
        {
            
        }
        private void OnFocus(object sender, EventArgs e)
        {
            CodeBox.SelectAll();
            CodeBox.SelectionBackColor = Color.White;
        }
    }
}
