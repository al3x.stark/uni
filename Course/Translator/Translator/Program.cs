﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Translator
{
    class Variable
    {
        string name;
        double value;

        public string getName() { return this.name; }
        public double getValue() { return this.value; }
        public void setName(string n) { this.name = n; }
        public void setValue(double v) { this.value = v; }
    }
    static class Verification
    {
        static string result;
        static string[] input_str;
        static bool er;
        static double varMain, varLog, varInterm, varResult;
        static int str_num = 0, ch_num = 0, er_count = 0;
        static List<Variable> variables = new List<Variable>();
        static List<string[]> arrays = new List<string[]>();
        static List<string> array = new List<string>();

        public static int getStr_num() { return str_num; }
        public static int getCh_num() { return ch_num; }
        public static bool getErr() { return er; }
        public static int getErr_count() { return er_count; }
        private static string MarkNext(int count = 0, bool rP = false)
        {
            int i;
            string output = String.Empty;
            char[] stop = { '+', '-', '*', '/', '&', '|', '!' };
            bool stp = false;
            if (count != 0) i = count;
            else
            {
                i = ch_num;
                if (rP)
                {
                    while (input_str[str_num][i] != ' ' && i != input_str[str_num].Length - 1)
                    {
                        foreach (char ch in stop)
                            if (input_str[str_num][i] == ch) { stp = true; break; }
                        if (stp) break;
                        i++;
                    }
                    if (!stp)
                        if (input_str[str_num][i] != ' ') i++;
                }
                else
                {
                    while (input_str[str_num][i] != ' ' && i != input_str[str_num].Length - 1) i++;
                    if (input_str[str_num][i] != ' ') i++;
                }                
                i -= ch_num;
            }
            er_count = i;
            output = input_str[str_num].Substring(ch_num, i);
            input_str[str_num] = input_str[str_num].Remove(ch_num, i);
            input_str[str_num] = input_str[str_num].Insert(ch_num, output.ToUpper());

            return output;
        }
        private static string MarkPrevious()
        {
            int i = ch_num;
            int count = 1;
            string output = String.Empty;
            while (input_str[str_num][i] != ' ' && i != 0) { i--; count++; }
            if (input_str[str_num][i] == ' ') { i++; count--; }
            er_count = count;
            ch_num = i;
            output = input_str[str_num].Substring(i, count);
            input_str[str_num] = input_str[str_num].Remove(i, count);
            input_str[str_num] = input_str[str_num].Insert(i, output.ToUpper());

            return output;
        }
        public static string MarkColumn()
        {
            string output = String.Empty;
            if (er)
            {
                for (int i = 0; i < ch_num; i++)
                {
                    if (input_str[str_num][i] == ' ')
                        output += " ";
                    else output += "  ";
                }
                output += "v";
            }
            return output;
        }
        public static string MarkStr()
        {
            string output = String.Empty;
            if (er)
            {
                for (int i = 0; i < str_num; i++)
                    output += "\r\n";
                output += ">";
            }
            return output;
        }
        private static void Error(int errorNumber, int str = -1, int ch = -1, string v = "")
        {
            er = true;
            if (str == -1) str = str_num;
            if (ch == -1) ch = ch_num;
            switch (errorNumber)
            {
                case 0:
                    result = $"Ошибка 0: \"{MarkNext(v.Length)}\" - Имя переменной должно начинаться с буквы. (Строка {str+1})";
                    break;
                case 1:
                    result = $"Ошибка 1: \"{MarkNext(v.Length)}\" - Имя переменной может содержать только буквы и цыфры. (Строка {str+1})";
                    break;
                case 2:
                    result = $"Ошибка 2: После знака \"-\" должна следовать функция, переменная или целое."  +
                        $"\r\nВстречено: \"конец уравнения\"  (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 3:
                    result = $"Ошибка 3: Переменная \"{MarkNext(v.Length)}\" не объявлена. (Строка {str + 1})";
                    break;
                case 4:
                    result = $"Ошибка 4: Ожидается метка." +
                        $"\r\nМетка должна иметь целочисленное значение." +
                        $"\r\nВстречено: \"{MarkNext(v.Length)}\" (Строка {str + 1})";
                    break;
                case 5:
                    result = $"Ошибка 5: После оператора должна следовать функция, переменная или целое число. (Строка {str + 1})" +
                        $"\r\nВстечен конец уравнения. Ожидался операнд.";
                    er_count = 1;
                    break;
                case 6:
                    result = $"Ошибка 6: Нарушен синтаксис правой части" +
                        $"\r\n После элемента \"{v}\" должен быть оператор, разделитель или объявление Множества." +
                        $"\r\nВстречено: \"{MarkNext(0, true)}\" (Строка {str + 1})";
                    break;
                case 8:
                    result = $"Ошибка 8: После метки должен следовать знак \":\"." +
                        $"\r\nВстречено: \"{MarkNext(1)}\" (Строка {str + 1})";
                    break;
                case 9:
                    result = $"Ошибка 9: После переменной должен следовать знак \"=\"." +
                        $"\r\nВстречено: \"{MarkNext(1)}\" (Строка {str + 1})";
                    break;
                case 10:
                    result = $"Ошибка 10: После знака \"=\" должна следовать правая часть." +
                        $"\r\nВстречено: \"{MarkNext()}\"  (Строка {str + 1})";
                    break;
                case 12:
                    result = $"Ошибка 12: Переменная не может иметь имя \"sin\" или \"cos\". (Строка {str + 1})";
                        MarkNext(3);
                    break;
                case 13:
                    result = $"Ошибка 13: Неизвестный элемент \"{MarkNext(v.Length)}\" в правой части. (Строка {str + 1})";
                    break;
                case 14:
                    result = $"Ошибка 14: Функция \"{MarkNext(3)}\" должна принимать параметр." +
                        $"\r\nВ качестве параметра может быть функция, переменная или целое число. (Строка {str + 1})";
                    break;
                case 15:
                    result = $"Ошибка 15: Встречено 2 оператора подряд. Ожидался операнд." +
                        $"\r\nПеред оператором должна быть функция, переменная или целое число. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 16:
                    result = $"Ошибка 16: Перед объявлением множества должно быть уравнение." +
                        $"\r\nПрограмма должна включать хотя бы одно уравнение и множество. (Строка {str + 1})";
                    er_count = 6;
                    break;
                case 17:
                    result = $"Ошибка 17: После разделителя \";\" должно следовать уравнениe." +
                        $"\r\nПеред объявлением Множества разделителя \";\" быть не должно. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 18:
                    result = $"Ошибка 18: Встречено 2 разделителя \";\" подряд. Ожидалась метка." +
                        $"\r\nПеред разделителем \";\" должно быть уравнениe. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 19:
                    result = $"Ошибка 19: Деление на ноль не предусмотрено. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 20:
                    result = $"Ошибка 20: Параметр \"{MarkNext(v.Length)}\" не распознан." +
                        $"\r\nВ качестве параметра функция может принимать функцию, пременную или целое." +
                        $"\r\nОбъявление функции должно иметь вид \"sin Параметр\" или \"cos Параметр\". (Строка {str + 1})";
                    break;
                case 27:
                    result = $"Ошибка 27: После объявления множества должна следовать хотя бы одна переменная. (Строка {str + 1})";
                    er_count = 6;
                    break;
                case 28:
                    result = $"Ошибка 28: Встречено 2 объявления множества подряд. Ожидалась россыпь переменных." +
                        $"\r\nПеред объявлением нового множества необходимо описать предыдущее. (Строка {str + 1})";
                    er_count = 6;
                    break;
                case 29:
                    result = $"Ошибка 29: Программа должна начинаться словом \"Begin\"." +
                        $"\r\nВстречено: \"{MarkNext()}\"." +
                        $"\r\nСлово \"Begin\" должно быть отделено пробелом или переносом строки. (Строка {str + 1})";
                    break;
                case 30:
                    result = $"Ошибка 30: Программа должна оканчиваться словом \"End\"." +
                        $"\r\nВстречено: \"{MarkPrevious()}\"." +
                        $"\r\nСлово \"End\" должно быть отделено пробелом или переносом строки. (Строка {str + 1})";
                    break;
                case 31:
                    er = false;
                    result = $"Ошибка 31: В программе отстутствует объявление множества." +
                        $"\r\nОбъявление множества  - это слово \"анализ\" или \"синтез\"." +
                        $"\r\nПрограмма должна включать хотя бы одно множество.";
                    break;
                case 35:
                    result = $"Ошибка 35: Функция (\"sin\" или \"cos\") должена быть отделена от параметра пробелом или переносом строки. (Строка {str + 1})";
                    ch_num++;
                    MarkPrevious();
                    break;
                case 36:
                    result = $"Ошибка 36: Метка отсутствует." +
                        $"\r\nПеред знаком \":\" должна быть метка. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 37:
                    result = $"Ошибка 37: Переменная отсутствует." +
                        $"\r\nПеред знаком \"=\" должна быть переменная. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 38:
                    er = false;
                    result = $"Ошибка 38: Передана пустая строка." +
                        $"\r\nПрограмма должна начинаться словом \"begin\".";
                    break;
                case 39:
                    result = $"Ошибка 39: После \"Begin\" должно следовать уравнение." +
                        $"\r\n Программа должна заканчиваться словом \"End\"." +
                        $"\r\nВстречено: \"конец строки\" (Строка {str + 1})";
                    ch_num -= 5;
                    MarkNext();
                    break;
                case 40:
                    result = $"Ошибка 40: После знака \":\" должна следовать переменная." +
                        $"\r\nВстречено: \"{MarkNext()}\"  (Строка {str + 1})";
                    
                    break;
                case 41:
                    result = $"Ошибка 41: После метки должен следовать знак \":\"." +
                        $"\r\nВстречено: \"{MarkNext()}\"  (Строка {str + 1})";
                    break;
                case 42:
                    result = $"Ошибка 42: Объявление множества написано некорректно." +
                        $"\r\nОбъявление множества должено быть отделено пробелом или переносом строки. (Строка {str + 1})";
                    MarkNext();
                    break;
                case 43:
                    result = $"Ошибка 43: После переменной должен следовать знак \"=\"." +
                        $"\r\nВстречено: \"{MarkNext()}\"  (Строка {str + 1})";
                    break;
                case 44:
                    result = $"Ошибка 44: Встречен оператор \"{MarkNext()}\". Ожидался операнд." +
                        $"\r\nПеред оператором должна быть функция, переменная или целое число. (Строка {str + 1})";
                    break;
                case 45:
                    result = $"Ошибка 45: Встречено \";\". Ожидалась метка." +
                        $"\r\nПеред разделителем \";\" должно быть уравнениe. (Строка {str + 1})";
                    er_count = 1;
                    break;
                case 46:
                    result = $"Ошибка 46: Встречено \"{MarkNext()}\". Ожидалась россыпь переменных." +
                        $"\r\nПеред объявлением нового множества необходимо описать предыдущее. (Строка {str + 1})";
                    break;
            }
        }
        private static void StrCount(string s)
        {
            foreach (char ch in s)
                if (ch == '\n') { str_num++; ch_num = 0; }
                else ch_num++;
        }
        private static string SkippingCount(string s)
        {
            int i;
            for (i = 0; i < s.Length; i++)
            {
                if (s[i] != ' ' && s[i] != '\n') break;
                if (s[i] == '\n') { str_num++; ch_num = 0; }
                else ch_num++;
            }
            return s.Remove(0, i);
        }
        private static bool AreFull(string[] blocks, int err1, int err2, int err3, int skip = 1)
        {
            string block;
            int strEnd, chEnd;

            if (blocks[0].Length == 0) { Error(err3); return false; }
            block = SkippingCount(blocks[0]);
            if (block.Length == 0) { Error(err3); return false; }
            StrCount(block);
            for (int i = 1; i < blocks.Length; i++)
            {
                if (i == blocks.Length - 1)
                {
                    strEnd = str_num;
                    chEnd = ch_num;
                    ch_num += skip;
                    if (blocks[i].Length == 0) { Error(err1); return false; }
                    block = SkippingCount(blocks[i]);
                    if (block.Length == 0) { str_num = strEnd; ch_num = chEnd; Error(err1); return false; }
                    StrCount(block);
                }
                else
                {
                    ch_num += skip;
                    if (blocks[i].Length == 0) { Error(err2); return false; }
                    block = SkippingCount(blocks[i]);
                    if (block.Length == 0) { Error(err2); return false; }
                    StrCount(block);
                }
            }

            return true;
        }
        private static bool IsSymbol(char c) { return (Char.IsDigit(c) || Char.IsLetter(c)); }
        private static bool IsInt(string s)
        {
            foreach (char ch in s)
                if (!Char.IsDigit(ch)) return false;

            return true;
        }
        private static bool IsMark(string s, int str)
        {
            foreach (char ch in s)
                if (!Char.IsDigit(ch)) { Error(4, str, -1, s); return false; }

            return true;
        }
        private static bool IsVariable(string s, int str, int chh, bool rP = false, bool foo = false)
        {
            int er1, er2;
            if (!rP)
            {
                er2 = 12;
                if (!Char.IsLetter(s[0])) { Error(0, str, -1, s); return false; }
                foreach (char ch in s)
                    if (!IsSymbol(ch)) { Error(1, str, -1, s); return false; }
            }
            else
            {
                er2 = 22;

                if (foo) er1 = 20;
                else er1 = 13;

                if (!Char.IsLetter(s[0])) { str_num = str; ch_num = chh; Error(er1, -1, -1, s); return false; }
                foreach (char ch in s)
                    if (!IsSymbol(ch)) { str_num = str; ch_num = chh; Error(er1, -1, -1, s); return false; }
            }
            if (s.ToLower() == "sin" || s.ToLower() == "cos") { str_num = str; ch_num = chh; Error(er2); return false; }

            return true;
        }
        private static bool VariableExsist(string s)
        {
            foreach (Variable v in variables)
                if (s == v.getName())
                { 
                    varMain = v.getValue(); return true; 
                }

            return false;
        }
        private static string[] SplitString(string str, string[] delimiters)
        {
            int elCount = 1, elBegin = 0;
            string s = str.ToLower();
            for (int i = 0; i < s.Length; i++)
            {
                foreach (string del in delimiters)
                    if (s[i] == del[0])
                    {
                        if (s.Remove(0, i).Length >= del.Length)
                        {
                            if (s.Substring(i, del.Length) == del)
                            {
                                if (i != 0)
                                    if (s[i - 1] != ' ' && s[i - 1] != '\n')
                                        break;
                                if (s[i+del.Length] == ' ' || s[i+del.Length] == '\n')
                                    elCount++; i += del.Length - 1; break;
                            }
                        }
                    }
            }
            string[] elements = new string[elCount];
            elCount = 0;
            for (int i = 0; i < s.Length; i++)
            {
                foreach (string del in delimiters)
                    if (s[i] == del[0])
                    {
                        if (s.Remove(0, i).Length >= del.Length)
                        {
                            if (s.Substring(i, del.Length) == del)
                            {
                                if (i != 0)
                                    if (s[i - 1] != ' ' && s[i - 1] != '\n')
                                        break;
                                if (s[i + del.Length] == ' ' || s[i + del.Length] == '\n')
                                {
                                    elements[elCount] = str.Substring(elBegin, i - elBegin);
                                    i += del.Length - 1;
                                    elBegin = i + 1;
                                    elCount++;
                                    break;
                                }
                            }
                        }
                    }
            }
            elements[elCount] = str.Substring(elBegin, s.Length - elBegin);

            return elements;

        }
        private static bool Block5(string s, bool foo = false)
        {
            int i = 0, str = str_num, ch = ch_num;
            string v;
            bool errSix = false;
            if (s.Length == 1) { v = s; ch_num++; }
            else
            {
                while (i != s.Length)
                {
                    if (s[i] == ' ' || s[i] == '\n') break;
                    i++;
                }
                v = s.Substring(0, i);
                ch_num += i;

                s = SkippingCount(s.Remove(0, i));
                if (s.Length != 0) { errSix = true; }
            }
            if (!IsInt(v))
            {
                if (!foo) { if (!IsVariable(v, str, ch, true)) return false; }
                else { if (!IsVariable(v, str, ch, true, true)) return false; }

                if (!VariableExsist(v)) { str_num = str; ch_num = ch; Error(3, -1, -1, v); return false; }
            }
            else
                varMain = Convert.ToInt32(v);

            if (errSix) { Error(6, -1, -1, v); return false; }

            return true;
        }
        private static bool Block4(string s, bool fun = false)
        {
            double rad;
            int str, ch;

            if (s.Length > 2)
            {
                string foo = s.Substring(0, 3);
                if (foo.ToLower() == "sin" || foo.ToLower() == "cos")
                {
                    if (s.Length == 3) { Error(14); return false; }
                    str = str_num;
                    ch = ch_num;
                    if (s[3] == ' ' || s[3] == '\n')
                    {
                        ch_num += 3;
                        s = SkippingCount(s.Remove(0, 3));
                        if (s.Length == 0) { str_num = str; ch_num = ch; Error(14); return false; }

                        if (Block4(s, true))
                        {
                            rad = Convert.ToDouble(varMain);
                            if (foo == "sin") varMain = Math.Sin(rad);
                            else varMain = Math.Cos(rad);
                        }
                        else return false;
                    }
                    else if (!Block5(s, fun))
                    {
                        if (Block5(s.Remove(0, 3), fun)) Error(35);
                        return false;
                    }
                }
                else if (!Block5(s, fun)) return false;
            }
            else if (!Block5(s, fun)) return false;

            return true;
        }
        private static bool Block3(string s)
        {
            bool no = false;
            int str, ch = ch_num;

            if (s[0] == '!')
            {
                no = true; str = str_num;
                ch_num++;
                s = SkippingCount(s.Remove(0, 1));
                if (s.Length == 0) { str_num = str; ch_num = ch; Error(5); return false; }
            }

            if (!Block4(s)) return false;
            if (no) varMain *= -1;

            return true;
        }
        private static bool Block2(string s)
        {
            int strSpace = str_num, chSpace = ch_num, opCount = 0;
            char[] oper = { '&', '|' };
            string opOrder = String.Empty;
            varLog = 0;

            foreach (char ch in s)
                if (ch == '&' || ch == '|') opOrder += ch;
            string[] bl3 = s.Split(oper);

            if (bl3.Length == 1)
            {
                if (!Block3(bl3[0])) return false;
                varLog = varMain;
            }
            else
            {
                chSpace = ch_num;
                if (!AreFull(bl3, 5, 15, 44)) return false;

                ch_num = chSpace;
                str_num = strSpace;
                for (int i = 0; i < bl3.Length; i++)
                {
                    bl3[i] = SkippingCount(bl3[i]);
                    if (!Block3(bl3[i])) return false;

                    if (i == 0)
                    {
                        varLog = varMain;
                    }
                    else
                    {
                        if (opOrder[opCount] == '&')
                        {
                            if (varMain == 0)
                                varLog = 0;
                        }
                        else
                        {
                            if (varLog == 0 && varMain != 0)
                                varLog = varMain;
                        }
                        opCount++;
                    }
                    ch_num++;
                }
            }

            return true;
        }
        private static bool Block1(string s)
        {
            int strSpace = str_num, chSpace = ch_num, opCount = 0;
            char[] oper = { '*', '/' };
            string opOrder = String.Empty;

            foreach (char ch in s)
                if (ch == '*' || ch == '/') opOrder += ch;
            string[] bl2 = s.Split(oper);

            if (bl2.Length == 1)
            {
                if (!Block2(bl2[0])) return false;
                varInterm = varLog;
            }
            else
            {
                chSpace = ch_num;
                if (!AreFull(bl2, 5, 15, 44)) return false;

                ch_num = chSpace;
                str_num = strSpace;
                for (int i = 0; i < bl2.Length; i++)
                {
                    bl2[i] = SkippingCount(bl2[i]);
                    strSpace = str_num;
                    chSpace = ch_num;
                    if (!Block2(bl2[i])) return false;

                    if (i == 0)
                    {
                        varInterm = varLog;
                    }
                    else
                    {
                        if (opOrder[opCount] == '*') varInterm *= varLog;
                        else
                        {
                            if (varLog == 0) { str_num = strSpace; ch_num = chSpace; Error(19); return false; }
                            varInterm /= varLog;
                        }
                        opCount++;
                    }
                    ch_num++;
                }
            }

            return true;
        }
        private static bool RightPart(string s)
        {
            bool minus = false;
            int strM, strSpace = str_num, chSpace = ch_num, opCount = 0;
            char[] oper = { '+', '-' };
            string opOrder = String.Empty;

            if (s[0] == '-')
            {
                minus = true;
                strM = str_num;
                ch_num++;
                s = SkippingCount(s.Remove(0, 1));
                if (s.Length == 0) { str_num = strM; ch_num = chSpace; Error(2); return false; }
            }

            foreach (char ch in s)
                if (ch == '+' || ch == '-') opOrder += ch;
            string[] bl1 = s.Split(oper);

            if (bl1.Length == 1)
            {
                if (!Block1(bl1[0])) return false;
                if (minus) varInterm *= -1;
                varResult = varInterm;
            }
            else
            {
                chSpace = ch_num;
                if (!AreFull(bl1, 5, 15, 44)) return false;

                ch_num = chSpace;
                str_num = strSpace;
                for (int i = 0; i < bl1.Length; i++)
                {
                    bl1[i] = SkippingCount(bl1[i]);
                    if (!Block1(bl1[i])) return false;

                    if (i == 0)
                    {
                        if (minus) varInterm *= -1;
                        varResult = varInterm;
                    }
                    else
                    {
                        if (opOrder[opCount] == '+') varResult += varInterm;
                        else varResult -= varInterm;
                        opCount++;
                    }
                    ch_num++;
                }
            }

            return true;
        }
        private static bool IsEquation(string s)
        {
            int i = 0;
            string newVar = String.Empty, mark = String.Empty;
            bool space = false;

            while (s[i] != ':' && i != s.Length-1)
            {
                if (s[i] == ' ' || s[i] == '\n')
                {
                    mark = s.Remove(i, s.Length - i);
                    if (!IsMark(mark, str_num)) return false;
                    ch_num += i;
                    s = SkippingCount(s.Remove(0, i));
                    if (s.Length == 0) { Error(41); return false; }
                    if (s[0] != ':') { Error(8); return false; }
                    else { i = 0; space = true; break; }
                }
                i++;
            }
            if (!space)
            {
                if (i == s.Length - 1) 
                {                    
                    if (s[i] == ':') 
                    {
                        mark = s.Remove(i);
                        if (mark.Length == 0) { Error(36); return false; }
                        if (!IsMark(mark, str_num)) return false;
                        ch_num += i;
                        Error(40); return false; 
                    }
                    else 
                    {
                        if (s[i] == ' ') s = s.Remove(i);
                        if (!IsMark(s, str_num)) return false;
                        ch_num += i;
                        Error(41); return false; 
                    }
                }
                else 
                { 
                    mark = s.Remove(i, s.Length - i);
                    if (mark.Length == 0) { Error(36); return false; }
                    if (!IsMark(mark, str_num)) return false;
                    ch_num += i;
                }
            }
            space = false; ch_num++;

            if (i == 0) s = s.Remove(0, 1);
            else { s = s.Remove(0, i+1); i = 0; }
            
            s = SkippingCount(s);
            if (s.Length == 0) { Error(40); return false; }
            while (s[i] != '=' && i != s.Length - 1)
            {
                if (s[i] == ' ' || s[i] == '\n')
                {
                    newVar = s.Remove(i, s.Length - i);
                    if (!IsVariable(newVar, str_num, ch_num)) return false;
                    ch_num += i;
                    s = SkippingCount(s.Remove(0, i));
                    if (s.Length == 0) { Error(43); return false; }
                    if (s[0] != '=') { Error(9); return false; }
                    else { i = 0; space = true; break; }
                }
                i++;
            }
            if (!space)
            {
                if (i == s.Length - 1)
                {
                    if (s[i] == '=')
                    {
                        newVar = s.Remove(i);
                        if (newVar.Length == 0) { Error(37); return false; }
                        if (!IsVariable(newVar, str_num, ch_num)) return false;
                        ch_num += i;
                        Error(10); return false;
                    }
                    else
                    {
                        if (s[i] == ' ') s = s.Remove(i);
                        if (!IsVariable(newVar, str_num, ch_num)) return false; ;
                        ch_num += i;
                        Error(43); return false;
                    }
                }
                else
                {
                    newVar = s.Remove(i, s.Length - i);
                    if (newVar.Length == 0) { Error(37); return false; }
                    if (!IsVariable(newVar, str_num, ch_num)) return false;
                    ch_num += i;
                }
            }
            ch_num++;

            if (i == 0) s = s.Remove(0, 1);
            else { s = s.Remove(0, i + 1);}

            s = SkippingCount(s);
            if (s.Length == 0) { Error(10); return false; }                        
            if (!RightPart(s)) return false; 

            Variable nVar;
            bool check = false;
            foreach (Variable v in variables)
                if (newVar == v.getName()) { v.setValue(varResult); check = true; }

            if (!check)
            {
                nVar = new Variable();
                nVar.setName(newVar);
                nVar.setValue(varResult);
                variables.Add(nVar);
            }

            return true;
        }
        private static bool Equations(string s)
        {
            int strSpace = str_num;
            int chSpace = ch_num;
            string[] eqs = s.Split(';');

            if (eqs.Length == 1)
            { if (!IsEquation(eqs[0])) return false; }
            else
            {
                if (!AreFull(eqs, 17, 18, 45)) return false;

                str_num = strSpace;
                ch_num = chSpace;
                for (int i = 0; i < eqs.Length; i++)
                {
                    eqs[i] = SkippingCount(eqs[i]);
                    if (!IsEquation(eqs[i])) return false;
                    ch_num++;
                }
            }

            return true;
        }
        private static bool IsArray(string s)
        {
            int i = 0;
            string v = String.Empty;

            while (true)
            {
                while (s[i] != ' ' && s[i] != '\n' && i != s.Length - 1) i++;
                if (i == s.Length - 1)
                {
                    v = s;
                    if (s[i] == ' ' || s[i] == '\n') v = v.Remove(v.Length - 1, 1);
                    if (IsVariable(v, -1, -1)) array.Add(v);
                    else return false;
                    break;
                }
                else
                {
                    v = s.Substring(0, i);
                    if (!IsVariable(v, -1, -1)) return false;
                    array.Add(v);
                    s = s.Remove(0, i);
                    s = SkippingCount(s);
                    i = 0;
                    if (s.Length == 0) break;
                }
            }

            return true;
        }
        private static bool Arrays(string s)
        {
            string[] delim = new string[2] { "анализ", "синтез" };
            int strSpace = str_num, chSpace = ch_num;

            s = SkippingCount(s);
            if (s.Length == 0) { Error(27); return false; }
            string[] arr = SplitString(s, delim);

            if (arr.Length == 1)
            {
                if (!IsArray(arr[0])) return false;
                arrays.Add(array.ToArray());
                array.Clear();
            }
            else
            {
                chSpace = ch_num;
                if (!AreFull(arr, 27, 28, 46, 6)) return false;

                ch_num = chSpace;
                str_num = strSpace;
                foreach (string a in arr)
                {
                    if (!IsArray(SkippingCount(a))) return false;
                    arrays.Add(array.ToArray());
                    array.Clear();
                }
            }

            return true;
        }
        private static void BuidResult()
        {
            result = "Уравнения\r\n";
            foreach (Variable v in variables) 
                result += $"{v.getName()} = {v.getValue()}\r\n";
            result += "Множества:\r\n";
            for (int i = 0; i < arrays.Count(); i++)
            {
                result += $"{i+1}:  ";
                foreach (string v in arrays[i])
                    result += $"{v}  ";
                result += "\r\n";
            }

        }
        private static bool Language(string s)
        {
            string[] delimiters = new string[2] { "анализ", "синтез" };
            string eqs = String.Empty, arr = String.Empty;
            bool exit = false;

            s = SkippingCount(s);
            if (s.Length == 0) { Error(38); return false; }
            if (s.Length < 5) { Error(29); return false; }
            if (s.Substring(0, 5).ToLower() != "begin") { Error(29); return false; }
            if (s.Length < 6) { ch_num += 5; Error(39); return false; }
            if (s[5] != ' ' && s[5] != '\n') { Error(29); return false; }
            ch_num += 5;

            s = SkippingCount(s.Remove(0, 5));
            if (s.Length < 3) { ch_num = input_str[str_num].TrimEnd().Length - 1; Error(30); return false; }
            if (s.Substring(s.Length - 3, 3).ToLower() != "end") 
            {
                str_num = input_str.Length - 1;
                ch_num = input_str[str_num].TrimEnd().Length - 1; 
                Error(30); return false; 
            }
            s = s.Remove(s.Length - 3, 3);
            if (s.Length != 0)
                if (s[s.Length-1] != ' ' && s[s.Length - 1] != '\n')
                {
                    str_num = input_str.Length - 1;
                    ch_num = input_str[str_num].TrimEnd().Length - 1;
                    Error(30); return false;
                }

            if (!s.ToLower().Contains("анализ") && !s.ToLower().Contains("синтез")) { Error(31); return false; }

            int i;
            bool exception = false;
            int exStr = str_num, exCh = 0;
            for (i = 0; i < s.Length; i++)
            {
                foreach (string del in delimiters)
                    if (s.ToLower()[i] == del[0])
                    {
                        if (s.Remove(0, i).Length >= del.Length)
                        {
                            if (s.ToLower().Substring(i, del.Length) == del)
                            {
                                if (i != 0)
                                    if (s[i - 1] != ' ' && s[i - 1] != '\n') 
                                    {
                                        if (!exception)
                                        {
                                            exception = true;
                                            for (int k = 0; k < i; k++)
                                                if (s[k] == '\n') { exStr++; exCh = k; }
                                            if (s[exCh + 1] == '\n') exCh++;
                                            exCh = i - exCh - 1;
                                        }
                                        continue; 
                                    }
                                if (s[i + del.Length] != ' ' && s[i + del.Length] != '\n')
                                {
                                    if (!exception)
                                    {
                                        exception = true;
                                        for (int k = 0; k < i; k++)
                                            if (s[k] == '\n') { exStr++; exCh = k; }
                                        if (s[exCh + 1] == '\n') exCh++;
                                        exCh = i - exCh - 1;
                                    }
                                    continue;
                                }
                                eqs = s.Substring(0, i);
                                i += del.Length;
                                arr = s.Substring(i, s.Length - i);
                                exit = true;
                                break;
                            }
                        }
                    }
                if (exit) break;
            }
            if (!exit) 
            {
                if (exception)
                {
                    str_num = exStr;
                    ch_num = exCh;
                    Error(42);
                }
                else Error(31); 
                return false; 
            }

            int j = i;
            for (i = 0; i < arr.Length; i++)
                if (arr[i] != ' ' && arr[i] != '\n') break;

            if (eqs.Length == 0) { Error(16); return false; }
            if (!Equations(eqs)) return false;
            ch_num += 6;
            arr = SkippingCount(arr);

            if (arr.Length == 0) { Error(27); return false; }
            if (!Arrays(arr)) return false;

            BuidResult();
            return true;
        }
        public static string Output(string input)
        {
            er = false;
            variables.Clear();
            arrays.Clear();
            array.Clear();
            str_num = 0; ch_num = 0;
            input = input.TrimEnd();
            input_str = input.Split('\n');
            Language(input);
            
            return result;
        }
        public static string GetInput()
        {
            string input = String.Empty;
            foreach (string str in input_str)
                input += str + "\n";
            input = input.Remove(input.Length - 1);
            return input;
        }
    }
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}// 
